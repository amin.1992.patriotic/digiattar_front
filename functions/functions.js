import Api from "../services/api";
import {toast} from "react-toastify";

const reminder = (phone , email , id , type) => {
    const obj = {
        "sms":phone ,
        "email":email,
        "reminder_type": type,
        "notification_by": "all",
        "record_id": id
    }
    new Api().post(`/reminder`, obj )
        .then((resp) => {
            if (typeof resp != "undefined") {
                toast.success(resp.msg)
            } else {
               return false
            }
        })
        .catch((err) => {
            throw err
        })
}

export { reminder }
