module.exports = {
    apps : [{
        name        : "digiattar",
        script      : "./server.js",
        watch       : true,
        env: {
            "NODE_ENV": "production",
        }
    }]
}
