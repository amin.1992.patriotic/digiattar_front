import axios from 'axios';
import { toast } from 'react-toastify';
import Cookies from 'universal-cookie';
import moment from "moment-jalaali";

import CONST from "../const/const";
import { ENV } from "../services/env"

let CryptoJS = require("crypto-js");



class Api {


    _headers() {
        let auth = '';
        const cookies = new Cookies();
        if (cookies.get('auth')) {
            auth = cookies.get('auth');
        }
        return {
            Accept: 'application/json',
            Authorization: `Bearer ${auth}`
        };
    }

    // check response after receive
    _dispatchResponse(response) {
        if (typeof response !== 'undefined') {
            if (response.status === 401) {
                cookies.remove('auth')
            } else if (response.status === 404) {
                toast.error('خطای سرویس گیرنده: آدرس وجود ندارد و یا سرور دان است.');
            } else if(response.status === 400) {
                toast.error('ساعت سیستم خود را به روی Asia/Tehran تنظیم نمایید.')
            } else {
                toast.error(response.statusText);
            }
        } else {
            console.log(response)
        }
    }

    _makeRequest(object) {

        let n_object = 'random:' + Math.random() + ';' +'time:' + moment().add(60, 'seconds').unix() + ';' + 'origin:' + "localhost:3007";

        let keys = Object.keys(object);

        keys.forEach((item) => {
            n_object += ";" + item + ':' + object[item]
        })
        console.log(object)


        let key = CryptoJS.enc.Hex.parse(CONST["k"]);
        let iv = CryptoJS.enc.Hex.parse(CONST["i"]);

        let encrypted = CryptoJS.AES.encrypt(n_object, key, {
            iv,
            padding: CryptoJS.pad.ZeroPadding,
        });

        return encrypted.toString();
    }

    /**
     * ---------------------------------------------------------------
     *  GET POST PUT DELETE API
     * ---------------------------------------------------------------
     */
    get(url, object= {}, hash = true) {
        return axios.get( ENV.API.FETCH +`${url}`, {
            headers: this._headers(),
            params: hash ? {'request' : this._makeRequest(object)} : object
        }).then( (response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }

    post(url, object= {}, hash = true) {
        return axios.post( ENV.API.FETCH + `${url}`, hash ? {'request' : this._makeRequest(object)} : object, {
            headers: this._headers(),
        }).then( (response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }

    put(url, object={}, hash= true) {
        return axios.put( ENV.API.FETCH+ `${url}`,  hash ? {'request' : this._makeRequest(object)} : object,{
            headers: this._headers(),
        }).then( (response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }

    delete(url, object = {}) {
        return axios.delete( ENV.API.FETCH+ `${url}`, {
            params : {'request' : this._makeRequest(object)},
            headers: this._headers(),
        }).then((response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }


    upload(url, object= {}) {
        return axios.post( ENV.API.domain+ `${url}`, object, {
            headers: {
                'content-type': 'multipart/form-data',
                Authorization: `Bearer ` + cookies.get('auth')
            }
        }).then( (response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }



}

export default Api;
