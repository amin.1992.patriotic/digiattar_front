import React, {useEffect, useState} from 'react';
import styles from "../../styles/userProfile.module.css"
import {Grid} from "@material-ui/core";
import Container from '@material-ui/core/Container';
import Footer from "../../components/footer/footer"
import Header from "../../components/header/header"
import {useDispatch, useSelector} from "react-redux";
import loginReducer from "../../redux/reducers/auth";
import {useRouter} from "next/router";
import Input from "../../components/input/Input";
import Button from "../../components/button/button";
import {separate} from "../../services/separate";
import Layout from "../../components/layout/layout";
import {Helmet} from "react-helmet";
import {ENV} from "../../services/env";
import Api from "../../services/api";
import {toast} from "react-toastify";
import moment from 'moment-jalali';
import InlineLoader from "../../components/inlineLoader/inlineLoader";
import {userData} from "../../redux/actions";

const Profile = (props) => {
    const loginData = useSelector((state) => state.loginReducer)
    const data = useSelector(state =>  state.loginReducer.user)
    const dispatch = useDispatch()
    const [userWishlist, setUserWishlist] = useState(false)
    const [userList, setUserList] = useState(true)
    const [editProfile, setEditProfile] = useState("")
    const [userAddresses, setUserAddresses] = useState("")
    const [userOrderList, setUOrderList] = useState(false)
    const [userChargeWallet, setChargeWallet] = useState("")
    const [phone, setPhone] = useState("")
    const [name, setName] = useState("")
    const [amount, setAmount] = useState("")
    const [userListData, setUsrListData] = useState("")
    const [loading, setLoading] = useState("")
    const [openDetail, setOpenDetail] = useState("")
    const [email, setEmail] = useState("")
    const [family, setFamily] = useState("")
    useEffect(() => {
        setLoading(true)
        new Api().get("/auth/orderlist", {})
            .then((res) => {
                console.log(res)
                setUsrListData(res)
                setLoading(false)

            })
            .catch((err) => {
                toast.error("خطایی رخ داده")
                setLoading(false)

            })
    }, [])

    const router = useRouter()
    function handleMenu(e) {
        switch (e) {
            case 1 : {
                return (
                    setUserList(true),
                        setEditProfile(false),
                        setUserWishlist(false),
                        setUserAddresses(false),
                        setUOrderList(false),
                        setChargeWallet(false)
                )
            }
            case 2 : {
                return (
                    setUserList(false),
                        setEditProfile(true),
                        setUserWishlist(false),
                        setUserAddresses(false),
                        setUOrderList(false),
                        setChargeWallet(false)
                )
            }
            case 3 : {
                return (
                    setUserList(false),
                        setEditProfile(false),
                        setUserAddresses(true),
                        setUserWishlist(false),
                        setUOrderList(false),
                        setChargeWallet(false)
                )
            }
            case 4 : {
                return (
                    setUserList(false),
                        setEditProfile(false),
                        setUserAddresses(false),
                        setUserWishlist(false),
                        setUOrderList(true),
                        setChargeWallet(false)
                )
            }
            case 5 : {
                return (
                    setUserList(false),
                        setEditProfile(false),
                        setUserAddresses(false),
                        setUserWishlist(false),
                        setUOrderList(false),
                        setChargeWallet(true)
                )
            }
        }
    }

    const handleEditProfile = () => {
        setLoading(true)
        new Api().put("/auth/user",{
            mobile:phone,
            email:email,
            name:name,
            family:family
        })
            .then((res) => {
                setLoading(false)
                if(res.status) {
                    toast.success("اطلاعات با موفقیت ثبت شد")
                    dispatch(userData(res))
                } else {
                    toast.error(res.message)
                }
            })
            .catch((err) => {
                setLoading(false)
                toast.error("خطایی رخ داده است .")
            })
    }

    return (
        <>
            <Helmet>
                <title>{"دیجی عطار مرجع اطلاعات و فروشگاه جامع گیاهان دارویی وطب سنتی | پروفایل"}</title>
            </Helmet>
            <Layout>
                <Container>
                    {
                        loginData.login ?
                            <Grid container={true} spacing={5}>
                                <Grid item md={3} xs={12}>
                                    <div className={styles.profile_menu}>
                                        <img className={styles.profile_avator} src={"/avator.png"}/>
                                        <span className={styles.profile_name}>{data.name + " " + data.family}</span>
                                        <span className={styles.profile_gmail}>{data && data.mobile}</span>
                                        {/*<div className={styles.wallet}>*/}
                                        {/*    <span className={styles.walletـ_Inventory}>موجودی کیف پول</span>*/}
                                        {/*    <span className={styles.walletـ_Inventory_cash}>82,00000</span>*/}
                                        {/*</div>*/}
                                        <div className={styles.user_menu}>
                                            <div className={styles.user_menu_title}>منوی کاربری</div>
                                            <div onClick={() => {
                                                handleMenu(1)
                                            }} className={styles.user_menu_list}>
                                                <img className={styles.user_menu_list_icon} src={"/des.png"}/>
                                                <span style={{color: userList && "var(--primary)"}}
                                                      className={styles.user_menu_list_text}>لیست سفارشات</span>
                                            </div>
                                            <div onClick={() => {
                                                handleMenu(2)
                                            }} className={styles.user_menu_list}>
                                                <img className={styles.user_menu_list_icon} src={"/des.png"}/>
                                                <span style={{color: editProfile && "var(--primary)"}}
                                                      className={styles.user_menu_list_text}>ویرایش پروفایل</span>
                                            </div>
                                            {/*<div onClick={() => {*/}
                                            {/*    handleMenu(3)*/}
                                            {/*}} className={styles.user_menu_list}>*/}
                                            {/*    <img className={styles.user_menu_list_icon} src={"/des.png"}/>*/}
                                            {/*    <span className={styles.user_menu_list_text}>آدرس های ثبت شده</span>*/}
                                            {/*</div>*/}
                                            {/*<div onClick={() => {*/}
                                            {/*    handleMenu(5)*/}
                                            {/*}} className={styles.user_menu_list}>*/}
                                            {/*    <img className={styles.user_menu_list_icon} src={"/des.png"}/>*/}
                                            {/*    <span style={{color: userChargeWallet && "var(--primary)"}}*/}
                                            {/*          className={styles.user_menu_list_text}>شارژ کیف پول</span>*/}
                                            {/*</div>*/}
                                        </div>
                                    </div>
                                </Grid>
                                <Grid item md={9} xs={12}>
                                    {
                                        userListData && userListData.length === 0 ?
                                        <div className={styles.emptyList}>
                                            {"سفارشی برای نمایش وجود ندارد"}
                                        </div>
                                        :
                                        <div className={styles.profile_main}>
                                            <div style={{display: userList ? "flex" : "none"}}
                                                 className={styles.profile_main_tab}>

                                                <div className={styles.profile_main_tab_list}>
                                                    <div className={styles.profile_main_tab_list_item}>
                                                        <table cellSpacing={0}>
                                                            <thead>
                                                            <tr>
                                                                <th>ردیف</th>
                                                                <th>شماره سفارش</th>
                                                                <th>تاریخ ثبت سفارش</th>
                                                                <th>مبلغ قابل پرداخت</th>
                                                                <th>عملیات پرداخت</th>
                                                                <th>جزییات</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            {
                                                                loading ?
                                                                    <InlineLoader/>
                                                                    :
                                                                    userListData && userListData.map((item, index) => {
                                                                        return (
                                                                            <tr>
                                                                                <td>{index + 1}</td>
                                                                                <td>{item.id}</td>
                                                                                <td>{moment(item.updated_at).format("jYYYY/jM/jD")}</td>
                                                                                <td>{separate(item.off_price) + "تومان"}</td>
                                                                                <td>{item.status === 1 ? "پرداخت شده" : "پرداخت نشده"}</td>
                                                                                <td onClick={(e) => {
                                                                                    router.push(`/invoice/${item.token}`)
                                                                                    // e.target === e.currentTarget && setOpenDetail(item.id)
                                                                                }}>
                                                                                    مشاهده
                                                                                    <div onClick={(e) => {
                                                                                        router.push(`/invoice/${item.id}`)
                                                                                        // e.target === e.currentTarget && setOpenDetail("")
                                                                                    }}
                                                                                         className={`${openDetail === item.id ? styles.open_profile_detail : styles.close_profile_detail} ${styles.profile_detail}`}>
                                                                                        <div
                                                                                            className={styles.profile_detail_box}>
                                                                                            <div>
                                                                                                <span>{"شماره سفارش : "}</span>
                                                                                                <span>{item.id}</span>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span>{"تاریخ ثبت سفارش : "}</span>
                                                                                                <span>{moment(item.updated_at).format("jYYYY/jM/jD")}</span>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span>{"مبلغ قابل پرداخت : "}</span>
                                                                                                <span>{separate(item.off_price)}</span>
                                                                                                <span>{" تومان "}</span>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span>{"تخفیف : "}</span>
                                                                                                <span>{separate(item.price - item.off_price)}</span>
                                                                                                <span>{" تومان "}</span>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span>{"عملیات پرداخت : "}</span>
                                                                                                <span>{item.status === 1 ? "پرداخت شده" : "پرداخت نشده"}</span>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span>{"هزینه پست : "}</span>
                                                                                                <span>{item.post_cost}</span>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span>{"مبلغ پرداخت شده : "}</span>
                                                                                                <span>{item.paid}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        )

                                                                    })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style={{display: editProfile ? "flex" : "none"}}
                                                 className={styles.profile_main_tab}>
                                                <div className={styles.profile_main_tab_inputs}>
                                                    <Input
                                                        hasText={"ویرایش نام"}
                                                        data={name}
                                                        onchange={(e) => {
                                                            setName(e)
                                                        }}
                                                        placeholder={"ویرایش نام"}
                                                        type={"text"}
                                                    />
                                                </div>
                                                <div className={styles.profile_main_tab_inputs}>
                                                    <Input
                                                        hasText={"ویرایش نام خانوادگی"}
                                                        data={family}
                                                        onchange={(e) => {
                                                            setFamily(e)
                                                        }}
                                                        placeholder={"ویرایش نام خانوادگی"}
                                                        type={"text"}
                                                    />
                                                </div>
                                                <div className={styles.profile_main_tab_inputs}>
                                                    <Input
                                                        hasText={"ویرایش شماره تلفن"}
                                                        data={phone}
                                                        onchange={(e) => {
                                                            setPhone(e)
                                                        }}
                                                        placeholder={"ویرایش شماره تلفن"}
                                                        type={"text"}
                                                    />
                                                </div>
                                                <div className={styles.profile_main_tab_inputs}>
                                                    <Input
                                                        hasText={"ویرایش ایمیل"}
                                                        data={email}
                                                        onchange={(e) => {
                                                            setEmail(e)
                                                        }}
                                                        placeholder={"ویرایش ایمیل"}
                                                        type={"text"}
                                                    />
                                                </div>
                                                <div className={styles.profile_main_tab_btn}>
                                                    <Button
                                                        loading={loading}
                                                        onClick={() => {
                                                            handleEditProfile()
                                                        }}
                                                        icon={"/arrow.png"}
                                                        text={"ویرایش"}
                                                        backgroundColor={"var(--primary)"}
                                                        width={"200px"}
                                                        padding={10}
                                                        margin={0}
                                                    />
                                                </div>
                                            </div>
                                            <div style={{display: userAddresses ? "flex" : "none"}}
                                                 className={styles.profile_main_tab}>ادرس
                                            </div>
                                            {/*<div style={{display: userChargeWallet ? "flex" : "none"}}*/}
                                            {/*     className={styles.charge_wallet}>*/}
                                            {/*    <div className={styles.charge_wallet_box}>*/}
                                            {/*        {*/}
                                            {/*            amount === "" ?*/}
                                            {/*                <span>مبلغ مورد نظر جهت شارژ کیف پول را وارد کنید</span>*/}
                                            {/*                :*/}
                                            {/*                <span>{`  شارژ کیف پول به مبلغ :${separate(amount)} تومان `}</span>*/}
                                            {/*        }*/}
                                            {/*        <Input*/}
                                            {/*            data={amount}*/}
                                            {/*            onchange={(e) => {*/}
                                            {/*                setAmount(e)*/}
                                            {/*            }}*/}
                                            {/*            placeholder={"مبلغ"}*/}

                                            {/*        />*/}
                                            {/*        <div style={{*/}
                                            {/*            opacity: amount === "" && .3,*/}
                                            {/*            pointerEvent: amount === "" && .3*/}
                                            {/*        }} className={styles.charge_wallet_box_btn}>*/}
                                            {/*            <Button*/}
                                            {/*                icon={"/arrow.png"}*/}
                                            {/*                text={"پرداخت"}*/}
                                            {/*                backgroundColor={"var(--primary)"}*/}
                                            {/*                width={"150px"}*/}
                                            {/*                padding={0}*/}
                                            {/*                margin={0}*/}
                                            {/*            />*/}
                                            {/*        </div>*/}
                                            {/*    </div>*/}
                                            {/*</div>*/}
                                        </div>
                                    }
                                </Grid>
                            </Grid> :
                            <div className={styles.show_profile}>
                                لطفا وارد حساب کاربری خود شوید
                            </div>
                    }

                </Container>
            </Layout>
        </>
    );
};

export default Profile;


