import React, {useEffect, useState ,useCallback} from 'react';
import {Provider} from 'react-redux'
import {store} from '../redux/store'
import '../styles/globals.css'
import { QueryClient, QueryClientProvider } from 'react-query'
import { Hydrate } from 'react-query/hydration'
import { ReactQueryDevtools } from 'react-query/devtools'
import { useRouter } from 'next/router';
import NProgress from "nprogress"
import 'nprogress/nprogress.css'
import PersistWrapper from 'next-persist/lib/NextPersistWrapper';


const App = ({Component, pageProps}) => {
    const router = useRouter();
    const [loading, setLoading] = useState(null);

    //import serviceWorker
    useEffect(() => {
        if("serviceWorker" in navigator) {
            window.addEventListener("load",  () => {
                navigator.serviceWorker.register("/sw.js").then(
                     (registration) => {
                        console.log("Service Worker registration successful with scope: ");
                    },
                );
            });
        }
    }, [])


    const onRouteChangeStart = useCallback(() => {
        setLoading(true);
        NProgress.start()
    }, []);

    const onRouteChangeDone = useCallback(() => {
        setLoading(false);
        NProgress.done()
    }, []);







    useEffect(() => {
        router.events.on('routeChangeStart', onRouteChangeStart);
        router.events.on('routeChangeComplete', onRouteChangeDone);
        router.events.on('routeChangeError', onRouteChangeDone);
    }, [onRouteChangeDone, onRouteChangeStart, router.events]);

    NProgress.configure({ showSpinner: false ,  easing: 'ease', speed: 350 });

    const npConfig = {
        method: 'localStorage',
        allowList: {
            loginReducer: ['login'],
        },
    };


    const [queryClient] = React.useState(() => new QueryClient())



    useEffect(()  => {
    window.addEventListener('contextmenu', function (e) {
      e.preventDefault();
    }, false);
  })


  return (
      <Provider store={store}>
          <PersistWrapper wrapperConfig={npConfig}>
          <QueryClientProvider client={queryClient}>
              <Hydrate state={pageProps.dehydratedState}>
                  {loading && <div style={{position:"fixed",width:"100%",height:"100%",zIndex:"1000",display:"flex",alignItems:"center",background:"white",justifyContent:"center",top:0}} ><img  src={("/loading.gif")}/></div>}
                  <Component {...pageProps} />
              </Hydrate>
              <ReactQueryDevtools />
          </QueryClientProvider>
          </PersistWrapper>
      </Provider>
  )
}
export default App;











