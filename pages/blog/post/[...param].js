import React, {useEffect, useRef, useState} from 'react';
import styles from "../../../styles/blog.module.css"
import {QueryCache, useQuery, usePaginatedQuery, QueryClient} from "react-query";
import {dehydrate} from "react-query/hydration";
import Api from "../../../services/api";
import {useRouter} from "next/router";
import Layout from "../../../components/layout/layout";
import Container from '@material-ui/core/Container';
import {Grid} from "@material-ui/core";
import {Helmet} from "react-helmet";
import moment from 'moment-jalali';
import Link from "next/link";
import {ENV} from "../../../services/env";
import InlineLoader from "../../../components/inlineLoader/inlineLoader";
import Share from "../../../components/share/share";



const fetchBlogParams = async (key) => {
        const res = await new Api().get(`/blog/content/${key.queryKey[1]}`,{});
        return res
}
const BlogParams = (props) => {
    const [toggle , setToggle] = useState(false)
    const titleRef = useRef([])
    const {blogParams} = props
    console.log(blogParams)
    const  router = useRouter()
    const id = router.query.param && router.query.param[0] || null
    const {data, isFetching, status} = useQuery([ "blogParam", id ], fetchBlogParams, {
        staleTime: 100000,
        initialData: blogParams,
    })
    useEffect(() => {
        if(typeof router.query.param[1] === "undefined") {
            router.push(`/blog/post/[...param]?id=${data.id}&slug=${data.slug}`, `/blog/post/${data.id}/${data.slug}`)
        }
        if(data && data.id === 159) {
            let text
            let titleId = localStorage.getItem("temperament_id")
            switch (parseInt(titleId)) {
                case 1 : {
                    text = "دم (گرم و تر)"
                    break;
                }
                case 2 : {
                    text = "طبع صفرا (گرم و خشک)"
                    break;
                }
                case 3 : {
                    text = "طبع بلغم (سرد و تر)"
                    break;
                }
                case 4 : {
                    text = "طبع سودا"
                    break;
                }
            }
            let target = document.querySelectorAll("strong")
            for(let i = 0 ; i < target.length - 1 ; i++) {
                if(target[i].innerText.includes(text)) {
                    if(typeof target[i].offsetTop !== "undefined") {
                        setTimeout(()=>{
                            window.scrollTo(0 , target[i].offsetTop)
                        },100)
                    }
                }
            }
        }
        window.scrollTo({top: 0, behavior: 'smooth'});
    },[data])


    const handleScrollOnTitle = (clickedItem) => {
        if(typeof window !== "undefined") {
            const itemInContent = window.document.querySelectorAll("h2")
            let arr = [...itemInContent]
            for(let item of arr) {
                item.style.color = "unset"
            }
            let item = arr?.find((item) => {
                return  clickedItem.value.replace(/(<([^>]+)>)/gi, "") === item.innerText
            })
            if(item)
            item.style.color = "red"
            item?.scrollIntoView({behavior:"smooth" , block:"center"})
        }
    }
    return (
        <Layout>
            <div className={styles.blog_ca}>
                <Helmet>
                    <title>{data && data.meta_title}</title>
                    <script type="application/ld+json">{`
                                {
                                    "@context": "https://schema.org",
                                    "@type": "BlogPosting",
                                    "name":"دیجی عطار"
                                    "mainEntityOfPage": {
                                    "@type": "WebPage",
                                        "@id":  "${ENV.API['FETCH']}"
                                },
                                    "headline": "${data && data.meta_title}",
                                
                                },
                                    "author": {
                                    "@type": "Person",
                                        "name": "digiattar"
                                },
                                    "publisher": {
                                    "@type": "Organization",
                                        "name": "digiattar",
                                      
                                    },
                                    "description": "${data && data.meta_description}"

                                }
                    `}</script>
                    <meta name='description' content={data ? (data && data.meta_description && data.meta_description) : ''}/>
                    <link rel="canonical" href={`${ENV.API['FETCH']}/${data ? data.id : ""}/${data ? data.slug : ""}`}/>
                </Helmet>
                <Container>

                    <Grid  xs={12}>
                        <div className={styles.blog_head}>
                            {isFetching && <InlineLoader/>}
                            <h1>{data ? data.title : ""}</h1>
                            {
                                data && data.heading && <h2>{data && data.heading}</h2>
                            }
                            <ul  className={styles.blog_detail}>
                                {
                                    data && data.created_by  &&
                                    <li>
                                        <span className={styles.account_img}/>
                                        {data.created_by && data.created_by.name}
                                    </li>
                                }
                                {
                                    data && data.created_at &&
                                    <li>
                                        <img src={"/time.svg"}/>
                                        {data && `${moment(data.created_at).format('jYYYY/jM/jD')}`}
                                    </li>
                                }
                                <Share share={true} position={"relative"} width={"30px"}/>
                            </ul>
                            {/*{data &&  <Bread item={data} route={"blog"}/>}*/}
                        </div>
                    </Grid>
                    <Grid spacing={2} container>
                        <Grid item md={9} xs={12}>
                            <div className={styles.main_img}>
                                {data && data.img &&
                                <img alt={data && data.meta_title} src={`${ENV.API['IMG']}content/${data.id}/${data.img}`}/>
                                }
                            </div>
                            <div className={styles.blog_content}>
                                {
                                    isFetching && <InlineLoader/>
                                }
                                {
                                    data?.h2_titles && data?.h2_titles[0]?.title &&
                                    <div className={styles.tabs_section}>
                                        <span onClick={() => {setToggle(!toggle)}}>
                                            مشاهده لیست عناوین
                                        </span>
                                        <ul className={`${toggle ? styles.tabs_section_show : styles.tabs_section_hide}`}>
                                            {
                                                data?.h2_titles?.map((item,index) => {
                                                    return(
                                                        <li ref = {(element) => titleRef.current[index] = element} onClick={(e) => {handleScrollOnTitle(item)}}>
                                                            <div dangerouslySetInnerHTML={{__html:item.title}}/>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul>
                                    </div>
                                }
                                {data && <div dangerouslySetInnerHTML={{__html: data.content}}/>}
                            </div>
                        </Grid>
                        <Grid item md={3} xs={12}>
                            <div className={styles.blog_similar_post}>
                                {isFetching && <InlineLoader/>}
                                {data && data.categories && data.categories.map((item, index) => {
                                    return(
                                        <>

                                            {item.contents && item.contents.length > 0 &&
                                            <div className={styles.contents_title}  onClick={() => {router.push(`/blog/post/?id=${item.id}&slug=${item.slug}`,`/blog/post/${item.id}/${item.slug}`)}}>
                                                <a>
                                                    <span>{item.title}</span>
                                                </a>
                                            </div>
                                            }
                                            {
                                                item.contents.map((items , index) => {
                                                    return (
                                                        <div style={{border:index === item.contents.length - 1 && "unset" }} className={styles.contents} onClick={() => {router.push(`/blog/post/[...param]`,`/blog/post/${items.id}/${items.slug}`)}}>
                                                            <div  className={styles.contents_img}>
                                                                <div>
                                                                    {items.img &&
                                                                    <img alt={data && data.meta_title} src={`${ENV.API['IMG']}content/${items.id}/100/${items.img}`}/>
                                                                    }
                                                                </div>
                                                                {items.title}
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </>
                                    );
                                })}
                            </div>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        </Layout>
    );
};
export async function getStaticPaths() {
    const res = await new Api().get(`/blog`);
    const paths = res && res.result.contents.data.map((post) => ( { params: { param: [ post.id.toString() , post.slug ] } }))
    return {
        paths,
        fallback: "blocking",
    }
}
export async function getStaticProps({params}) {
    const res = await new Api().get(`/blog/content/${params.param[0]}`,{});
    return {
        props: {
            blogParams:res
        },
        revalidate: 1,
    }
}

export default BlogParams;





