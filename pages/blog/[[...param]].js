import React, {useEffect, useState} from 'react';
import {Box, Container, Grid} from "@material-ui/core";
import Layout from "../../components/layout/layout";
import BlogBox from "../../components/blogBox/blogBox";
import {ENV} from "../../services/env";
import Pagination from "react-js-pagination";
import {useQuery} from "react-query";
import Api from "../../services/api";
import {useRouter} from "next/router";
import styles from "../../styles/blog.module.css";
import {Helmet} from "react-helmet";
import Bread from "../../components/bread/bread";
import Loading from "../../components/loading/loading";
import Share from "../../components/share/share";
import {useSelector} from "react-redux";
import * as queryString from "querystring";




const Blog = (props) => {
    const {blog} = props
    const data = blog
    const router = useRouter()
    const categoryData = useSelector((state) => state.menu.data)
    const [expanded, setExpanded] = useState([])
    const [active, setActive] = useState(isNaN(router.query.param && router.query.param[0]) ? 1 : parseInt(router.query.param && router.query.param[0]))
    useEffect(() => {

        window.scrollTo({top: 0, behavior: 'smooth'});
    }, [active])


    useEffect(() => {
        const bread = []
        bread.push({ link:"/shop", name :"وبلاگ"})
        localStorage.setItem("bread" , JSON.stringify(bread))
    } ,[])



    const handleAddExpand = async (value) => {
        if (expanded.includes(value)) {
            const newArr = []
            const index = expanded.indexOf(value);
            await newArr.push(expanded.splice(index, 1));
            await setExpanded(newArr)
        } else {
            const newArr = [...expanded]
            await newArr.push(value);
            await setExpanded(newArr)
        }

    }
    const renderBlogTree = (props) => {
        // nodes mapping
        const treeNodes = props && props.map((item, key) => {
            // value id node
            const {id} = item;
            // label title node
            const {title} = item;
            // slug
            const {slug} = item;

            // check has child
            const hasChild = item.children.length > 0;

            // check has child is true fetch children

            const children = hasChild ? renderBlogTree(item.children) : '';
            return (
                <>
                    <div onClick={() => handleAddExpand(id)}
                         className={styles.tree_view}>
                        {hasChild === true &&
                        <img src={!expanded.includes(id) ? "/plus_icon.svg" : "/minus_icon.svg"}/>
                        }
                        <li key={key} className={styles.tree_box}>
                          <span
                              className={
                                  hasChild === true
                                      ? 'tree-parent has-child'
                                      : 'tree-parent  has-no-child'
                              }
                          >
                          </span>
                            <div onClick={(e) => {
                                e.target === e.currentTarget && handleCategoryChange(id, slug)
                            }} className={styles.tree_box_title}>{title}</div>
                            {hasChild === true && (
                                <ul
                                    style={{
                                        display: expanded.includes(id)
                                            ? 'flex'
                                            : 'none'
                                    }}
                                    className={styles.tree_children}
                                >
                                    {children}
                                </ul>
                            )}
                        </li>
                    </div>
                </>
            );
        });
        return treeNodes;
    }

    const handlePageChange = async (activePage) => {
        setActive(activePage)
        if (router.query?.param?.[0] !== undefined) {
            router.push(`/blog/${router.query.param[0]}/${router.query.param[1]}?page=${activePage}`)
        } else {
            router.push(`/blog/?page=${activePage}`)
        }
    }

    const handleCategoryChange = async (activeId , slug) => {
        setActive(1)
       router.push(`/blog/${activeId}/${slug}`)
    }

    return (
        <Layout>
            {
                data && data.result !== undefined &&
                <Helmet>
                    <title>{data && data.result.category.meta_title}</title>
                    <script type="application/ld+json">{`
                                {
                                    "@context": "https://schema.org",
                                    "@type": "BlogPosting",
                                    "name":"دیجی عطار"
                                    "mainEntityOfPage": {
                                    "@type": "WebPage",
                                        "@id": "${ENV.API['FETCH']}"
                                },
                                    "headline": "${data && data.result.category.meta_title}",
                                    "author": {
                                    "@type": "Person",
                                        "name": "digiattar"
                                },
                                    "publisher": {
                                    "@type": "Organization",
                                        "name": "digiattar",
                                    },
                                    "description": "${data && data.result.category.meta_description}"
                                }
                            `}</script>
                    <meta name='description'
                          content={data ? (data && data.result.category.meta_description && data && data.result.category.meta_description) : ''}/>
                    <link rel="canonical" href={`${ENV.API['FETCH']}`}/>
                </Helmet>
            }

            <div className={styles.blog_loading}>
                <Container>
                    <div id={"blog_head"} className={styles.blog_head}>
                        {
                            data && data.result !== undefined  && data.result.category.label ? <h1>{data && data.result !== undefined && data.result.category.label}</h1> :
                                <h1>{data && data.result !== undefined && data.result.category.title}</h1>
                        }
                        {/*{data && <Bread item={data && data.result.category} route={"blog"}/>}*/}
                    </div>
                    <Grid spacing={4} container justify="center">
                        <Grid item md={3} xs={12}>
                            <div className={styles.blog_categories}>
                                <div className={styles.blog_categories_img}/>
                                {
                                    renderBlogTree(categoryData.blog_categories)
                                }
                            </div>
                        </Grid>
                        <Grid item spacing={4} md={9} xs={12} container={true}>
                            {
                                data && data.result !== undefined && data.result.contents.data && data.result.contents.data.length === 0 ?
                                    <p className={styles.nothing_to_show}>مطلبی برای نمایش وجود ندارد </p> :
                                    data && data.result !== undefined && data.result.contents && data.result.contents.data.map((item) => {
                                        return (
                                            <Grid item md={4} sm={6} xs={12}>
                                                <BlogBox
                                                    title={item.title}
                                                    src={item.img && ENV["API"]["IMG"] + "/content/" + item.id + "/" + item.img}
                                                    des={item.content}
                                                    visitor={item.visitor}
                                                    created_at={item.created_at}
                                                    id={item.id}
                                                    slug={item.slug}
                                                />
                                            </Grid>
                                        )
                                    })
                            }
                            {
                                data && data.result !== undefined && data.result.contents.total / data.result.contents.per_page > 1 &&
                                <div className={styles.pagination}>
                                    <Pagination
                                        activePage={active}
                                        itemsCountPerPage={data.result.contents && data.result.contents.per_page}
                                        totalItemsCount={data.result.contents && data.result.contents.total}
                                        pageRangeDisplayed={5}
                                        onChange={handlePageChange}
                                    />
                                </div>
                            }
                        </Grid>
                    </Grid>
                    <style jsx>
                        {`
                           #blog_head {
                            margin: 0 auto 20px auto;
                                  }
                        `}
                    </style>
                </Container>
            </div>
        </Layout>
    );
};

export async function getServerSideProps({ query }) {
    const blog = await new Api().get(`/blog${query?.param?.[0] ? `/category/${query?.param?.[0]}` : ""}`,{
        page:query.page || 1
    });
    return {
        props: {
            blog,
        },
    };
}

export default Blog;
