import React, {useState} from 'react';
import styles from "../../styles/bmi.module.css"
import Input from "../../components/input/Input";
import Layout from "../../components/layout/layout";
import Container from "@material-ui/core/Container";
import Button from "../../components/button/button";
import Select from "../../components/select/select";
import BmiVector from "../../components/bmiVector/bmiVector";


const Index = () => {
    const [height, setHeight] = useState("");
    const [labelColor, setLabelColor] = useState("red")
    const [resultText, setResultText] = useState("")
    const [weight, setWeight] = useState("");
    const [idealWeight, setIdealWeight] = useState("");
    const [bmi, setBmi] = useState("");
    const handleResult = async (w,h) => {
        let bmiResult = await (w * 10000) / (h * h)
        setWeight(w);
        setHeight(h);
        setIdealWeight(h - 100)
        await setBmi(bmiResult)
        switch (true) {
            case bmiResult < 16.5 : {
                return (
                    await setLabelColor("red"),
                        await setResultText("دچار کمبود وزن شدید")
                )
            }
            case  bmiResult > 16.5 && 18.5 > bmiResult : {
                return (
                    await setLabelColor("var(--Third)"),
                        await setResultText(" کمبود وزن")
                )
            }
            case  bmiResult > 18.2 && 25 > bmiResult : {
                return (
                    await setLabelColor("var(--primary)"),
                        await setResultText(" عادی")
                )
            }
            case  bmiResult > 25 && 30 > bmiResult : {
                return (
                    await setLabelColor("var(--Third)"),
                        await setResultText(" اضافه وزن")
                )
            }
            case  bmiResult > 30 && 35 > bmiResult : {
                return (
                    await setLabelColor("var(--Third)"),
                        await setResultText(" چاقی نوع اول (چاقی معمولی)")
                )
            }
            case  bmiResult > 35 && 40 > bmiResult : {
                return (
                    await setLabelColor("red"),
                        await setResultText(" چاقی نوع دوم (چاقی شدید)")
                )
            }
            case  40 < bmiResult : {
                return (
                    await setLabelColor("red"),
                        await setResultText(" چاقی نوع سوم (چاقی بسیار شدید)")
                )
            }
        }
    }
    const gender = [
        {title: "زن ", id: 1},
        {title: "مرد", id: 2},
    ]
    return (
        <Layout>
            <Container>
                <div className={styles.bmi}>
                    <h2>
                        می توانید برای اندازه گیری BMI خود از فرم زیر استفاده کنید:
                    </h2>
                    <div className={styles.calculate_bmi_box}>
                        <Input
                            hasText={"لطفا قد خود را وارد کنید"}
                            data={height}
                            onchange={(e) => {
                                handleResult(weight,e)
                            }}
                            placeholder={"قد (سانتیمتر)"}
                            type={"text"}
                            width={"200px"}
                            margin={"0"}
                        />
                        <Input
                            hasText={"لطفا وزن خود را وارد کنید"}
                            data={weight}
                            onchange={(e) => {
                                handleResult(e,height)
                            }}
                            placeholder={"وزن (کیلوگرم)"}
                            type={"text"}
                            width={"200px"}
                            margin={"0"}
                        />
                        {
                            <Select
                                placeHolder={"جنسیت را انتخاب کنید"}
                                option={gender}
                                onChange={(e) => {
                                    console.log(e)
                                }}
                                width={"200px"}
                                height={"50px"}
                                fontSize={"14px"}
                                fontWeight={"unset"}
                                color={"#B0BEC5"}
                                margin={"0"}
                            />
                        }
                    </div>
                    <div className={styles.result_bmi_box}>
                        <div className={styles.result_bmi}>
                            {
                                height !== "" && weight !== "" &&
                                <span id={"result_bmi_label"} className={styles.result_bmi_label}>
                                 {resultText}
                              </span>
                            }

                            <span>شاخص توده بدنی :</span>
                            <span>{height !== "" && weight !== "" ? bmi && bmi.toFixed(2) : "-------------"}</span>
                        </div>
                        <div className={styles.result_bmi}>
                            <span> وزن ایده آل شما :</span>
                            <span>{height !== "" && weight !== "" ? Math.floor(idealWeight) + " کیلوگرم ": "-------------"}</span>
                        </div>
                        <div className={styles.result_bmi}>
                            <span>{` میزان ${weight - (idealWeight) < 0 ? " کمبود " : " اضافه "} وزن :  `}</span>
                            <span>{height !== "" && weight !== "" ? Math.abs(weight - (idealWeight))  + " کیلوگرم " : "-------------"}</span>
                        </div>
                    </div>
                    <div className={styles.bmi_btn_record}>
                        <Button
                            onClick={() => {
                                alert("aaaaa")
                            }}
                            icon={"/arrow.png"}
                            text={"ثبت در پرونده پزشکی"}
                            backgroundColor={"var(--primary)"}
                            width={"250px"}
                            padding={10}
                        />
                    </div>
                    <style jsx>
                     {`
                     #result_bmi_label {
                       background-color:${labelColor}
                      }
                    `}
                    </style>
                </div>
            </Container>
        </Layout>
    );
};

export default Index;
