import React, {useEffect, useState} from 'react';
import styles from "../styles/main.module.css"
import {Grid} from "@material-ui/core";
import Container from '@material-ui/core/Container';
import {connect, Provider, useDispatch, useSelector} from 'react-redux';
import Layout from "../components/layout/layout";
import SubMenu from "../components/subMenu/subMenu";
import Line from "../components/line/line";
import Slider from "../components/slider/slider";
import MiniBox from "../components/miniBox/miniBox";
import CategoryBox from "../components/categoryBox/categoryBox";
import Temperament from "../components/temperament/temperament";
import LatestContent from "../components/latestContent/latestContent";
import {Helmet} from "react-helmet";
import Api from "../services/api";
import Link from "next/link";
import {useRouter} from "next/router";
import {fetchSettings} from "../redux/actions";
import Image from "next/image"
const menuList = [
    {title: "گیاهان دارویی", img: "/menuPlan.png"},
    {title: "دمنوش ها", img: "/subMenu2.png"},
    {title: "عرقیات", img: "/subMenu1.png"},
    {title: "گیاهان دریایی", img: "/subMenu3.png"},
    {title: "گیاهان دریایی", img: "/subMenu2.png"},
    {title: "گیاهان دریایی", img: "/subMenu3.png"},
    {title: "گیاهان دریایی", img: "/subMenu1.png"},
    {title: "گیاهان دریایی", img: "/subMenu2.png"},
]



const Home = (props) => {
    const dispatch = useDispatch()
    const [data , setData] = useState({})
    const [supportsPWA, setSupportsPWA] = useState(false);
    const [promptInstall, setPromptInstall] = useState(null);

    const router = useRouter()
    let swiperData = props.homeData.swiperData

    let installPromp

    useEffect(() => {

        const handler = e => {
            console.log("we are being triggered :D");
            e.preventDefault();
            setSupportsPWA(true);
            setPromptInstall(e);
        };
        window.addEventListener("beforeinstallprompt", handler);

        localStorage.removeItem("bread" )

        window.addEventListener("scroll", () => {
            if (window.scrollY > 500) {
                [...document.querySelectorAll('#baner img')].map(img => img.src = img.getAttribute("data-src"))
            }
        })

        return window.addEventListener("beforeinstallprompt", handler);
    }, [])

    const fetchSetting =  () => {
         new Api().get(`/setting`)
            .then((res) => {
                dispatch(fetchSettings(res))
                setData(res)
            }).catch((error) => {
                throw error
            });
    };
    useEffect(() => {
      fetchSetting()
    },[])


    const handlePromp = ()=> {
        installPromp.prompt()
        installPromp.userChoice.then((choice) => {
            if (choice.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
            } else {
                console.log('User dismissed the A2HS prompt');
            }
        })
    }

    return (
        <Layout>
            {
                data &&
                <Helmet>
                    <title>{data?.meta_title}</title>
                    <script type="application/ld+json">{`
                                    {
                                        "@context": "https://schema.org",
                                        "@type": "Organization",
                                         "name":"دیجی عطار",
                                        "mainEntityOfPage": {
                                        "@type": "WebPage",
                                            "@id":  "${data?.backend}"
                                    },
                                        "headline": "${data?.meta_title}",
                                        "author": {
                                        "@type": "Person",
                                            "name": "digiattar",
                                      },
                                        "publisher": {
                                        "@type": "Organization",
                                            "name": "digiattar",
                                        },
                                        "description": "${data?.meta_description}",
                                    }
                                `}</script>
                    <meta name='description' content={data?.meta_description ? data?.meta_description : ''}/>
                    <link rel="canonical" href={`${data?.backend}`}/>
                </Helmet>
            }
            <Container>
                <Grid container={true}>
                    <Grid item md={3}>
                        <div onClick={() => router.push("/blog/post/202/معرفی-6-داروی-گیاهی-موثر-در-درمان-کرونا")}  className={styles.main_image_top}>
                            <Image
                                priority={true}
                                loading={"eager"}
                                width={500}
                                height={500}
                                src={"/mainImageTop.png"}
                            />
                        </div>
                    </Grid>
                    <Grid item md={9} xs={12} spacing={2}>
                        <div className={styles.slider}>
                            <Slider
                                type={"mainSlider"}
                                slidesPerView={1}
                                data={props.homeData?.sliderData?.[0]?.files}
                            />
                        </div>
                    </Grid>
                </Grid>

                <SubMenu menuList={menuList}/>
                <div className={styles.swiper}>
                    <Line  title={swiperData && swiperData[0].title}/>
                    <Slider

                        type={"CategoryBox"}
                        slidesPerView={5}
                        data={swiperData && swiperData[0].products}
                    />
                </div>
                <div className={styles.swiper}>
                    <Line title={"پیشنهاد روز"}/>
                    <Slider

                        type={"MiniBox"}
                        slidesPerView={4}
                        data={swiperData && swiperData[0].products}
                    />
                </div>
                <div className={styles.swiper}>
                    <Line  title={swiperData && swiperData[1] && swiperData[1].title}/>
                    <Slider

                        type={"CategoryBox"}
                        slidesPerView={5}
                        data={swiperData && swiperData[1] && swiperData[1].products}
                    />
                </div>
                <div id={"baner"} className={styles.baner_top}>
                    <div className={styles.baner_right}>
                        <div className={styles.baner_right_top}>
                            <Link href={"/product/1172"}>
                                <img alt={"عطاری آنلاین دیجی عطار، فروشگاه جامع گیاهان دارویی وطب سنتی"} data-src={"/baner2.jpg"} src={""}/>
                            </Link>
                        </div>
                        <div className={styles.baner_right_down}>
                            <Link href={`/product/17`}>
                                <img alt={"عطاری آنلاین دیجی عطار، فروشگاه جامع گیاهان دارویی وطب سنتی"} data-src={"/baner3.jpg"} src={""}/>
                            </Link>
                        </div>
                    </div>
                    <div className={styles.baner_left}>
                        <div className={styles.baner_left_box}>
                            <Link href={`/product/771`}>
                                <img alt={"عطاری آنلاین دیجی عطار، فروشگاه جامع گیاهان دارویی وطب سنتی"} data-src={"/baner1.jpg"} src={""}/>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className={styles.swiper}>
                    <Line  title={swiperData && swiperData[2] && swiperData[2].title}/>
                    <Slider

                        type={"CategoryBox"}
                        slidesPerView={5}
                        data={swiperData && swiperData[2] && swiperData[2].products}
                    />
                </div>
            </Container>
            <Temperament/>
            <div onClick={handlePromp}
                 className={"promp"}/>
            <Container>
                {
                    swiperData && swiperData.map((item, index) => {
                        if (index > 2) {
                            return (
                                <div className={styles.swiper}>
                                    <Line title={item.title}/>
                                    <Slider

                                        type={"CategoryBox"}
                                        slidesPerView={index}
                                        data={item.products}
                                    />
                                </div>
                            )
                        }
                    })
                }
                <div id={"baner"} className={styles.middle_baner}>
                    <img alt={"عطاری آنلاین دیجی عطار، فروشگاه جامع گیاهان دارویی وطب سنتی"} src={''} data-src={"/banermiddle.png"}/>
                </div>
                        <div className={styles.latest_content}>
                            <LatestContent data={ props?.homeData?.blogData?.result?.contents}/>
                        </div>
            </Container>
        </Layout>
    );
};

export async function getStaticProps() {
    const sliderData = await new Api().get(`/slider`)
        .then((res) => {
            if (typeof res !== "undefined") {
                return res
            } else {
                return null
            }
        }).catch((err) => {
            console.log(" :" + err)
        })
    const blogData = await new Api().get(`/blog?page=1&limit=4`)
        .then((res) => {
            if (typeof res !== "undefined") {
                return res
            } else {
                return null
            }
        }).catch((err) => {
            console.log(" :" + err)
        })

    const swiperData = await new Api().get(`/shop/products/swiper`)
        .then((res) => {
            if (typeof res !== "undefined") {
                return res
            } else {
                return null
            }
        })
        .catch((err) => {
            console.log(" :" + err)
        })
    return {
        props: {
            homeData: {sliderData, blogData, swiperData},
        },
        revalidate: 1,
    }
}

export default Home;



