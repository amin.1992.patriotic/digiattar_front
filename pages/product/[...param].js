import React, {createRef, useEffect, useState} from "react";
import styles from "../../styles/product.module.css";
import Layout from "../../components/layout/layout";
import {Container, Fab} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Slider from "../../components/slider/slider";
import Select from "../../components/select/select";
import Line from "../../components/line/line";
import Button from "../../components/button/button";
import {ENV} from "../../services/env";
import {QueryCache, useQuery} from "react-query";
import {dehydrate} from "react-query/hydration";
import {useRouter} from "next/router";
import Loading from "../../components/loading/loading";
import {useDispatch, useSelector} from "react-redux";
import Api from "../../services/api";
import {toast} from "react-toastify";
import {prevData, prevDataCount, loginModalToggle} from "../../redux/actions";
import {Helmet} from "react-helmet";
import Bread from "../../components/bread/bread";
import {separate} from "../../services/separate";
import Share from "../../components/share/share";
import {GlassMagnifier} from "react-image-magnifiers";
import { useAmp } from 'next/amp'

// export const config = { amp: 'true' }


const Product = (props) => {
    console.log(props)
    const isAmp = useAmp()

    const router = useRouter();
    const dispatch = useDispatch();
    const category = props && props.shopParams && props.shopParams.res && props.shopParams.res.categories
    const isLogin = useSelector((state) => state.loginReducer.login);
    const user = useSelector((state) => state.loginReducer.user);
    const cart = useSelector((state) => state.cart);
    let prefix = ENV.API["IMG"] + 'product/' + router.query.param[0] + '/'
    const token = useSelector((state) => state.loginReducer.token);


    const [form, setForm] = useState({});
    const [n_form, setN_form] = useState([]);
    const [open, setOpen] = React.useState(false); // price dialog
    const [multiplePrice, setMultiplePrice] = React.useState([]); // contain price parameter rows
    const productData = props.shopParams.res;
    const [loading, setLoading] = useState(false);
    const [loadingBox, setLoadingBox] = useState(false);
    const [loadingParams, setLoadinParams] = useState(false);
    const [imgLoading, setImgLoading] = useState(false);
    const productGalleryData = productData && productData.product && productData.product.files;
    const [selectTab, setSelectTab] = useState(1);
    const [galleryIndex, setGalleryIndex] = useState(0);
    const [modalGallery, setModalGallery] = useState(false);
    const [allProductType, setAllProductType] = useState([]);
    const [priceState, setPriceState] = useState([]);
    const [productTypes, setProductTypes] = useState([]);
    const [productTypesData, setProductTypesData] = useState([]);
    const [breadItem, setBreadItem] = useState([]);

    useEffect(() => {
        fetchProductTypes()
        if (typeof router.query.param[1] === "undefined") {
            router.push(`/product/[...param]?id=${productData.product.id}&slug=${productData.product.slug}`, `/product/${productData.product.id}/${productData.product.slug}`)
        }
        window.scrollTo({top: 0, behavior: "smooth"});
    }, [productData.product.id]);


    useEffect(() => {
        handleChangeProductTypes()
    }, []);


    const stripHtml = (html) => {
        if (typeof window !== "undefined") {
            let tmp = document.createElement("DIV");
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        }

    }
    const handleScroll = (select) => {
        if (typeof window !== undefined) {
            const tab = document.querySelectorAll("#tab")
            tab && tab[select] && tab[select].scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"})
        }
    }
    const fetchProductTypes = async () => {

        const bread = []
        const item = await JSON.parse(localStorage.getItem("bread"))
        for (let i = 0; i < (item !== null ? item.length : 0); i++) {
            if (!item[i].link.includes("product"))
                bread.push(item[i])
        }
        if (item === null) {
            bread.push({
                link: `/shop/category/${productData && productData.product.categories[0].id}`,
                name: productData && productData.product.categories[0].title
            })
        }

        setLoadinParams(true)
        await new Api().get(`/shop/products/${router.query.param[0]}/priceTypes`, {})
            .then((res) => {
                if (typeof res !== "undefined") {
                    bread.push({link: `/product/${productData.product.id}`, name: productData.product.title})
                    localStorage.setItem("bread", JSON.stringify(bread))
                    setBreadItem(bread)
                    setProductTypesData(res)
                    setLoadinParams(false)
                }
            }).catch((err) => {
                setLoadinParams(false)
            })
    }
    const handleChangeProductTypes = (id) => {
        new Api().post('/shop/products/' + router.query.param[0] + '/pins', id ? {selected:[id]} : {selected:[]}, false)
            .then((resp) => {
                if (typeof resp != "undefined") {
                    if (resp.result?.length > 0) {
                        handleParameter(resp.result)
                        setLoading(false);

                    } else {
                        setLoading(false);
                        setProductTypesData([])
                        toast.error("محصول با مشخصات انتخابی موجود نمی باشد .")
                    }
                }
            });
    };

    const handleParameter = (parameter) => {
        let arr = []
        let le = parameter?.[0]?.selected?.length
        parameter?.forEach((item) => {
            for (let i = 0; i < le; i++) {
                arr.push(item.selected[i])
            }
        })
        const filter = Array.from(new Set(arr.map(a => a?.id)))
            .map(id => {
                return arr?.find(a => a?.id === id)
            })
        let split = filter.reduce((acc, obj) => {
            let key = obj?.["parent"]
            if (!acc[key]) {
                acc[key] = []
            }
            acc[key].push(obj)
            return acc
        }, {})
        setPriceState(Object.entries(split))
    }

    const addToCards = (typeId) => {
        setLoading(true);
        if (!cart.data.includes(typeId)) {
            new Api()
                .post("/card", {
                    count: 1,
                    id: typeId,
                })
                .then((res) => {
                    setLoading(false);
                    if (typeof res !== "undefined") {
                        if (res.status) {
                            router.push("/cart")
                            toast.success("با موفقیت به سبد خرید اضافه شد");
                            dispatch(prevDataCount(res.card));
                            dispatch(prevData(res.card));
                        } else {
                            if (res.msg === "Column 'product_pins_id' cannot be null") {
                                toast.info("محصول ناموجود است ");
                            } else {
                                toast.error(res.msg);
                            }
                        }
                    }
                })
                .catch((err) => {
                    setLoading(false);
                    toast.error(err);
                });
        }
    };

    const selectTabs = () => {
        let char = (/[ا-ی^]/);
        return (
            <>
                <div className={styles.product_specifications}>
                    <div id={"tab"} className={styles.product_specifications_title}>مشخصات</div>
                    {productData && productData.attributes.map((item) => {
                        return (
                            <div className={styles.product_specifications_detail}>
                                <h2 className={styles.product_specifications_property}>
                                    {item.title + " " + productData.product.title}
                                </h2>
                                {
                                    item.title?.includes("نام های دیگر") ?
                                        <h2
                                            style={{direction: char.test(item.value && item.value.toString()) || char.test(item.attr_selected && item.attr_selected.toString()) ? "rtl" : "ltr" , fontWeight:"100"}}
                                            className={styles.product_specifications_value}
                                            dangerouslySetInnerHTML={{__html: item.attr_selected || item.value}}/>
                                        :
                                        <div
                                            style={{direction: char.test(item.value && item.value.toString()) || char.test(item.attr_selected && item.attr_selected.toString()) ? "rtl" : "ltr"}}
                                            className={styles.product_specifications_value}
                                            dangerouslySetInnerHTML={{__html: item.attr_selected || item.value}}/>
                                }
                            </div>
                        );
                    })}
                </div>
                {
                    productData.product.content &&
                    <>
                        <div id={"tab"} className={styles.product_specifications_title}>توضیحات تکمیلی</div>
                        <div className={styles.further_details}>
                            <div
                                className="Text"
                                dangerouslySetInnerHTML={{__html: productData.product.content}}
                            />
                        </div>
                    </>
                }

            </>
        );
    };
    return (
        <>
            <Layout>
                <Helmet>
                    <title>{productData && productData.product && productData.product.meta_title}</title>
                    <script type="application/ld+json">{`
                                    {
                                    "@context": "https://schema.org",
                                    "@type": "product",
                                    "name":"${productData && productData.product && productData.product.title}",
                                    "mpn":${productData.product.id},
                                    "sku":${productData.product.id},
                                    "image":["${ENV.API["IMG"] + 'product/' + router.query.param[0] + '/' + (productGalleryData[galleryIndex] && productGalleryData[galleryIndex].file)}"]
                                    "mainEntityOfPage": {
                                     "@type": 
                                        "ListItem",
                                        "position":1,
                                        "name":"${productData && productData.product && productData.product.title}",
                                        "item":"${"https://digiattar.com//product/" + productData.product.id}",
                                     },
                                     "offers": {
                                            "@type": "AggregateOffer",
                                            "priceCurrency": "IRR",
                                            "offerCount": ${productTypesData && productTypesData[0] && productTypesData[0].package_count},
                                            "offers": {
                                                "@type": "Offer",
                                                "priceCurrency": "IRR",
                                                 "price":${productTypesData && productTypesData[0] && separate(productTypesData[0].price)} ,
                                                "itemCondition": "https://schema.org/UsedCondition",
                                                "availability": "https://schema.org/InStock",
                                                "seller": {
                                                    "@type": "Organization",
                                                    "name": "دیجی عطار"
                                                }
                                            }
                                        },
                                    "headline": "${productData && productData.product && productData.product.title}",
                                    "author": {
                                    "@type": "Person",
                                        "name": "دیجی عطار",
                                     },
                                      "reviewRating": {
                                            "@type": "Rating",
                                              "ratingValue": ${(Math.random() + 4).toFixed(1)},
                                              "ratingCount": ${Math.floor(Math.random() * 10 + 12)},
                                              "reviewCount": ${Math.floor(Math.random() * 20 + 123)}"
                                          },
                                    "publisher": {
                                    "@type": "Organization",
                                        "name": "دیجی عطار",
                                      
                                    },
                                    "description": "${productData && productData.product && productData.product.meta_description}"
                                }
                            `}</script>

                    <meta
                        name="description"
                        content={
                            productData
                                ? productData.product && productData.product.meta_description &&
                                productData.product.meta_description
                                : ""
                        }
                    />
                    <link rel="canonical" href={`${ENV.API["FETCH"]}`}/>
                </Helmet>

                <Container>
                    <div className={styles.product}>
                        <Bread item={breadItem}/>
                        {/*<Bread />*/}

                        <Grid spacing={2} justify="center" container={true}>
                            <Grid item md={4} xs={12}>
                                <div  className={styles.product_gallery}>
                                    <div
                                        onClick={(e) => {
                                            e.target === e.currentTarget &&
                                            setModalGallery(false);
                                        }}
                                        className={`${styles.modal_gallery} , ${
                                            modalGallery ? styles.open_modal : styles.close_modal
                                        }`}
                                    >
                                        <div className={styles.modal_gallery_main}>
                                            <Grid spacing={2} container={true}>
                                                <Grid item md={4} xs={12}>
                                                    <div className={styles.modal_gallery_main_image}>
                                                        {
                                                            productGalleryData && productGalleryData[galleryIndex] &&
                                                            <img
                                                                onLoad={() => {
                                                                    setImgLoading(false)
                                                                }}
                                                                src={prefix + "/500/" + productGalleryData[galleryIndex].file}
                                                            />
                                                        }
                                                    </div>
                                                </Grid>
                                                <Grid item md={6} xs={12}>
                                                    <div
                                                        id="gallery"
                                                        className={styles.modal_gallery_list_img}
                                                    >
                                                        {productGalleryData &&
                                                        productGalleryData.map((item, index) => {
                                                            return (
                                                                <img
                                                                    alt={item.alt}
                                                                    onClick={() => {
                                                                        setImgLoading(true);
                                                                        setGalleryIndex(index);
                                                                    }}
                                                                    src={
                                                                        prefix + "/100/" + item.file
                                                                    }
                                                                />
                                                            );
                                                        })}
                                                    </div>
                                                    <style jsx>
                                                        {`
                                                          #gallery
                                                          img:nth-child(${galleryIndex + 1}) {
                                                            border: 5px solid var(--primary);
                                                          }
                                                        `}
                                                    </style>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    </div>
                                    <div className={styles.product_gallery_main_image}>
                                        {productGalleryData && productGalleryData[galleryIndex] &&
                                        <GlassMagnifier
                                            circle={true}
                                            magnifierSize={200}
                                            alt={productGalleryData[galleryIndex].alt}
                                            imageSrc={prefix +
                                            "/400/" +
                                            productGalleryData[galleryIndex].file}
                                            imageAlt="Example"
                                            largeImageSrc={prefix + "/500/" + productGalleryData[galleryIndex].file}
                                        />
                                        }
                                    </div>
                                    <div style={{width: productGalleryData && productGalleryData?.length < 4 && "100%"}}
                                         className={styles.product_gallery_list_image}>
                                        {productGalleryData && productGalleryData.length > 4 && (
                                            <span
                                                onClick={() => {
                                                    setModalGallery(true);
                                                }}
                                            >
                                                        مشاهده همه
                                                      </span>
                                        )}
                                        {productGalleryData &&
                                        productGalleryData.map((item, index) => {
                                            if (index < 4) {
                                                return (
                                                    <img
                                                        alt={item.alt}
                                                        onClick={() => {

                                                            setGalleryIndex(index);
                                                        }}
                                                        src={prefix + "/100/" + item.file}
                                                    />
                                                );
                                            }
                                        })}
                                    </div>
                                </div>
                            </Grid>

                            <Grid item md={5} xs={12}>
                                <div className={styles.product_option}>
                                    <h1>{productData && productData.product && productData.product.title}</h1>
                                </div>
                                <p className={styles.product_main_about}>{productData && productData.product && productData.product.short_content}</p>
                                {
                                    <div className={'price-parameter-box'}>
                                        <Grid container={true} spacing={1}>

                                            {priceState?.map((parameter, index) => {
                                                return (
                                                    parameter[0] !== "undefined" &&
                                                    <Grid xs={12} item={true}>
                                                        {
                                                            <Select
                                                                title={parameter?.[1].title}
                                                                value={n_form[parameter?.[1].id] || productTypesData && productTypesData[0] && productTypesData[0]?.price_parameters[index]?.id}
                                                                option={parameter?.[1]}
                                                                onChange={(e) => handleChangeProductTypes(e)}
                                                                width={"200px"}
                                                                height={"50px"}
                                                                fontSize={"14px"}
                                                                fontWeight={"unset"}
                                                                color={"#B0BEC5"}
                                                                margin={"0"}
                                                            />
                                                        }
                                                    </Grid>
                                                );
                                            })}
                                        </Grid>
                                    </div>
                                }
                            </Grid>
                            {
                                <Grid item md={3} xs={12}>
                                    <div className={styles.price_box}>
                                        <Share position={"absolute"} width={"30px"} id={router.query.param[0]}
                                               phone={user.phone} email={user.email}/>
                                        {
                                            <>
                                                {
                                                    (productTypesData && productTypesData[0] && productTypesData[0].price !== 0 && productTypesData && productTypesData && productTypesData.price !== 0) &&
                                                    <del className={styles.price_number}>
                                                        {(parseInt(productTypesData && productTypesData[0] && productTypesData[0].price) === parseInt(productTypesData && productTypesData.off_price || productTypesData && productTypesData[0] && productTypesData[0].off_price)) ? "" : separate(productTypesData && productTypesData.price || productTypesData && productTypesData[0] && productTypesData[0].price) + " تومان "}
                                                    </del>
                                                }
                                                {
                                                    loadingBox ? <Loading position={"absolute"}/> :
                                                        <p className={styles.price_number}>
                                                            {(productTypesData && productTypesData[0] && productTypesData[0].package_count < 1 || productTypesData && productTypesData && productTypesData.package_count < 1) ? "ناموجود" : productTypesData && productTypesData?.length !== 0 ? separate(productTypesData && productTypesData.off_price || productTypesData && productTypesData[0] && productTypesData[0].off_price) + " تومان " : "ناموجود"}
                                                        </p>
                                                }
                                                <ul className={styles.attributes_in_box_price}>
                                                    {productData && productData.attributes && productData.attributes.map((item) => {
                                                            let value = item.value || item.attr_selected
                                                            return (
                                                                (item.id === 29 || item.id === 38 || item.id === 27 || item.id === 36) && value &&
                                                                <div>
                                                                    {item.title + " " + productData.product.title + " : "}
                                                                    {stripHtml(value) && stripHtml(value).substr(0, 50)}
                                                                    {value && value?.length > 50 ? " ... " : ""}
                                                                </div>
                                                            )
                                                        }
                                                    )}
                                                </ul>
                                                <Button
                                                    disable={(productTypesData && productTypesData[0] && productTypesData[0].package_count < 1 || productTypesData && productTypesData && productTypesData.package_count < 1)}
                                                    onClick={(e) => {
                                                        isLogin
                                                            ? addToCards(productTypesData.id || productTypesData && productTypesData[0] && productTypesData[0].id)
                                                            : dispatch(loginModalToggle(true));
                                                    }}
                                                    icon={"/arrow.png"}
                                                    text={"افزودن به سبد خرید"}
                                                    backgroundColor={"rgba(0,164,179,.7)"}
                                                    width={"100%"}
                                                    padding={10}
                                                    margin={0}
                                                    loading={loading}
                                                />
                                            </>
                                        }
                                    </div>
                                </Grid>
                            }
                            <Grid item xs={12}>
                                <div className={styles.product_tabs}>
                                    <div
                                        onClick={() => {
                                            handleScroll(0)
                                        }}
                                    >
                                        <div>مشخصات</div>
                                    </div>
                                    <div
                                        onClick={() => {
                                            handleScroll(1)
                                        }}
                                    >
                                        <div>توضیحات تکمیلی</div>
                                    </div>
                                </div>
                                {selectTabs()}
                            </Grid>
                        </Grid>
                    </div>
                    {productData.new_related_product?.length > 0 && (
                        <>
                            <Line title={"محصولات مرتبط "}/>
                            <Slider
                                type={"CategoryBox"}
                                slidesPerView={5}
                                data={productData.new_related_product}
                            />
                        </>
                    )}
                </Container>
            </Layout>
        </>
    );
};

export async function getStaticPaths() {
    const res = await new Api().get(`/shop`);
    const paths = await res && res.products.data.map((post) => ({params: {param: [post.id.toString(), post.slug]}}))

    return {
        paths,
        fallback: "blocking",
    }
}

export async function getStaticProps({params}) {
    const res = await new Api().get(`/shop/products/${params.param[0]}`, {})
        .then((res) => {
            console.log(res)
            if (typeof res !== "undefined") {
                return res
            } else {
                return null
            }
        })
        .catch((err) => {
        })
    console.log({"res": res})

    return {
        props: {
            shopParams: {res}
        },
        revalidate: 1,
    }
}


export default Product;
