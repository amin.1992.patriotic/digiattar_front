import React, {useEffect, useState} from 'react';
import styles from "../../styles/cartDetail.module.css"
import Layout from "../../components/layout/layout";
import {Grid} from "@material-ui/core";
import Container from '@material-ui/core/Container';
import StageBar from "../../components/stageBar/stageBar";
import RadioSelect from "../../components/radioSelect/radioSelect";
import Button from "../../components/button/button";
import {shoppingLine} from "../../redux/actions";
import {connect, useDispatch, useSelector} from "react-redux";
import Api from "../../services/api";
import Input from "../../components/input/Input";
import Selector from "../../components/selector/selector";
import {useRouter} from "next/router";
import {toast} from "react-toastify";
import Loading from "../../components/loading/loading";
import {order} from "../../redux/actions"
import Radio from "../../components/radio/radio";
import PostRadio from "../../components/postRadio/postRadio";


const Address = () => {
    const isLogin = useSelector((state) => state.loginReducer.login)
    const post = useSelector(state => state.setting.data.forwardings)
    const [loading, setLoading] = useState(false)
    const [loadingAddress, setLoadingAddress] = useState(false)
    const [loadingEditAddress, setLoadingEditAddress] = useState(false)
    const [address, setAddress] = useState("")
    const [addressId, setAddressId] = useState("")
    const [regionsId, setRegionsId] = useState("")
    const [postTypeId, setPostTypeId] = useState(1)
    const [name, setName] = useState("")
    const [mobile, setMobile] = useState("")
    const [nationalCode, setNationalCode] = useState("")
    const [postalCode, setPostalCode] = useState("")
    const [data, setData] = useState()
    const [regions, setRegions] = useState([]);
    const [sumPrice, setSumPrice] = useState();
    const [mainLoading, setMainLoading] = useState(false);
    const [sumOffPrice, setSumOffPrice] = useState();
    const [addressModal, setAddressModal] = useState(0);
    const [token, setToken] = useState(null);
    const [id, setId] = useState(null);
    const [wrapInput, setWrapInput] = useState(50);
    const router = useRouter();
    const dispatch = useDispatch()
    const productList = useSelector(state => state.cart.prevData)
    const modalHeight = 2000

    useEffect(() => {
        dispatch(shoppingLine("step_two"))
        if (isLogin) {
            new Api().get("/region", {})
                .then((res) => {
                    if (typeof res !== "undefined") {
                        setRegions(res)
                    }
                })
                .catch((err) => {
                    toast.error(err);
                })
            registeredAddresses()
            window.addEventListener("resize", (e) => {
                if (e.target.innerWidth < 600) {
                    setWrapInput(100)
                } else {
                    setWrapInput(50)
                }
            })
        }
    }, [isLogin])


    const registeredAddresses = () => {
        setMainLoading(true)
        new Api().get("/address", {})
            .then((res) => {
                setMainLoading(false)
                if (typeof res !== "undefined") {
                    setData(res)
                    if (res && res.length === 0) {
                        setAddressModal(modalHeight)
                    }
                }
            })
            .catch((err) => {
                toast.error(err);
                setMainLoading(false)

            })
    }
    useEffect(() => {
        let getSumPrice = productList && productList.reduce(function (prev, cur) {
            return prev + cur.price * cur.count;
        }, 0);
        let getSumOffPrice = productList && productList.reduce(function (prev, cur) {
            return prev + cur.off_price * cur.count;
        }, 0);
        setSumPrice(getSumPrice)
        setSumOffPrice(getSumOffPrice)
    }, [])

    const verificatin = () => {
        if (regionsId === "") {
            toast.error("لطفا منطقه خود را انتخاب کنید");
            return false
        }
        if (address === "") {
            toast.error("لطفا آدرس خود را انتخاب کنید");
            return false
        }
        if (name === "") {
            toast.error("لطفا نام خود را انتخاب کنید");
            return false
        }
        if (mobile === "") {
            toast.error("لطفا موبایل خود را انتخاب کنید");
            return false
        }
        if (mobile && mobile.length !== 11) {
            toast.error(" شماره موبایل اشتباه است");
            return false
        }
        if (mobile && isNaN(mobile)) {
            toast.error(" شماره موبایل اشتباه است");
            return false
        }
        if (mobile && parseInt(mobile.charAt(0)) !== 0) {
            toast.error(" شماره موبایل با صفر شروع می شود");
            return false
        }
        if (mobile && parseInt(mobile.charAt(1)) !== 9) {
            toast.error(" رقم دوم شماره موبایل 9 است");
            return false
        }
        if (postalCode && postalCode.length !== 10 && postalCode && postalCode.length !== 0) {
            toast.error(" کد پستی باید ده رقمی باشد");
            return false
        }
        return true
    }

    const handleCalculateAddress = (id) => {
        let obj = {
            address_token:id,
        }
        new Api().get(`/forwarding/calculate/1` , obj )
            .then((res) => {
                if (typeof res !== "undefined") {
                    if(res.status) {
                    } else {
                        toast.error(res.message);
                    }
                }
            })
            .catch((err) => {
                toast.error(err);
            })
    }

    const newAddress = () => {
        if (!verificatin()) {
            return false
        }
        setLoadingAddress(true)
        let obj = {
            "postal_code": postalCode,
            "reciver_name": name,
            "reciver_mobile": mobile,
            "reciver_national_code": nationalCode,
            "region_id": regionsId,
            "main": address,
        }
        new Api().post(`/address`, obj )
            .then((res) => {
                setLoadingAddress(false)
                if (typeof res !== "undefined") {
                    if(res.status) {
                        toast.success("آدرس شما با موفقیت ثبت گردید");
                        registeredAddresses()
                    } else {
                        toast.error(res.message);
                    }
                }
            })
            .catch((err) => {
                setLoadingAddress(false)
                toast.error(err);
            })
    }


    const editAddress = () => {
        if (!verificatin()) {
            return false
        }
        setLoadingEditAddress(true)
        let obj = {
            "postal_code": postalCode,
            "reciver_name": name,
            "reciver_mobile": mobile,
            "reciver_national_code": nationalCode,
            "region_id": regionsId,
            "main": address,
            "token":token,
            "id":id,
        }

        new Api().put(`/address/${token}`, obj , false)
            .then((res) => {
                setLoadingEditAddress(false)
                if (typeof res !== "undefined") {
                    if(res.status) {
                        toast.success("آدرس شما با موفقیت ثبت گردید");
                        registeredAddresses()
                    } else {
                        toast.error(res.message);
                    }
                }
            })
            .catch((err) => {
                setLoadingEditAddress(false)
                toast.error(err);
            })
    }


    const handlePush = () => {
        setLoading(true)
        if (addressId === "") {
            toast.error("لطفا آدرس خود را انتخاب کنید");
            setLoading(false)
            return false
        }
        new Api().post(`/order`, {
            "address_id": addressId,
            "forwarding_type_id":1
        })
            .then((res) => {
                setLoading(false)
                if (typeof res !== "undefined") {
                    if (res.status) {
                        dispatch(order(res))
                        router.push(`/invoice/${res.token}`)
                    }
                }
            })
            .catch((err) => {
                setLoading(false)
                toast.error(err);
            })
    }

    const handleDelete = (id) => {
        new Api().delete(`/address/${id}`, {},false)
            .then((res) => {
                if (typeof res !== "undefined") {
                    if(res.status) {
                        toast.success("آدرس شما با موفقیت حذف گردید");
                        registeredAddresses()
                    } else {
                        toast.error(res.message);
                    }
                }
            })
            .catch((err) => {
                toast.error(err);
            })
    }

    const handleEdit = (e) => {
        setAddressModal(modalHeight)
        if(typeof window !== "undefined") {
            window.scrollTo({behavior:"smooth" , top:0})
        }
       let editAddress =  data?.find((item) => {
            return item.token === e
        })
        setPostalCode(editAddress.postal_code)
        setName(editAddress.reciver_name)
        setMobile(editAddress.reciver_mobile)
        setNationalCode(editAddress.reciver_national_code)
        setRegionsId(JSON.parse(editAddress.regions).at(-1).id)
        setAddress(editAddress.main)
        setToken(editAddress.token)
        setId(editAddress.id)

    }

    return (

        <Layout>
            <Container>
                {
                    isLogin ?
                        <>
                            <StageBar/>
                            <Grid container spacing={3}>
                                <Grid xs={12} item>
                                    {/*<div className={styles.discount_code}>*/}
                                    {/*    <span className={styles.discount_code_btn}>ثبت کد</span>*/}
                                    {/*    <input placeholder={"کد تخفیف را وارد کنید"}/>*/}
                                    {/*</div>*/}
                                    <div className={styles.option}>
                                        <div onClick={() => {
                                            addressModal !== modalHeight ? setAddressModal(modalHeight) : setAddressModal(0)
                                        }}>
                                            افزودن آدرس جدید
                                            <div className={styles.add_address}/>
                                        </div>
                                        <div id="address_modal" className={styles.modal_address}>
                                            {
                                                regions.length > 0 && <Selector selector={regions} onChange={(e) => {
                                                    setRegionsId(e)
                                                }}/>
                                            }
                                            <Input
                                                hasText={"آدرس دریافت کننده را وارد کنید"}
                                                data={address}
                                                onchange={(e) => {
                                                    setAddress(e)
                                                }}
                                                placeholder={"آدرس دریافت کننده را وارد کنید"}
                                                type={"text"}
                                            />
                                            <Input
                                                hasText={"نام دریافت کننده را وارد کنید"}
                                                data={name}
                                                onchange={(e) => {
                                                    setName(e)
                                                }}
                                                placeholder={"نام دریافت کننده را وارد کنید"}
                                                type={"text"}
                                                width={`${wrapInput}%`}
                                            />
                                            <Input
                                                hasText={"کد ملی دریافت کننده را وارد کنید"}
                                                data={nationalCode}
                                                onchange={(e) => {
                                                    setNationalCode(e)
                                                }}
                                                placeholder={"کد ملی دریافت کننده را وارد کنید"}
                                                type={"text"}
                                                width={`${wrapInput}%`}
                                            />
                                            <Input
                                                hasText={"شماره تماس دریافت کننده را وارد کنید"}
                                                data={mobile}
                                                onchange={(e) => {
                                                    setMobile(e)
                                                }}
                                                placeholder={"شماره تماس دریافت کننده را وارد کنید"}
                                                type={"text"}
                                                width={`${wrapInput}%`}
                                            />
                                            <Input
                                                hasText={"کد پستی دریافت کننده را وارد کنید"}
                                                data={postalCode}
                                                onchange={(e) => {
                                                    setPostalCode(e)
                                                }}
                                                placeholder={"کد پستی دریافت کننده را وارد کنید"}
                                                type={"text"}
                                                width={`${wrapInput}%`}
                                            />
                                           <div style={{display:"flex"}}>
                                               <div style={{marginLeft:"5px"}} className={styles.address_btn}>
                                                   <Button
                                                       onClick={newAddress}
                                                       backgroundColor={"var(--primary)"}
                                                       text={"ثبت اطلاعات جدید"}
                                                       icon={"/save_icon.png"}
                                                       width={"190px"}
                                                       padding={10}
                                                       margin={0}
                                                       loading={loadingAddress}
                                                   />
                                               </div>
                                               <div className={styles.address_btn}>
                                                   <Button
                                                       onClick={editAddress}
                                                       backgroundColor={"var(--Third)"}
                                                       text={"ویرایش اطلاعات"}
                                                       icon={"/save_icon.png"}
                                                       width={"190px"}
                                                       padding={10}
                                                       margin={0}
                                                       loading={loadingEditAddress}
                                                   />
                                               </div>
                                           </div>
                                        </div>
                                    </div>
                                    {
                                        data && data.length > 0 &&
                                        <>
                                            <div className={styles.selection}>
                                                <span>انتخاب آدرس</span>
                                                {mainLoading ? <Loading position={"absolute"}/> : data && data.map((item, index) => {
                                                    return (
                                                        <RadioSelect
                                                            onEdit = {(e) => {
                                                                handleEdit(e)
                                                            }}
                                                            onDelete = {(e) => {
                                                                handleDelete(e)
                                                            }}
                                                            onChange={(e) => {
                                                                setAddressId(e)
                                                                handleCalculateAddress(e)
                                                            }}
                                                            address={item.main && item.main}
                                                            city={item.regions}
                                                            name={"address"}
                                                            id={item.token}
                                                            postalCode={item.postal_code}
                                                            reciverMobile={item.reciver_mobile}
                                                            reciverName={item.reciver_name}
                                                        />
                                                    );
                                                })}
                                            </div>
                                            <div className={styles.selection}>
                                                <span>انتخاب نحوه ارسال</span>
                                                {
                                                    post?.map((item) => {
                                                       return(
                                                           <PostRadio
                                                               onChange={(e) => {
                                                               setPostTypeId(e)
                                                              }}
                                                             item = {item}
                                                           />
                                                       )
                                                    })
                                                }
                                            </div>
                                        </>

                                    }
                                    {
                                        data && data.length > 0 && <Button
                                            onClick={() => {
                                                handlePush()
                                            }}
                                            backgroundColor={"var(--primary)"}
                                            text={"تکمیل فرآیند خرید"}
                                            icon={"/save_icon.png"}
                                            width={"250px"}
                                            padding={10}
                                            margin={0}
                                            loading={loading}
                                        />
                                    }
                                    <style jsx>
                                        {`
                                           #address_modal {
                                           max-height : ${addressModal}px
                                      }
                            `}
                                    </style>
                                </Grid>
                            </Grid>
                        </> :
                        <span className="pls_login">لطفا برای مشاهده سبد خرید وارد سایت شوید </span>
                }
            </Container>
        </Layout>
    );
};


export default Address;
