import React, {useEffect, useState} from 'react';
import styles from "../../styles/cartIndex.module.css"
import Container from "@material-ui/core/Container";
import StageBar from "../../components/stageBar/stageBar";
import {Grid} from "@material-ui/core";
import Button from "../../components/button/button";
import Layout from "../../components/layout/layout";
import Api from "../../services/api";
import {ENV} from "../../services/env";
import {useDispatch, useSelector} from "react-redux";
import {prevDataCount, prevData, shoppingLine} from "../../redux/actions";
import {toast} from "react-toastify";
import {useRouter} from "next/router";
import {separate} from "../../services/separate"
import Loading from "../../components/loading/loading";

const Index = (props) => {
    const router = useRouter()
    const [sumPrice, setSumPrice] = useState();
    const [sumOffPrice, setSumOffPrice] = useState();
    const [mainLoading, setMainLoading] = useState(false);
    const dispatch = useDispatch()
    const isLogin = useSelector((state) => state.loginReducer.login)
    useEffect(() => {
        dispatch(shoppingLine("step_one"))
    }, [])
    const data = useSelector(state => state.cart.prevData)
    useEffect(() => {
        let getSumPrice = data && data.length > 0 && data.reduce(function (prev, cur) {
            if((+cur.count <= cur.inventory) && (cur.last_price === cur.off_price))
            return prev + cur.price * cur.count;
        }, 0);
        let getSumOffPrice = data && data.length > 0 && data.reduce(function (prev, cur) {
            if((+cur.count <= cur.inventory) && (cur.last_price === cur.off_price))
            return prev + cur.off_price * cur.count;
        }, 0);
        setSumPrice(getSumPrice ? getSumPrice : 0)
        setSumOffPrice(getSumOffPrice ? getSumOffPrice : 0)
    }, [data])


    const deleteProduct = (id) => {
        setMainLoading(true)
        new Api().delete(`/card/${id}`)
            .then((res) => {
                setMainLoading(false)
                if (typeof res !== "undefined") {
                    if (res.status) {
                        dispatch(prevDataCount(res.card))
                        dispatch(prevData(res.card))
                    } else {
                        toast.info(res.msg);
                    }
                }
            })
            .catch((err) => {
                toast.error(err);
                setMainLoading(false)
            })
    }

    const increase = (count, id) => {
        setMainLoading(true)
        new Api().post("/card", {
            count: count,
            id: id,
        })
            .then((res) => {
                setMainLoading(false)
                if (typeof res !== "undefined") {
                    if (res.status) {
                        toast.success(" سبد خرید به روز رسانی شد");
                        dispatch(prevData(res.card))
                    } else {
                        if (res.msg === "Request more inventory") {
                            toast.info("موجودی انبار کافی نیست");
                        } else {
                            toast.info(res.msg);
                        }
                    }
                }
            })
            .catch((err) => {
                setMainLoading(false)
                toast.error(err);
            })
    }

    return (
        <Layout>
            <Container>
                {
                    isLogin ?
                        <>
                            <StageBar/>
                            <Grid container spacing={3}>
                                <Grid md={9} xs={12} item>
                                    <div className={styles.products_list}>
                                        {
                                            data && data.length > 0 && <h1>لیست محصولات</h1>
                                        }
                                        {mainLoading && <Loading position={"absolute"}/>}
                                        {

                                            data && data.length > 0 ?
                                                data && data.map((item) => {

                                                    return (
                                                        <div className={styles.return_to_shop}>
                                                            <div className={styles.recommended_products}>
                                                                  <span className={styles.text_alarm}>
                                                                       {item.last_price !== item.off_price && "محصول موردنظر به دلیل تغییر قیمت از سبد خرید شما حذف شد"}
                                                                   </span>
                                                                  <span className={styles.text_alarm}>
                                                                       {+item.count > item.inventory && "محصول موردنظر به دلیل تغییر موجودی از سبد خرید شما حذف شد"}
                                                                   </span>
                                                                <div  className={`${styles.recommended_products_box} ${(item.last_price !== item.off_price || +item.count > item.inventory) && styles.lock_product}`}>

                                                                    <div onClick={() => {
                                                                        router.push(`/product/[...param]`, `/product/${item.product_id}/${item.slug}`)
                                                                    }} className={styles.recommended_products_img}>
                                                                        <img
                                                                            src={ENV["API"]["IMG"] + "/product/" + item.product_id + "/200/" + item.img}/>
                                                                    </div>
                                                                    <div className={styles.recommended_products_box_detail}>
                                                                        <div
                                                                            className={styles.recommended_products_information}>
                                                                            <div
                                                                                className={styles.recommended_products_title}>
                                                                                {item.product_title}
                                                                            </div>
                                                                            <div className={styles.recommended_products_price}>
                                                                                {item.price !== 0 || item.price !== item.off_price &&
                                                                                <span className={styles.recommended_products_price_orginal}>
                                                                                {separate(item.price)}
                                                                                    <span> تومان</span>
                                                                                    </span>
                                                                                }

                                                                                <span className={styles.recommended_products_price_off}>
                                                                                    {separate(item.off_price)}
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div className={styles.recommended_products_number}>
                                                                            <div onClick={() => {
                                                                                increase(+item.count + 1, item.types_id)
                                                                            }}
                                                                                 className={`${styles.recommended_products_number_plus_icon} ${(+item.count === 5 || +item.count === item.inventory) && "disable"}`}/>
                                                                            <span
                                                                                className={styles.recommended_products_number_input}>{+item.count}</span>
                                                                            <div onClick={() => {
                                                                                +item.count > 0 && increase(+item.count - 1, item.types_id)
                                                                            }}
                                                                                 className={`${styles.recommended_products_number_minus_icon} ${+item.count === 1 && "disable"}`}/>
                                                                        </div>
                                                                        <div onClick={() => {
                                                                               deleteProduct(item.types_id)
                                                                              }}  className={styles.close_btn}/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                                :
                                                <div className={styles.empty_cart}>
                                                    <span>
                                                        سبد خرید شما خالی است .
                                                    </span>
                                                    <img src={"/emtyCart.png"}/>
                                                </div>

                                        }

                                    </div>
                                </Grid>
                                <Grid md={3} xs={12} item>
                                    <div className={styles.price_summary}>
                                        <div className={styles.price_summary_item}>
                                            <span className={styles.price_summary_title}>قیمت کل</span>
                                            <span className={styles.price_summary_value}>{mainLoading ?
                                                <Loading position={"absolute"} width={"20"}
                                                         height={"20"}/> : separate(sumPrice) + " تومان "}</span>
                                        </div>
                                        <div className={styles.price_summary_item}>
                                            <span className={styles.price_summary_title}> تخفیف </span>
                                            <span className={styles.price_summary_value}>{mainLoading ?
                                                <Loading position={"absolute"} width={"20"}
                                                         height={"20"}/> : separate(sumPrice - sumOffPrice) + " تومان "}</span>
                                        </div>
                                        <div className={styles.price_summary_total_price}>
                                            <div className={styles.price_summary_total_payable}>
                                                <span className={styles.price_summary_total_payable_title}>مبلغ قابل پرداخت</span>
                                                <span
                                                    className={styles.price_summary_total_payable_value}>{mainLoading ?
                                                    <Loading position={"absolute"} width={"20"}
                                                             height={"20"}/> : separate(sumOffPrice) + " تومان "} </span>
                                            </div>
                                        </div>
                                    </div>
                                    <Button
                                        onClick={() => {
                                            router.push("/cart/address")
                                        }}
                                        backgroundColor={"var(--primary)"}
                                        text={"تکمیل  فرآیند خرید"}
                                        icon={"/save_icon.png"}
                                        width={"100%"}
                                        padding={10}
                                        margin={0}
                                        opacity={data && data.length === 0 && .5}
                                        events={data && data.length === 0 && "none"}
                                        cursor={"pointer"}
                                        fontSize={16}
                                    />

                                </Grid>
                            </Grid>
                        </>
                        :
                        <span className="pls_login">لطفا برای مشاهده سبد خرید وارد سایت شوید </span>
                }
            </Container>
        </Layout>
    );
};

export default Index;


