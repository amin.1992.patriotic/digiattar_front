import React, {createRef, useEffect, useState} from 'react';
import styles from "../../styles/cartFactor.module.css"
import Button from "../../components/button/button";
import {useDispatch, useSelector} from "react-redux";
import {shoppingLine} from "../../redux/actions";
import Api from "../../services/api";
import {toast} from "react-toastify";
import moment from "moment-jalali";
import {useRouter} from "next/router";
import Loading from "../../components/loading/loading";
import {separate} from "../../services/separate";
import GroupButton from "../../components/groupButton/groupButton";

const Invoice = (props) => {
    const router = useRouter()
    const routId = router.query.id
    let offPriceCount = 0
    const canvasRef = createRef()
    const componentRef = React.createRef()
    const dispatch = useDispatch()
    const [orderData, setOrderData] = useState()
    const [loading, setLoading] = useState(false)
    const [orderLoading, setOrderLoading] = useState(false)

    useEffect(() => {
        setOrderLoading(true)
        if (typeof routId !== "undefined") {
            new Api().get(`/order/${routId}`, {})
                .then((res) => {
                    setOrderLoading(false)
                    if (typeof res !== "undefined") {
                        if (res.status) {
                            setOrderData(res.order)
                        } else {
                            toast.error(res.msg)
                        }
                    }
                })
                .catch((err) => {
                    setOrderLoading(false)
                    toast.error(err);
                })
            dispatch(shoppingLine("step_tree"))
        }
    }, [routId])

    useEffect(() => {
        console.log(orderData)
        let img = new Image();
        if(+orderData?.expired === 1) {
            img.src = '/expire.png';
        } else {
            img.src = '/mohr.png';
        }
        let c = document.getElementById("canvas");
        let ctx = c && c.getContext("2d");
        if (ctx !== null) {
            ctx.canvas.width = 300;
            ctx.canvas.height = 200;
        }
        ctx && ctx.drawImage(img, 0, 0);
    } ,[orderData]);

    const conditionPayment = (status) => {
        switch (status) {
            case 0 :
                return (
                    <span className="red">پرداخت نشده</span>
                )
            case 1 :
                return (
                    <span className="green">پرداخت شده</span>
                )

        }
    }

    const gateway = () => {
        setLoading(true);
        new Api().post(`/gateway`, {
            "invoice_id": routId,
            "paymentType": "online",
            "gateway": "saman"
        }).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    switch ("online") {
                        case 'online':
                            toast.success('در حال اتصال به درگاه بانک...');
                            let form = document.createElement("form");
                            form.method = response.payload.method;
                            form.action = response.payload.action;
                            response.payload.fields.map((item) => {
                                let element1 = document.createElement("input");
                                element1.value = item.value;
                                element1.name = item.name;
                                form.appendChild(element1);
                            });
                            document.body.appendChild(form);
                            form.submit();
                            break;
                        case "voucher":
                            toast.success(response.msg);
                            setLoading(false)
                            break;
                        case 'wallet':
                            toast.success(response.msg);
                            setLoading(false)
                            break;

                    }

                } else {
                    toast.error(response.msg);
                }

                setLoading(false)
            }
            setLoading(false)
        }).catch((err) => {

            toast.error(err);
            setLoading(false)
        })
    };

    return (
        <div className={styles.factor_container}>
            {
                orderLoading ? <Loading/> :
                    <>
                        {
                           (orderData && orderData.status === 1 || +orderData?.expired === 1) &&
                            <div className={styles.canvas_box}>
                                <canvas ref={canvasRef} id="canvas"/>
                            </div>
                        }
                        <div ref={componentRef} className={styles.cart_factor}>
                            <div className={styles.factor_header}>
                                <div onClick={() => {
                                    router.push("/")
                                }} className={styles.factor_header_left}>
                                    <img src={"/b_logo.png"}/>
                                </div>
                                <div className={styles.factor_header_center}>
                                    <h1>فاکتور فروش</h1>
                                </div>

                                <div className={styles.factor_header_right}>
                                    <div className={styles.factor_number}>
                                        <span> شماره فاکتور : </span>
                                        <span>{`Digiattar-${orderData && orderData.order_id}`}</span>
                                    </div>
                                    <div className={styles.factor_time}>
                                        <span>  تاریخ فاکتور : </span>
                                        <span>{moment(orderData && orderData.created_at).format('h:mm:ss , jYYYY/jM/jD')}</span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.member_information}>
                                <div className={styles.member_information_badge}>اطلاعات کاربر</div>
                                <div className={styles.member_information_box}>
                                    <div>
                                        <span className={styles.member_information_box_title}> نام و نام خانوادگی</span>
                                        <span className={styles.points}> : </span>
                                        <span
                                            className={styles.member_information_box_value}> {orderData && orderData.reciver_name}  </span>
                                    </div>
                                    <div>
                                        <span className={styles.member_information_box_title}> شماره شناسنامه</span>
                                        <span className={styles.points}> : </span>
                                        <span
                                            className={styles.member_information_box_value}> {orderData && orderData.national_code}</span>
                                    </div>
                                    <div>
                                        <span className={styles.member_information_box_title}> شماره تماس</span>
                                        <span className={styles.points}> : </span>
                                        <span
                                            className={styles.member_information_box_value}> {orderData && orderData.reciver_mobile}  </span>
                                    </div>
                                    <div>
                                        <span className={styles.member_information_box_title}>آدرس</span>
                                        <span className={styles.points}> : </span>
                                        <span className={styles.member_information_box_value}>
                                        {
                                            orderData && JSON.parse(orderData.address_regions).map((item) => {
                                                return (
                                                    item.title + " - "
                                                )
                                            })
                                        }
                                            {orderData && orderData.address_main}
                                    </span>
                                    </div>
                                    <div>
                                        <span className={styles.member_information_box_title}>کد پستی</span>
                                        <span className={styles.points}> : </span>
                                        <span
                                            className={styles.member_information_box_value}> {orderData && orderData.postal_code}  </span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.condition}>
                                <div>
                                    <span>وضعیت پرداخت</span>
                                    <span className={styles.points}> : </span>
                                    {
                                        conditionPayment(orderData && orderData.status)
                                    }

                                </div>
                                <div>
                                    <span>نوع تسویه حساب</span>
                                    <span className={styles.points}> : </span>
                                    <span>پرداخت اینترنتی</span>
                                </div>
                            </div>
                            <div className={styles.factor_table}>
                                <table cellSpacing="0">
                                    <thead>
                                    <th>ردیف</th>
                                    <th>مــــــحــــصـــــول</th>
                                    <th>تعداد</th>
                                    <th>قیمت</th>
                                    <th>تخفیف</th>
                                    <th>قیمت با اعمال تخفیف</th>
                                    <th>مالیات</th>
                                    <th>قیمت کل</th>
                                    </thead>
                                    <tbody>

                                    {
                                        orderData && JSON.parse(orderData.basket).map((item, index) => {
                                            offPriceCount = (item.price - item.off_price) + offPriceCount
                                            return (
                                                <tr>
                                                    <td>{index + 1}</td>
                                                    <td>{item.title}</td>
                                                    <td>{item.count}</td>
                                                    <td>{separate(item.price)}</td>
                                                    <td>{separate(item.price - item.off_price)}</td>
                                                    <td>{separate(item.off_price)} </td>
                                                    <td>{item.tax} </td>
                                                    <td>{separate(item.off_price * item.count)}</td>
                                                </tr>
                                            )
                                        })
                                    }

                                    </tbody>
                                </table>
                            </div>
                            <div className={styles.mini_table}>
                                <table>
                                    <thead>
                                    <th>قیمت کل</th>
                                    <th className="red">هزینه ارسال</th>
                                    <th className="green">تخفیف</th>
                                    <th>مالیات</th>
                                    <th>قابل پرداخت</th>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{separate(orderData && orderData.off_price)}</td>
                                        <td className="red">{separate(orderData && orderData.post_cost)}</td>
                                         <td className="green">{separate(offPriceCount)}</td>
                                        <td>{separate(orderData && orderData.tax)}</td>
                                        <td>{separate(orderData && orderData.total_pay)}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div className={styles.factor_btn}>
                                <GroupButton>
                                    <div onClick={() => {
                                        gateway()
                                    }}
                                         style={{
                                             background: "#68b35f",
                                             pointerEvents:( orderData && orderData.status === 1 || +orderData?.expired === 1) && "none",
                                             opacity: (orderData && orderData.status === 1 || +orderData?.expired === 1) && .3
                                         }}
                                    >
                                        {
                                            loading ? <Loading radius={0} position={"absolute"}/> : "پرداخت"
                                        }

                                    </div>
                                    <div onClick={() => {window.print()}} style={{background: "#f35334"}}>پرینت</div>
                                    <div onClick={() => {orderData && orderData.status === 0 ? router.push('/cart') : router.push("/")}} style={{background: "#4e3737"}}>بازگشت</div>
                                </GroupButton>
                            </div>
                        </div>
                    </>
            }
        </div>
    );
};


export default Invoice;
