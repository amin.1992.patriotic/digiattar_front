import React from 'react';
import Document, {Html, Head, Main, NextScript} from 'next/document';
import {ServerStyleSheets} from '@material-ui/core/styles';

const THEME_COLOR =  "#10b245"

export default class MyDocument extends Document {
    render() {
        return (
            <Html  lang="fa">
                <Head>
                    <link rel="manifest" href="/manifest/manifest.json" />
                    <link href="/sw/sw.js" />
                    <link rel={"apple-touch-icon"} href="/small_logo.png"/>
                    <link rel={"icon"} href={"/small_logo.png"}/>
                    <meta charSet="utf-8"/>
                    <meta name="theme-color" content={THEME_COLOR} />
                </Head>
                <meta name="viewport"
                      content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0"/>
                <body oncopy="return false" oncut="return false" onpaste="return false">
                    <Main/>
                    <NextScript/>
                </body>
            </Html>
        );
    }
}

MyDocument.getInitialProps = async (ctx) => {

    const sheets = new ServerStyleSheets();
    const originalRenderPage = ctx.renderPage;

    ctx.renderPage = () =>
        originalRenderPage({
            enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
        });

    const initialProps = await Document.getInitialProps(ctx);

    return {
        ...initialProps,
        styles: [...React.Children.toArray(initialProps.styles), sheets.getStyleElement()],
    };
};












