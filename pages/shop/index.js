import React, { useEffect, useState ,useCallback } from "react";
import { Grid } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import styles from "../../styles/category.module.css";
import CategoryBox from "../../components/categoryBox/categoryBox";
import CheckBox from "../../components/checkBox/checkBox";
import Switch from "../../components/switch/switch";
import Layout from "../../components/layout/layout";
import { initializeStore } from "../../redux/store";
import { ENV } from "../../services/env";
import { useRouter } from "next/router";
import Pagination from "react-js-pagination";
import { Helmet } from "react-helmet";
import { QueryCache, usePaginatedQuery, useQuery } from "react-query";
import { dehydrate } from "react-query/hydration";
import Api from "../../services/api";
import Loading from "../../components/loading/loading";
import { useDispatch, useSelector } from "react-redux";
import { tree } from "../../redux/actions";
import InputSearch from "../../components/inputSearch/inputSearch"
import Link from "next/link";
import Faq from "../../components/faq/faq";
const queryString = require('query-string');

const Shop = (props) => {
    console.log(props)
    let liste = [
        {id:1 , q:"از کدام فروشگاه گوشی موبایل را ارزان تر بخریم؟" , a:" امروز گوشی را در تمام فروشگاه‌ها مشاهده کنید و ارزان‌ترین فروشنده اینترنتی را بیابید."},
        {id:2 , q:"چطور مشخصات و قیمت گوشی‌ها را با هم مقایسه کنیم؟" , a:"دسترسی به مقایسه قیمت گوشی و مشخصات فنی از طریق لیست‌ محصول و صفحه‌ی آن‌ها امکان‌پذیر است. همچنین استفاده از نظرات دیگر کاربران، بررسی‌های تخصصی زومیت و بخش پرسش و پاسخ نیز به ان"},
        {id:3 , q:"لیست قیمت گوشی سامسونگ، شیائومی، اپل، هواوی و دیگر برندها را چطور پیدا کنیم؟" , a:"با کلیک روی «این لینک» و ورود به بخش لیست قیمت موبایل و انتخاب برند و بازه‌ی قیمتی موردنظر خود لیست تمام گوشی‌های موردنظر شما بر اساس محبوبیت در بین کاربران برای شما نمایش داده خواهد شد."},
    ]
    const data = props.res
    const router = useRouter();
    const dispatch = useDispatch();
    const treeList = useSelector((state) => state.other);
    const [active, setActive] = useState(
        isNaN(router.query.page) ? 1 : parseInt(router.query.page)
    );
    const [scrollHandler, setScrollHandler] = useState(false);
    const [searchText, setSearchText] = useState("");
    const [pageCount, setPageCount] = useState(0);
    const [sort, setSort] = useState(0);
    const [stock, setStock] = useState(1);
    const [height, setHeight] = useState(40);
    const [multipleExample, setMultipleExample] = useState("");
    const [list, setList] = useState([]);
    var idForNewData = "";
    let parsed = router.query


    useEffect(() => {
        const bread = []
        bread.push({ link:"/shop", name :"فروشگاه"})
        localStorage.setItem("bread" , JSON.stringify(bread))

        window.scrollTo({ top: 0, behavior: "smooth" });
        idForNewData = router.query.id;
        data && setPageCount(data.products.data.length);
    }, [router.query.page]);

    const handleMultipleToggle = (index) => {
        const arr = [...multipleExample];
        const i = arr.indexOf(index);
        if (arr.includes(index)) {
            arr.splice(i, 1);
        } else {
            arr.push(index);
        }
        setMultipleExample(arr);
    };
    let defultHeight = 350;

    const handlePageChange = async (page) => {
        await router.push(`/shop/?page=${page}&stock=${router.query.stock !== undefined ? router.query.stock : 1}&sort=${router.query.sort !== undefined ? router.query.sort : 0}${list.length !== 0 ? `&attribute=${list}` : ""}`,);
        await setScrollHandler(true);
        await setScrollHandler(false);
        await setActive(page);

    };
    const handleSortChange = async (sort) => {
        await setSort(sort);
        router.push(
            `/shop/?page=${router.query.page !== undefined ? router.query.page : 1}&stock=${router.query.stock !== undefined ? router.query.stock : 1}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}`
        );
    };
    const handleStockChange = async (stock) => {
        await setStock(stock);
        await router.push(
            `/shop/?page=${router.query.page !== undefined ? router.query.page : 1}&stock=${stock}&sort=${router.query.sort !== undefined ? router.query.sort : 0}${list.length !== 0 ? `&attribute=${list}` : ""}`
        );
    };

    const handlePageCategory = (id , slug) => {
        router.push(`/shop/category/[...param]`,`/shop/category/${id}/${slug}`);
    };

    const handleTick = (e) => {
        const arr = [...list];
        const i = arr.indexOf(e);
        if (arr.includes(e)) {
            arr.splice(i, 1);
        } else {
            arr.push(e);
        }
        setList(arr);
        router.push(
            `/shop?page=${router.query.page !== undefined ? router.query.page : 1}&stock=${router.query.stock !== undefined ? router.query.stock : 1}&sort=${router.query.sort !== undefined ? router.query.sort : 0}${arr.length !== 0 ? `&attribute=${arr}` : ""}`
        );
    };

    const renderBlogTree = useCallback(
        (nodes , type) => {
            const nodeList =
                nodes &&
                nodes.map((item) => {
                    const items = { id: item.id, name: item.title };
                    const hasChild = item.children && item.children.length > 0;
                    const children = hasChild ? renderBlogTree(item.children) : "";

                    if(searchText !== "" && !item.title.includes(searchText)) {
                        return false
                    }
                    return (
                        <>
                            <span
                                className = {
                                    hasChild === true
                                        ? "tree-parent has-child"
                                        : "tree-parent  has-no-child"
                                }
                            >
                              <li className={styles.filter_list_grouping_item}>
                                <span>{item.title }</span>
                                  {
                                      <CheckBox
                                          checked={type === "disease" ? parsed.disease :  parsed.attribute}
                                          name={"shop"}
                                          onChange={(e) => {
                                              handleTick(e[0]);
                                          }}
                                          color={"var(--Third)"}
                                          type={"checkbox"}
                                          item={items}
                                      />
                                  }
                              </li>
                         </span>
                            {hasChild === true && (
                                <ul
                                    className={styles.tree_children}
                                    style={{
                                        display: list && list.includes(item.id) ? "block" : "none",
                                    }}
                                >
                                    {children}
                                </ul>
                            )}
                        </>
                    );
                });
            return nodeList;
        },
    );


    return (
        <>
            {
                <>
                    <Helmet>
                        <title>{"عطاری آنلاین دیجی عطار | فروشگاه جامع گیاهان دارویی وطب سنتی"}</title>
                    </Helmet>
                    <div className={styles.category}>
                        <Layout>
                            <div className={styles.category_loading}>
                                <Container dir="rtl">
                                    <Grid spacing={3}  container={true}>
                                        <Grid item md={3} xs={12} container={true}>
                                            <Grid item xs={12}>
                                                <div className={styles.category_menu}>
                                                    <div className={styles.filter}>
                                                        <span>فیلتر محصولات</span>
                                                        <img src={"/filterIcon.png"} />
                                                    </div>
                                                    <div className={styles.filter_list_title}>
                                                        <span>{"فقط کالای موجود"}</span>
                                                        <Switch
                                                            onChange={(e) => {
                                                                handleStockChange(e);
                                                            }}
                                                            id={
                                                                router.query.stock !== undefined
                                                                    ? router.query.stock
                                                                    : 1
                                                            }
                                                        />
                                                    </div>
                                                    <div
                                                        style={{
                                                            maxHeight: multipleExample.includes(50)
                                                                ? defultHeight
                                                                : 40,
                                                        }}
                                                        className={styles.filter_list}
                                                    >
                                                        {data && data.cached && data.cached.tree && data.cached.tree.length > 0 && (
                                                            <>
                                                                <div
                                                                    onClick={() => {
                                                                        handleMultipleToggle(50);
                                                                    }}
                                                                    className={styles.filter_list_title}
                                                                >
                                                                    <span>دسته بندی</span>
                                                                    <img
                                                                        style={{
                                                                            transform: multipleExample.includes(50)
                                                                                ? "rotate(0deg) "
                                                                                : "rotate(90deg)",
                                                                        }}
                                                                        src={"/arrowdown.png"}
                                                                    />
                                                                </div>
                                                                <ul className={styles.filter_list_grouping}>
                                                                    <>
                                                                        {data.cached &&
                                                                        data.cached.tree.map((item) => {
                                                                            return (
                                                                                <li
                                                                                    className={
                                                                                        styles.filter_list_grouping_item
                                                                                    }
                                                                                    onClick={() => {
                                                                                        handlePageCategory(item.id , item.slug)
                                                                                    }}
                                                                                >
                                                                                    <span>{item.title}</span>
                                                                                </li>
                                                                            );
                                                                        })}
                                                                    </>
                                                                </ul>
                                                            </>
                                                        )}
                                                    </div>
                                                    <div
                                                        style={{
                                                            maxHeight: multipleExample.includes(60)
                                                                ? defultHeight
                                                                : 40,
                                                        }}
                                                        className={styles.filter_list}
                                                    >
                                                        {data && data.cached && data.cached.tree && data.cached.tree.length > 0 && (
                                                            <>
                                                                <div
                                                                    onClick={() => {
                                                                        handleMultipleToggle(60);
                                                                    }}
                                                                    className={styles.filter_list_title}
                                                                >
                                                                    <span>بیماری</span>
                                                                    <img
                                                                        style={{
                                                                            transform: multipleExample.includes(60)
                                                                                ? "rotate(0deg) "
                                                                                : "rotate(90deg)",
                                                                        }}
                                                                        src={"/arrowdown.png"}
                                                                    />

                                                                </div>
                                                                <ul className={styles.filter_list_grouping}>
                                                                    <>
                                                                        <InputSearch search={(e) => {setSearchText(e)}}/>
                                                                        {renderBlogTree(data.cached.filter, "disease")}
                                                                    </>
                                                                </ul>
                                                            </>
                                                        )}
                                                    </div>
                                                    {data &&
                                                    data.cached &&
                                                    data.cached.tree.map((item, index) => (
                                                        <div
                                                            style={{
                                                                maxHeight: multipleExample.includes(index)
                                                                    ? defultHeight
                                                                    : 40,
                                                            }}
                                                            className={styles.filter_list}
                                                        >
                                                            {item.tags && item.tags.length > 0 && (
                                                                <>
                                                                    <div
                                                                        onClick={() => {
                                                                            handleMultipleToggle(index);
                                                                        }}
                                                                        className={styles.filter_list_title}
                                                                    >
                                                                        <span>{item.title}</span>
                                                                        <img
                                                                            style={{
                                                                                transform: multipleExample.includes(
                                                                                    index
                                                                                )
                                                                                    ? "rotate(0deg) "
                                                                                    : "rotate(90deg)",
                                                                            }}
                                                                            src={"/arrowdown.png"}
                                                                        />
                                                                    </div>
                                                                    <ul className={styles.filter_list_grouping}>
                                                                        {item.tags &&
                                                                        item.tags.map((items) => {
                                                                            return (
                                                                                <li
                                                                                    className={
                                                                                        styles.filter_list_grouping_item
                                                                                    }
                                                                                >
                                                                                    <span>{items.name}</span>
                                                                                    {
                                                                                        <CheckBox
                                                                                            name={"shop"}
                                                                                            onChange={(e) => {
                                                                                                handlePageCategory(
                                                                                                    e,
                                                                                                    item.id
                                                                                                );
                                                                                            }}
                                                                                            color={"var(--Third)"}
                                                                                            item={items}
                                                                                            type={"checkbox"}
                                                                                        />
                                                                                    }
                                                                                </li>
                                                                            );
                                                                        })}
                                                                    </ul>
                                                                </>
                                                            )}
                                                        </div>
                                                    ))}
                                                </div>
                                            </Grid>
                                        </Grid>
                                        <Grid item md={9} xs={12} container={true} justify="center">
                                            <div className={styles.sort}>
                                                {data &&
                                                data.cached &&
                                                data.cached.sort.map((item, index) => {
                                                    return (
                                                        <div
                                                            onClick={() => {
                                                                handleSortChange(index);
                                                            }}
                                                            id="sort"
                                                            className={styles.order_list}
                                                        >
                                                            {item.title}
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                            <Grid
                                                style={{ height: "-webkit-fill-available" }}
                                                item
                                                xs={12}
                                                container={true}
                                                spacing={2}
                                            >
                                                {data && data.products && data.products.data && data.products.data.map((item) => {
                                                    return (
                                                        <Grid item lg={3} sm={4} xs={12}>
                                                            <CategoryBox
                                                                row={true}
                                                                product_id={item.pivot && item.pivot.product_id}
                                                                src={ENV["API"]["IMG"] + "/product/" + item.id + "/200/" + item.img}
                                                                title={item.title}
                                                                price={item.types && item.types[0] && item.types[0].price}
                                                                off={item.types && item.types[0] && item.types[0].off_price}
                                                                id={item.id}
                                                                typeId={item.types && item.types[0] && item.types[0].id}
                                                                slug={item.slug}
                                                                weight={item.types?.[0].price_parameters?.[0]?.title}
                                                            />
                                                        </Grid>
                                                    );
                                                })}
                                                {data && data.products && data.products.data && data.products.data.length === 0 && (
                                                    <p className={styles.nothing_to_show}>
                                                        محصولی برای نمایش وجود ندارد{" "}
                                                    </p>
                                                )}
                                                {data && data.products && data.products.total / data.products.per_page > 1 && (
                                                    <div className={styles.pagination}>
                                                        <Pagination
                                                            activePage={active}
                                                            itemsCountPerPage={data.products.per_page}
                                                            totalItemsCount={data.products.total}
                                                            pageRangeDisplayed={5}
                                                            onChange={handlePageChange}
                                                        />
                                                    </div>
                                                )}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Container>
                            </div>
                        </Layout>
                    </div>
                    <style jsx>{`
                         #sort:nth-child(${router.query.sort !== undefined
                        ? parseInt(router.query.sort) + 1
                        : 1}) {
                          background-color: var(--Third);
                          color: white;
                          box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2);
                        }
                     `}</style>
                </>
            }
        </>
    );
};

export async function getServerSideProps({ query }) {
    const res = await new Api().get(`/shop`, {
        page: query.page === undefined ? 0 : query.page,
        stock: query.stock === undefined ? 1 :query.stock,
        sort: query.sort === undefined ? 0 :query.sort ,
        filter: query.attribute === undefined ? [] : [query.attribute]
    } );
    return {
        props: {
            res,
        },
    };
}

export default Shop;
