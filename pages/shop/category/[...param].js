import React, {useCallback, useEffect, useState} from "react";
import {Grid} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import styles from "../../../styles/category.module.css";
import CategoryBox from "../../../components/categoryBox/categoryBox";
import CheckBox from "../../../components/checkBox/checkBox";
import Switch from "../../../components/switch/switch";
import Layout from "../../../components/layout/layout";
import {ENV} from "../../../services/env";
import {useRouter} from "next/router";
import Pagination from "react-js-pagination";
import {Helmet} from "react-helmet";
import {QueryCache, useQuery, usePaginatedQuery} from "react-query";
import {dehydrate} from "react-query/hydration";
import Api from "../../../services/api";
import Loading from "../../../components/loading/loading";
import queryString from "query-string";
import InputSearch from "../../../components/inputSearch/inputSearch";
import Faq from "../../../components/faq/faq";


const Category = ({data}) => {
    console.log(data)
    const router = useRouter();
    let attribute = router.query

    var idForNewData = "";
    const [searchText, setSearchText] = useState("");
    const [params, setParams] = useState([]);
    const [urls, setUrls] = useState("");
    const [active, setActive] = useState(0);
    const [pageCount, setPageCount] = useState(0);
    const [sort, setSort] = useState(0);
    const [stock, setStock] = useState(1);
    const [height, setHeight] = useState(40);
    const [list, setList] = useState([]);
    const [disease, setDisease] = useState([]);
    const [multipleExample, setMultipleExample] = useState("");

    let parsed = router.query

    useEffect(() => {
        console.log(data)
        const bread = []
        const item = JSON.parse(localStorage.getItem("bread"))
        for(let i = 0 ; i < (item !== null ? item.length : 0 ); i++) {
            if(!item[i].link.includes("shop/category"))
                bread.push(item[i])
        }
        bread.push({ link:`/shop/category/${data.cached.id}` , name:data.cached.slug })
        localStorage.setItem("bread" , JSON.stringify(bread))


        if (typeof router.query.param[1] === "undefined") {
            router.push(`/shop/category/[...param]?id=${data.cached.id}&slug=${data.cached.slug}`, `/shop/category/${data.cached.id}/${data.cached.slug}`)
        }
        const parsed = queryString.parse(location.search);
        setActive(isNaN(parsed.page) ? 1 : parseInt(parsed.page));
    }, []);

    // useEffect(() => {
    //     !isFetching && window.scrollTo({ top: 0, behavior: "smooth" });
    //     idForNewData = router.query.id;
    //     data && setPageCount(data.products.data.length);
    // } ,[router.query.page]);

    const handleMultipleToggle = (index) => {
        const arr = [...multipleExample];
        const i = arr.indexOf(index);
        if (arr.includes(index)) {
            arr.splice(i, 1);
        } else {
            arr.push(index);
        }
        setMultipleExample(arr);
    };
    let defultHeight = 350;

    let arr = [];

    const handlePageChange = async (active) => {
        await setActive(active);
        // window.scrollTo({top: 0, behavior: 'smooth'});
        await router.push(
            `/shop/category/[...param]?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`,
            `/shop/category/${router.query.param[0]}/${router.query.param[1]}?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`);
    };
    const handleSortChange = async (sort) => {
        await setSort(sort);
        await router.push(
            `/shop/category/[...param]?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`,
            `/shop/category/${router.query.param[0]}/${router.query.param[1]}?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`
        );
    };
    const handleStockChange = async (stock) => {
        await setStock(stock);
        await router.push(
            `/shop/category/[...param]?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`,
            `/shop/category/${router.query.param[0]}/${router.query.param[1]}?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`
        );
    };
    const handleTick = (e) => {
        const arr =  [...disease] ;
        const i = arr.indexOf(e);
        if (arr.includes(e)) {
            arr.splice(i, 1);
        } else {
            arr.push(e);
        }
        setDisease(arr)
            router.push(
                `/shop/category/[...param]?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${arr.length !== 0 ? `&disease=${arr}` : ""}`,
                `/shop/category/${router.query.param[0]}/${router.query.param[1]}?page=${active}&stock=${stock}&sort=${sort}${list.length !== 0 ? `&attribute=${list}` : ""}${arr.length !== 0 ? `&disease=${arr}` : ""}`
            );

    };
    const handlePageCategory = async (e) => {
        const arr = [...list];
        const i = arr.indexOf(e[0]);
        if (arr.includes(e[0])) {
            arr.splice(i, 1);
        } else {
            arr.push(e[0]);
        }
        setList(arr);
        await router.push(
            `/shop/category/[...param]?page=${active}&stock=${stock}&sort=${sort}${arr.length !== 0 ? `&attribute=${arr}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`,
            `/shop/category/${router.query.param[0]}/${router.query.param[1]}?page=${active}&stock=${stock}&sort=${sort}${arr.length !== 0 ? `&attribute=${arr}` : ""}${disease.length !== 0 ? `&disease=${disease}` : ""}`
        );
    };
    const renderBlogTree = useCallback(
        (nodes , type) => {
            const nodeList =
                nodes &&
                nodes.map((item) => {
                    const items = {id: item.id, name: item.title};
                    const hasChild = item.children && item.children.length > 0;
                    const children = hasChild ? renderBlogTree(item.children) : "";

                    if (searchText !== "" && !item.title.includes(searchText)) {
                        return false
                    }
                    return (
                        <>
                            <span
                                className={
                                    hasChild === true
                                        ? "tree-parent has-child"
                                        : "tree-parent  has-no-child"
                                }
                            >
                              <li className={styles.filter_list_grouping_item}>
                                <span>{item.title}</span>
                                  {
                                      <CheckBox
                                          checked={ parsed.disease }
                                          name={"shop"}
                                          onChange={(e) => {
                                              handleTick(e[0]);
                                          }}
                                          color={"var(--Third)"}
                                          type={"checkbox"}
                                          item={items}
                                      />
                                  }
                              </li>
                         </span>
                            {hasChild === true && (
                                <ul
                                    className={styles.tree_children}
                                    style={{
                                        display: list && list.includes(item.id) ? "block" : "none",
                                    }}
                                >
                                    {children}
                                </ul>
                            )}
                        </>
                    );
                });
            return nodeList;
        },
    );

    return (
        <>
            <Helmet>
                <title>{data.cached && data.cached.meta_title}</title>
                <script type="application/ld+json">{`
                                {
                                    "@context": "https://schema.org",
                                    "@type": "cached",
                                    "name":"دیجی عطار"
                                    "mainEntityOfPage": {
                                    "@type": "WebPage",
                                        "@id":  "${ENV.API["FETCH"]}"
                                },
                                    "headline": "${data.cached && data.cached.title}",
                             
                                    "author": {
                                    "@type": "Person",
                                        "name": "digiattar"
                                },
                                    "publisher": {
                                    "@type": "Organization",
                                        "name": "digiattar",
                                      
                                    },
                                    "description": "${data.cached && data.cached.meta_description}"
                                }
                            `}</script>

                <meta
                    name="description"
                    content={
                        data
                            ? data.cached && data.cached.meta_description &&
                            data.cached.meta_description
                            : ""
                    }
                />
                <link rel="canonical" href={`${ENV.API["FETCH"]}`}/>
            </Helmet>
            <div className={styles.category}>
                <Layout>
                    <div className={styles.category_loading}>
                        <Container dir="rtl">
                            <Grid spacing={3} container={true}>
                                <Grid item md={3} xs={12} container={true}>
                                    <Grid item xs={12}>
                                        <div className={styles.category_menu}>
                                            <div className={styles.filter}>
                                                <span>فیلتر محصولات</span>
                                                <img src={"/filterIcon.png"}/>
                                            </div>
                                            <div className={styles.filter_list_title}>
                                                <span>{"فقط کالای موجود"}</span>
                                                <Switch
                                                    onChange={(e) => {
                                                        handleStockChange(e);
                                                    }}
                                                    id={
                                                        router.query.stock !== undefined
                                                            ? router.query.stock
                                                            : 1
                                                    }
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    maxHeight: multipleExample.includes(60)
                                                        ? defultHeight
                                                        : 40,
                                                }}
                                                className={styles.filter_list}
                                            >
                                                {(

                                                    <>
                                                        <div
                                                            onClick={() => {
                                                                handleMultipleToggle(60);
                                                            }}
                                                            className={styles.filter_list_title}
                                                        >
                                                            <span>بیماری</span>
                                                            <img
                                                                style={{
                                                                    transform: multipleExample.includes(60)
                                                                        ? "rotate(0deg) "
                                                                        : "rotate(90deg)",
                                                                }}
                                                                src={"/arrowdown.png"}
                                                            />
                                                        </div>
                                                        <ul className={styles.filter_list_grouping}>
                                                            <>
                                                                <InputSearch search={(e) => {
                                                                    setSearchText(e)
                                                                }}/>
                                                                {renderBlogTree(data.cached.filter , "disease")}
                                                            </>
                                                        </ul>
                                                    </>
                                                )}
                                            </div>
                                            <div
                                                style={{
                                                    maxHeight: multipleExample.includes(50)
                                                        ? defultHeight
                                                        : 40,
                                                }}
                                                className={styles.filter_list}
                                            >
                                                {data && data.cached &&
                                                data.cached.tree &&
                                                data.cached.tree.length > 0 && (
                                                    <>
                                                        <div
                                                            onClick={() => {
                                                                handleMultipleToggle(50);
                                                            }}
                                                            className={styles.filter_list_title}
                                                        >
                                                            <span>نوع</span>
                                                            <img
                                                                style={{
                                                                    transform: multipleExample.includes(50)
                                                                        ? "rotate(0deg) "
                                                                        : "rotate(90deg)",
                                                                }}
                                                                src={"/arrowdown.png"}
                                                            />
                                                        </div>
                                                        <div className={styles.filter_list_grouping}>
                                                            <>
                                                                {data.cached &&
                                                                data.cached.tree.map((item) => {
                                                                    return (
                                                                        <div
                                                                            className={
                                                                                styles.filter_list_grouping_item
                                                                            }
                                                                        >
                                                                            <span>{item.title}</span>
                                                                            <CheckBox
                                                                                name={"category"}
                                                                                onChange={(e) => {
                                                                                    handlePageCategory(e);
                                                                                }}
                                                                                color={"var(--Third)"}
                                                                                item={item}
                                                                                type={"radio"}
                                                                            />
                                                                        </div>
                                                                    );
                                                                })}
                                                            </>
                                                        </div>
                                                    </>
                                                )}
                                            </div>
                                            {data &&
                                            data.cached &&
                                            data.cached.attributes.map((item, index) => (
                                                <div
                                                    style={{
                                                        maxHeight: multipleExample.includes(index)
                                                            ? defultHeight
                                                            : 40,
                                                    }}
                                                    className={styles.filter_list}
                                                >
                                                    {item.tags.length > 0 && (
                                                        <>
                                                            <div
                                                                onClick={() => {
                                                                    handleMultipleToggle(index);
                                                                }}
                                                                className={styles.filter_list_title}
                                                            >
                                                                <span>{item.title}</span>
                                                                <img
                                                                    style={{
                                                                        transform: multipleExample.includes(index)
                                                                            ? "rotate(0deg) "
                                                                            : "rotate(90deg)",
                                                                    }}
                                                                    src={"/arrowdown.png"}
                                                                />
                                                            </div>
                                                            <ul className={styles.filter_list_grouping}>
                                                                {item.tags &&
                                                                item.tags.map((items) => {
                                                                    return (
                                                                        <li
                                                                            className={
                                                                                styles.filter_list_grouping_item
                                                                            }
                                                                        >
                                                                            <span>{items.name}</span>
                                                                            {
                                                                                <CheckBox
                                                                                    checked={ parsed.attribute }
                                                                                    name={"attributes"}
                                                                                    onChange={(e) => {
                                                                                        handlePageCategory(e, item.id);
                                                                                        console.log("bbbb")
                                                                                    }}
                                                                                    color={"var(--Third)"}
                                                                                    item={items}
                                                                                    type={"checkbox"}
                                                                                />
                                                                            }
                                                                        </li>
                                                                    );
                                                                })}
                                                            </ul>
                                                        </>
                                                    )}
                                                </div>
                                            ))}
                                        </div>
                                    </Grid>
                                </Grid>
                                <Grid item md={9} xs={12} container={true} justify="center">
                                    <div className={styles.sort}>
                                        {data &&
                                        data.cached &&
                                        data.cached.sort.map((item, index) => {
                                            return (
                                                <div
                                                    onClick={() => {
                                                        handleSortChange(index);
                                                    }}
                                                    id="sort"
                                                    className={styles.order_list}
                                                >
                                                    {item.title}
                                                </div>
                                            );
                                        })}
                                    </div>
                                    <Grid
                                        style={{height: "-webkit-fill-available"}}
                                        item
                                        xs={12}
                                        container={true}
                                        spacing={2}
                                    >

                                        {data && data.products &&
                                        data.products.data &&
                                        data.products.data.map((item) => {
                                            return (
                                                <Grid item lg={3} md={4} sm={4} xs={12}>
                                                    <CategoryBox
                                                        row={true}
                                                        product_id={item.pivot && item.pivot.product_id}
                                                        src={ENV["API"]["IMG"] + "/product/" + item.id + "/400/" + item.img}
                                                        title={item.title}
                                                        price={item.types && item.types[0] && item.types[0].price}
                                                        off={item.types && item.types[0] && item.types[0].off_price}
                                                        id={item.id}
                                                        typeId={item.types && item.types[0] && item.types[0].id}
                                                        slug={item.slug}
                                                        weight={item.types?.[0]?.price_parameters?.[0]?.title}
                                                    />
                                                </Grid>
                                            );
                                        })}
                                        {data && data.products &&
                                        data.products.data &&
                                        data.products.data.length === 0 && (
                                            <p className={styles.nothing_to_show}>
                                                مطلبی برای نمایش وجود ندارد{" "}
                                            </p>
                                        )}
                                        {data && data.products && data.products.total / data.products.per_page > 1 && (
                                            <div className={styles.pagination}>
                                                <Pagination
                                                    activePage={active}
                                                    itemsCountPerPage={data.products.per_page}
                                                    totalItemsCount={data.products.total}
                                                    pageRangeDisplayed={5}
                                                    onChange={handlePageChange}
                                                />
                                            </div>
                                        )}
                                    </Grid>
                                    {
                                        data?.cached?.faq?.length > 0 &&
                                            <Faq list = {data?.cached?.faqs}/>
                                    }
                                </Grid>

                            </Grid>
                        </Container>
                    </div>
                </Layout>
            </div>
            <style jsx>{`
              #sort:nth-child(${router.query.sort !== undefined
                      ? parseInt(router.query.sort) + 1
                      : 1}) {
                background-color: var(--Third);
                color: white;
                box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2);
              }
            `}</style>
        </>
    );
};

export async function getServerSideProps({query}) {
    const res = await new Api().get(`/shop/category/${query.param[0]}`, {
        page: query.page === undefined ? 0 : query.page,
        stock: query.stock === undefined ? 1 : query.stock,
        sort: query.sort === undefined ? 0 : query.sort,
        selected_tags: query.attribute === undefined ? [] : [query.attribute],
        filters: query.disease === undefined ? [] : [query.disease]
    } , false);
    return {
        props: {
            data: res
        },
    };
}

export default Category;
