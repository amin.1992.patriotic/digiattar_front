import React, {useEffect} from 'react';
import Layout from "../../components/layout/layout"
import styles from "../../styles/determinationOfTemperament.module.css";
import BmiVector from "../../components/bmiVector/bmiVector";
import Container from "@material-ui/core/Container";

const Result = () => {
    let data
    useEffect(() => {
         data = JSON.parse(sessionStorage.getItem("temperament"))
    } ,[])
    return (
        <Layout>
            <Container dir="rtl">
            <div className={`${styles.bmi_toggle}`}>
                {
                    data && <BmiVector data={data}/>
                }
            </div>
            </Container>
        </Layout>
    );
};

export default Result;
