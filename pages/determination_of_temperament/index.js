import React, {useEffect, useState} from 'react';
import styles from "../../styles/determinationOfTemperament.module.css"
import Layout from "../../components/layout/layout";
import Container from "@material-ui/core/Container";
import Radio from "../../components/radio/radio";
import BmiVector from "../../components/bmiVector/bmiVector";
import Button from "../../components/button/button";
import Api from "../../services/api";
import {sendData} from "next/dist/next-server/server/api-utils";
import {toast} from "react-toastify";
import {useRouter} from "next/router";
import {loginModalToggle} from "../../redux/actions";
import {useSelector, useDispatch} from "react-redux";
import InlineLoader from "../../components/inlineLoader/inlineLoader";
import {Helmet} from "react-helmet";
import {ENV} from "../../services/env";

const DeterminationOfTemperament = () => {
    const isLogin = useSelector(state => state.loginReducer.login)
    const dispatch = useDispatch()
    const router = useRouter()
    const [listItem, setListItem] = useState([])
    const [data, setData] = useState("")
    const [loading, setLoading] = useState(false)
    const [loadingForm, setLoadingForm] = useState(false)
    const [itemId, setItemId] = useState([])
    useEffect(() => {
        setLoadingForm(true)
        new Api().get(`/determinetemperament/form`, {})
            .then((res) => {
                if (res) {
                    setData(res)
                }
                setLoadingForm(false)
            })
            .catch((err) => {
                setLoadingForm(false)
                throw err
            })
    }, [])

    const handleList = (id, answer) => {
        let list = [...listItem]
        let objIndex = list.findIndex((obj => obj.key == id));
        if (objIndex !== -1) {
            list[objIndex].value = answer
        } else {
            list.push({key: id.toString(), value: answer})
        }
        setListItem(list)
    }
    const sendTempData = () => {
        if (listItem.length !== data.length) {
            toast.error("لطفا به همه سوالات پاسخ دهید")
            return false
        }
        setLoading(true)
        new Api().post(`/determinetemperament/calculate`, {
            list: listItem,
            record_id: 1

        }, false)
            .then((res) => {
                setLoading(false)
                if (typeof res !== "undefined") {
                    if (res.status) {
                        sessionStorage.setItem("temperament", JSON.stringify(res.data))
                        router.push('/determination_of_temperament/result');
                    } else {
                        toast.error(res.message)
                    }

                }
            })
            .catch((err) => {
                setLoading(false)
                throw err
            })
    }
    const handleToggle = (item) => {
        const list = [...itemId]
        const index = list.indexOf(item.id)
        if(index === -1) {
            setItemId([...itemId , item.id])
        } else (
            list.splice( index , 1 ),
            setItemId(list)
        )
    }
    return (
        <Layout>
            <Helmet>
                <title>{"تعیین مزاج آنلاین"}</title>
                <script type="application/ld+json">{`
                                {
                                    "@context": "https://schema.org",
                                    "@type": "cached",
                                    "name":"دیجی عطار"
                                    "mainEntityOfPage": {
                                    "@type": "WebPage",
                                        "@id":  "${ENV.API["FETCH"]}"
                                },
                         
                            `}</script>

            </Helmet>
            <Container dir="rtl">
                <div className={styles.determination_of_temperament}>
                    {
                        loadingForm ?
                            <InlineLoader/>
                            :
                            <>
                                <div className={styles.determination_of_temperament_head}>
                                    لطفا فرم پرسشنامه زیر را پر نمایید
                                </div>
                                <div className={styles.determination_of_temperament_section}>
                                    <div className={styles.determination_of_temperament_section_title}>
                                        <div className={styles.determination_of_temperament_section_title_item}>
                                            سوالات
                                        </div>
                                        <div className={styles.determination_of_temperament_section_title_item}>
                                            جواب
                                        </div>
                                    </div>
                                    <div className={styles.determination_of_temperament_section_body}>
                                        {
                                            data && data.map((item, index) => {
                                                return (
                                                    <div className={styles.determination_of_temperament_section_body_item}>
                                                        <div onClick={() => {handleToggle(item)}} className={styles.question_text}>
                                                            {item.question_text}
                                                            <span className={`${styles.arrow_icon} ${itemId && !itemId.includes(item.id)  ? "rotate_left" : "rotate_down"}`}>
                                                                <img src={"/down-arrow.png"}/>
                                                            </span>
                                                        </div>

                                                        <div className={`${styles.answer_text} ${itemId && !itemId.includes(item.id)  ? "show_answer" : "hidden_answer"}`}>
                                                            <Radio id={index} onChange={answer => {
                                                                handleList(item.id, answer)
                                                            }} item={item.answers}/>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>

                                </div>
                                <Button
                                    onClick={() => {
                                        isLogin ? sendTempData() : dispatch(loginModalToggle(true))

                                    }}
                                    loading={loading}
                                    backgroundColor={"var(--fourth)"}
                                    text={"نتیجه تست"}
                                    icon={"/save_icon.png"}
                                    padding={5}
                                    margin={10}
                                    width={"250px"}
                                />
                            </>
                    }

                </div>
            </Container>
        </Layout>
    );
};

export default DeterminationOfTemperament;
