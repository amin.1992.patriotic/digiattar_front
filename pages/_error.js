import React from 'react';
import Layout from "../components/layout/layout";
const Error = (props) => {
    console.log(props)
    return (
        <Layout>
            <div className={"error"}>
                <img src={"/404-robot.gif"}/>
            </div>
        </Layout>
    );
};

export default Error;