import React, {useEffect, useState} from 'react';
import styles from "../../styles/search.module.css"
import Layout from "../../components/layout/layout";
import {Container, Grid} from "@material-ui/core";
import Api from "../../services/api";
import {useRouter} from "next/router";
import BlogBox from "../../components/blogBox/blogBox";
import {ENV} from "../../services/env";
import Pagination from "react-js-pagination";
import CategoryBox from "../../components/categoryBox/categoryBox";
import InlineLoader from "../../components/inlineLoader/inlineLoader";
import {separate} from "../../services/separate";

const Slug = () => {
    const router = useRouter()
    const [data, setData] = useState("")
    const [blogData, setBlogData] = useState("")
    const [loading, setLoading] = useState(false)
    const [modal, setModal] = useState(false)
    const [itemId, setItemId] = useState(null)
    const [word, setWord] = useState(null)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'});
        setLoading(true)
        let searchWord = localStorage.getItem("word")
        setWord(searchWord)
        new Api().get(`/search`, {q: searchWord, type: "site"}).then((response) => {
            if (typeof response !== "undefined") {
                if (response) {
                    setLoading(false)
                    setData(response)
                } else {
                    setLoading(false)
                }
            }
        })
        new Api().get(`/search`, {q: searchWord, type: "blog"}).then((response) => {
            if (typeof response !== "undefined") {
                if (response) {
                    setLoading(false)
                    setBlogData(response)
                } else {
                    setLoading(false)
                }
            }
        })
    }, [router])
    return (
        <Layout>
            <Container>
                {loading && <div style={{
                    position: "fixed",
                    right: "0",
                    width: "100%",
                    height: "100%",
                    zIndex: "1000",
                    display: "flex",
                    alignItems: "center",
                    background: "white",
                    justifyContent: "center",
                    top: 0
                }}>
                    <img src={("/loading.gif")}/>
                </div>
                }
                {

                    (data && data.contents && data.contents.length > 0 || data && data.other_name && data.other_name.length > 0) &&
                    <>
                        <div className={styles.search_title}>
                            نتایج جستجو در فروشگاه
                        </div>
                        <div className={styles.search_result}>
                            <Grid xs={12} container={true}>
                                {
                                    data && data.categories && data.categories.length > 0 &&
                                    <>
                                        <b className="search_main_results_title">
                                            {`نتایج ${word} در دسته بندی محصولات : `}
                                        </b>
                                        <div className={"search_category"}>
                                            <div className={"search_category_section"}>
                                                {
                                                    data && data.categories && data.categories.map((item) => {
                                                        return (
                                                            <div onClick={() => {
                                                                router.push(`/shop/category/[...param]`, `/shop/category/${item.id}/${item.slug}`);
                                                            }} className={"search_category_section_item"}>
                                                                {item.title}
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </>
                                }
                                <div style={{width: "100%", margin: "10px 0 "}}>
                                    <b className="search_main_results_title">
                                        {`نتایج ${word} در محصولات : `}
                                    </b>
                                </div>
                                {
                                    data && data.contents && data.contents.map((item) => {
                                        return (
                                            <Grid item lg={4} sm={6} xs={12}>
                                                <div onClick={() => {
                                                    router.push(`/product/[...param]?id=${item.id}&slug=${item.slug}`, `/product/${item.id}/${item.slug}`)
                                                }} className={styles.search_items}>
                                                        {
                                                            (item.img === null || item.img === "") ?
                                                                <img src={"/emty_image.png"}/>
                                                                :
                                                                <img
                                                                    src={ENV["API"]["IMG"] + "/product/" + item.id + "/100/" + item.img}/>
                                                        }
                                                       <div className={"search_main_results_box_detail"} >
                                                           <b>
                                                             {item.title}
                                                           </b>
                                                           <b  className={"search_main_results_box_detail_price"} >
                                                               {(item.types[0] && item.types[0].package_count < 1 || typeof item.types[0] === "undefined") ? "ناموجود" :item.types[0] && separate(item.types[0].price) + " تومان " }
                                                           </b>
                                                       </div>
                                                </div>
                                            </Grid>
                                        );
                                    })
                                }
                                <div style={{width: "100%", margin: "10px 0 "}}>
                                    <b className="search_main_results_title">
                                        {`نتایج ${word} در سایر نام های محصولات : `}
                                    </b>
                                </div>
                                {data && data.other_name && data.other_name.map((item) => {
                                    return (
                                        <Grid item lg={4} sm={6} xs={12}>
                                            <div className={styles.search_items}>
                                                {
                                                    (item.img === null || item.img === "") ?
                                                        <img src={"/emty_image.png"}/>
                                                        :
                                                        <img src={ENV["API"]["IMG"] + "/product/" + item.id + "/100/" + item.img}/>
                                                }
                                                <span>
                                                    <div  onClick={(e) => {
                                                          router.push(`/product/[...param]?id=${item.id}&slug=${item.slug}`, `/product/${item.id}/${item.slug}`)}}>
                                                          <div className={styles.search_items_title}
                                                               dangerouslySetInnerHTML={{__html: item.title.replaceAll(word, '<b style="color:red !important;">' + word + '</b>')}}
                                                          />
                                                        <div style={{width: "100%", marginTop: "10px", color: "rgba(0,0,0,.7)"}}>
                                                            <div style={{height: "75px"}}
                                                                 className={`${item.heading.length > 100 && "search_main_results_list_des"}`}
                                                                 dangerouslySetInnerHTML={{__html: item.heading.replaceAll(word, '<b style="color:red !important;">' + word + '</b>').substr(0, 100)}}/>
                                                        </div>
                                                    </div>

                                                    {
                                                        item.heading.length > 100 &&
                                                        <b onMouseOver={() => {
                                                            setModal(true);
                                                            setItemId(item.id)
                                                        }} onMouseOut={() => {
                                                            setModal(false)
                                                        }} className={"more_btn_search_item"} style={{color: "green"}}>نام
                                                            های
                                                            بیشتر
                                                            <div
                                                                className={`search_modal_more_name ${(modal && item.id === itemId) ? "more_name_show" : "more_name_hide"}`}
                                                                style={{
                                                                    marginTop: "10px",
                                                                    color: "rgba(0,0,0,.7)",
                                                                    right: "20%"
                                                                }}>
                                                                <div
                                                                    dangerouslySetInnerHTML={{__html: item.heading.replaceAll(word, '<b style="color:red !important;">' + word + '</b>')}}/>
                                                            </div>
                                                        </b>
                                                    }
                                                </span>
                                            </div>
                                        </Grid>
                                    );
                                })}
                            </Grid>
                        </div>
                    </>
                }
                {
                    blogData && blogData.contents && blogData.contents.length > 0 &&
                    <>
                        <div className={styles.search_title}>
                            نتایج جستجو در وبلاگ
                        </div>
                        <div className={styles.search_result}>
                            <Grid xs={12} container={true}>
                                <div style={{width: "100%", margin: "10px 0 "}}>
                                    <b className="search_main_results_title">
                                        {`نتایج ${word} در مقالات : `}
                                    </b>
                                </div>
                                {blogData && blogData.contents && blogData.contents.map((item) => {
                                    return (
                                        <Grid item lg={4} sm={6} xs={12}>
                                            <div onClick={() => {
                                                router.push(`/blog/post/[...param]?id=${item.id}&slug=${item.slug}`, `/blog/post/${item.id}/${item.slug}`)
                                            }} className={styles.search_items}>
                                                {
                                                    (item.img === null || item.img === "") ?
                                                        <img src={"/emty_image.png"}/>
                                                        :
                                                        <img
                                                            src={ENV["API"]["IMG"] + "/content/" + item.id + "/100/" + item.img}/>
                                                }
                                                <span>
                                           {item.title}
                                       </span>
                                            </div>
                                        </Grid>
                                    );
                                })}
                            </Grid>
                        </div>
                    </>
                }

            </Container>
        </Layout>
    );
};

export default Slug;