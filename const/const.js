const CONST={
    'k': 'c0ff70cc197a07dff1fb709688170426',
    'i': 'f8b4e45085a1045902c3c69c80e67a7c',
    radio:[
        {id:1,title :  "براساس حجم" ,
            value:[
                {id:1,title:" یک لیتری"},
                {id:2,title:" دو لیتری"},
                {id:3,title:" سه لیتری"},
                {id:4,title:" پنج لیتری"},
            ]
        },
        {id:1,title :  "براساس اندازه" ,
            value:[
                {id:5,title:" یک متر"},
                {id:6,title:" دو متر"},
                {id:7,title:" سه متر"},
                {id:8,title:" پنج متر"},
            ]
        },
        {id:1,title :  "براساس وزن" ,
            value:[
                {id:9,title:" یک کیلو"},
                {id:10,title:" دو کیلو"},
                {id:11,title:" سه کیلو"},
                {id:12,title:" پنج کیلو"},
            ]
        },
    ]
}
export default CONST
