import React from 'react';
import {SHOPPING_LINE} from "../../redux/types";
import {connect} from "react-redux";
import Container from "@material-ui/core/Container";
import {useRouter} from "next/router";

const StageBar = (props) => {
    const  rooter = useRouter()
    return (
        <div className="stage_bar">
            <div className={`circle was_seen `}>
                <div>سبد خرید</div>
            </div>
            <div className={`line ${ props.data.step === "step_two" || props.data.step === "step_tree" ? "was_seen" : "not_seen"}`}/>
            <div className={`circle ${props.data.step === "step_two" || props.data.step === "step_tree" ? "was_seen" : "not_seen"}`}>
                <div>تکمیل کردن اطلاعات</div>
            </div>
            <div  className={`line ${props.data.step === "step_tree" ? "was_seen" : "not_seen"}`}/>
            <div className={`circle ${ props.data.step === "step_tree" ? "was_seen" : "not_seen"}`}>
                <div>فاکتور</div>
            </div>
            <div className="prev_step" onClick={()=>{rooter.back()}}>
                <img src={"/right_arrow.png"}/>
                <span>مرحله قبل</span>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        data: state.others,
    }
}

export default connect(mapStateToProps)(StageBar);
