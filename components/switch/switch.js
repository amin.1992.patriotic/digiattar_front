import React, {useEffect, useState} from 'react';

const Switch = (props) => {
    const  [ switchOnOrOff , setSwitchOnOrOff ] = useState()
    useEffect(()=>{
        setSwitchOnOrOff(props.id == 1 ? true : false)
    },[props.id])
    return (
        <div className="switch">
            <input
                onChange={(e) => {
                    setSwitchOnOrOff(!switchOnOrOff)
                    props.onChange(switchOnOrOff ? 0 : 1)
                }} className="styled-switch" id={"switch" + props.id} checked={switchOnOrOff} type="checkBox" value={props.id}/>
            <label htmlFor={"switch" + props.id}/>
            <style jsx>
                {`
                    .styled-checkbox:checked + label:before {
                       background:red;
                    }
                 `}
            </style>
        </div>
    );
};

export default Switch;
