import React from 'react';

const Loading = (props) => {
    return (
        <>
            <div style={{borderRadius:props.radius + "px"}} className="modal_loading">
                <img  src={"/spinner.svg"}/>
                <style jsx>
                    {`
                          .modal_loading {
                             position: ${props.position};
                           }
                          .loading {
                                top: calc(50% - ${(props.width) / 2}px);
                                right: calc(50% - ${(props.height) / 2}px);
                                width : ${props.width}px;
                                height : ${props.height}px;
                             }
                    `}
                </style>
            </div>
        </>
    );
};

export default Loading;
