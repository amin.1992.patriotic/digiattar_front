import React, {useState, useEffect} from 'react';
import styles from '../../styles/Header.module.css';
import {Grid} from "@material-ui/core";
import Container from '@material-ui/core/Container';
import LoginModal from "../loginModal/LoginModal";
import Link from "next/link";
import {useRouter} from "next/router";
import SearchMain from "../searchMain/searchMain";
import Api from "../../services/api";
import {useDispatch, useSelector} from "react-redux";
import Cookies from "universal-cookie";
import {menu, logOut, loginModalToggle, shoppingLine} from "../../redux/actions";
import {toast} from "react-toastify";
import {useQuery} from "react-query";



const Header = (props) => {
    const menuRef = React.createRef()
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
    const loginData = useSelector((state) => state.loginReducer)
    const toggleLoginModal = useSelector(state => state.others.toggleLoginModal)
    const cookies = new Cookies();
    const isLogin = useSelector((state) => state.loginReducer.login)
    const router = useRouter()
    const [modalWidth, setModalWidth] = useState(0)
    const [modalHeight, setModalHeight] = useState(0)
    const [isMobile, setIsMobile] = useState(false)
    const [modalToggle, setModalToggle] = useState(false)
    const [expanded, setExpanded] = useState([])
    const [profileModal, setProfileModal] = useState("none")
    let data = useSelector((state) => {
        return state.menu.data
    })

    const fetchMenu = async () => {
        const data = await new Api().get(`/menu`, {})
            .then((res) => {
                dispatch(menu(res))
                return res
            }).catch((err) => {
                throw err
            })
        return data
    }

    useEffect(() => {
        window.addEventListener("resize", () => {
            if (typeof window !== undefined) {
                setModalWidth(window.innerWidth)
                setModalHeight(window.innerHeight)
            }
            if (modalWidth < 960) {
                setIsMobile(true)
            } else {
                setIsMobile(false)
            }
        })
        if (modalWidth < 960) {
            setIsMobile(true)
        } else {
            setIsMobile(false)
        }
        setModalWidth(window.innerWidth)
        setModalHeight(window.innerHeight)

    }, [])

    useEffect(() => {
        if(!data)
        data = fetchMenu

        setExpanded(expanded)
    }, [])

    const setLogOut = async () => {
        await cookies.remove('auth')
        await setProfileModal("none")
        await dispatch(logOut())
    }
    const closeProfile = () => {
        setProfileModal("none")

    }
    const handleModal = () => {
        setProfileModal("flex");
        dispatch(loginModalToggle(false))
    }
    const handleCategory = (id, slug) => {
        router.push(`/shop/category/[...param]`, `/shop/category/${id}/${slug}`)
        setModalToggle(false)
    }


    const handleAddExpand = async (value, child) => {
        if (expanded.includes(value)) {
            const newArr = [...expanded]
            const index = expanded.indexOf(value);
            await newArr.splice(index, 1);
            await setExpanded(newArr)
        } else {
            const newArr = [...expanded]
            await newArr.push(value);
            await setExpanded(newArr)
        }

    }
    const renderBlogTree = (props) => {
        // nodes mapping
        const treeNodes = props && props.map((item, key) => {
            // value id node
            const {id} = item;
            // label title node
            const {title} = item;
            // slug
            const {slug} = item;

            // check has child
            const hasChild = item.children.length > 0;

            // check has child is true fetch children

            const children = hasChild ? renderBlogTree(item.children) : '';
            return (
                <>
                    <div
                        className={styles.tree_view}>
                        {hasChild === true &&
                        <img onClick={(e) => e.target === e.currentTarget && handleAddExpand(id, item.children)}
                             src={!expanded.includes(id) ? "/plus_icon.svg" : "/minus_icon.svg"}/>
                        }
                        <li key={key} className={styles.tree_box}>
                          <span
                              className={
                                  hasChild === true
                                      ? 'tree-parent has-child'
                                      : 'tree-parent  has-no-child'
                              }
                          >
                          </span>
                            <div onClick={(e) => {
                                e.target === e.currentTarget && handleCategory(id, slug)
                            }} className={styles.tree_box_title}>{title}</div>
                            {hasChild === true && (
                                <ul
                                    style={{
                                        display: expanded.includes(id)
                                            ? 'flex'
                                            : 'none'
                                    }}
                                    className={styles.tree_children}
                                >
                                    {children}
                                </ul>
                            )}
                        </li>
                    </div>
                </>
            );
        });
        return treeNodes;
    }
    return (
        <>
            <div className={styles.header}>
                <LoginModal onchange={(e) => {
                    dispatch(loginModalToggle(e))
                }} show={toggleLoginModal}/>
                <span/>
                <Container>
                    <Grid container={true}>
                        <div className={styles.header__section}>
                            <div className={styles.header_right_section}>
                                <Link href={"/"}>
                                    <img className={styles.logo} alt={"لوگو"} src={'/logo.png'}/>
                                </Link>
                                <div className={styles.compose}>
                                    <div onClick={() => {
                                        setModalToggle(true)
                                    }} className={styles.grouping}>
                                        <img alt={"ایکون"} src={"/Group.png"}/>
                                        <span> دسته بندی ها</span>
                                    </div>
                                    <div className={styles.header_list}>
                                        <Link href={"/blog"}>
                                            <a>
                                                وبلاگ
                                            </a>
                                        </Link>
                                    </div>
                                    <div className={styles.header_list}>
                                        <Link href={"/bmi"}>
                                            <a>
                                                {"محاسبه BMI"}
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                                <div onClick={(e) => {
                                    e.target === e.currentTarget && setModalToggle(false)
                                }} className={modalToggle ? styles.open_menu : styles.menu_modal}>
                                    <div ref={menuRef}
                                         className={`${modalToggle ? styles.open_box : styles.menu_box_close} ${styles.menu_box}`}>
                                        <div className={styles.menu_logo}>
                                            <img src={"/logo.png"}/>
                                        </div>
                                        <Link href={"/shop"} prefetch={false}>
                                            <div onClick={() => {
                                                setModalToggle(false)
                                            }} className={styles.tree_box_title}>
                                                {"فروشگاه"}
                                            </div>
                                        </Link>

                                        {renderBlogTree(data?.product_categories)}
                                    </div>
                                </div>
                            </div>
                            <div className={styles.header_left_section}>
                                <SearchMain/>
                                <div onClick={(e) => {
                                    !loginData.login ? dispatch(loginModalToggle(true)) : handleModal()
                                }} className={styles.account}>
                                    {!loginData.login ?
                                        <>
                                            <span className={styles.account_img}/>
                                            <span> ورود / عضویت</span>
                                        </>
                                        :
                                        <>
                                            <span className={styles.account_img}/>
                                            <img className={styles.account_img_arrow} src={"/arrow-down.svg"}/>
                                        </>
                                    }
                                </div>
                                {
                                    // loginData.login &&
                                    <div id="profile_box" className={styles.profile_box}>
                                        <div className={styles.profile_close}>
                                            <div onClick={(e) => {
                                                e.target === e.currentTarget && closeProfile()
                                            }}/>
                                        </div>
                                        <div>
                                            <img src={"/profile.svg"}/>
                                            <div onClick={() => {
                                                router.push("/profile")
                                            }} className={styles.detail}>
                                                <div>
                                                    {
                                                        loginData.user.name !== null ?
                                                            loginData.user.name + " " + loginData.user.family
                                                            :
                                                            loginData.user.mobile
                                                    }
                                                </div>
                                                <div>
                                                    <span>مشاهده پروفایل کاربری</span>
                                                    <img style={{transform: "rotate(180deg)"}} src={"/arrow-down.svg"}/>
                                                </div>
                                            </div>
                                        </div>
                                        <div onClick={setLogOut}>
                                            <img src={"/logout.svg"}/>
                                            <span>خروج از حساب کاربری</span>
                                        </div>
                                    </div>
                                }
                                <div onClick={() => {
                                    router.push("/cart")
                                }} className={styles.cart}>
                                    {
                                        isLogin &&
                                        <div className={styles.counter}>
                                            {cart.prevDataCount}
                                        </div>
                                    }
                                    <span className={styles.cart_icon}/>
                                    <span>سبد خرید</span>
                                </div>
                            </div>
                        </div>
                        <style jsx>
                            {`
                          #profile_box {
                            display:${profileModal}
                          }
                        `}
                        </style>
                    </Grid>
                </Container>
            </div>
        </>
    );
};

export default Header;
