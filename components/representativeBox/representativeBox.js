import React from 'react';

const RepresentativeBox = (props) => {
    return (
        <div className="representative_box">
            <img alt={"پروفایل نمایندگی"} className="representative_box_image" src={props.src} />
            <span className="representative_box_title" >{props.title}</span>
            <span className="representative_box_des" >{props.description}</span>
            <span className="representative_box_label" > {props.label}</span>
            <span className="representative_box_list" > {props.list}</span>
        </div>
    );
};

export default RepresentativeBox;
