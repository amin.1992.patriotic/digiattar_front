import React, {Component} from 'react';
import styles from "../../styles/emtyCart.module.css"
import Container from '@material-ui/core/Container';
import Footer from "../footer/footer"
import Header from "../header/header"
class EmtyCart extends Component {
    render() {
        return (
            <>
                <div className={styles.emty_cart_main}>
               <Header/>
                    <Container>
                        <div className={styles.emty_cart}>
                            <img src={"/emtyCart.png"}/>
                             <p>سبد خرید شما خالی است</p>
                            <div className={styles.emty_cart_btn}>بازگشت به فروشگاه</div>
                            <div className={styles.return_to_shop}>
                                <p>محصولات پیشنهادی</p>

                                <div className={styles.recommended_products}>
                                    <div className={styles.recommended_products_box}>
                                        <div className={styles.recommended_products_img}>
                                            <img src={"/g4.png"}/>
                                        </div>
                                        <div className={styles.recommended_products_information}>
                                        <span className={styles.recommended_products_title}>عرق 40 گیاه دو آتیشه سبلان</span>
                                               <div className={styles.recommended_products_price}>
                                                   <span className={styles.recommended_products_price_orginal}> 38.000</span>
                                                   <span className={styles.recommended_products_price_off}>46.000</span>
                                               </div>
                                           </div>
                                           <div className={styles.recommended_products_number}>
                                               <span className={styles.recommended_products_number_plus_icon}>
                                                   <img src={"/plus_icon.png"}/>
                                               </span>
                                               <span className={styles.recommended_products_number_input}>12</span>
                                               <span className={styles.recommended_products_number_minus_icon}>
                                                   <img src={"/minus_icon.png"}/>
                                               </span>
                                           </div>
                                           <div className={styles.recommended_products_btn}>
                                               <img src={"/basket.png"}/>
                                               افزودن به سبد خرید
                                           </div>
                                    </div>
                                </div>
                                <div className={styles.recommended_products}>
                                    <div className={styles.recommended_products_box}>
                                        <div className={styles.recommended_products_img}>
                                            <img src={"/g4.png"}/>
                                        </div>
                                        <div className={styles.recommended_products_information}>
                                        <span className={styles.recommended_products_title}>عرق 40 گیاه دو آتیشه سبلان</span>
                                               <div className={styles.recommended_products_price}>
                                                   <span className={styles.recommended_products_price_orginal}> 38.000</span>
                                                   <span className={styles.recommended_products_price_off}>46.000</span>
                                               </div>
                                           </div>
                                           <div className={styles.recommended_products_number}>
                                               <span className={styles.recommended_products_number_plus_icon}>
                                                   <img src={"/plus_icon.png"}/>
                                               </span>
                                               <span className={styles.recommended_products_number_input}>12</span>
                                               <span className={styles.recommended_products_number_minus_icon}>
                                                   <img src={"/minus_icon.png"}/>
                                               </span>
                                           </div>
                                           <div className={styles.recommended_products_btn}>
                                               <img src={"/basket.png"}/>
                                               افزودن به سبد خرید
                                           </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
              <Footer/>
                </div>
            </>
        );
    }
}

export default EmtyCart;
