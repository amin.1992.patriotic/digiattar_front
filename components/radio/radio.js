import React, {useEffect, useState} from 'react';

const Radio = (props) => {
    const [value , setValue] = useState("")
    const handleChange = (e) => {
        setValue(e.target.value)
        props.onChange(e.target.value)
    }
    return (
        <div className="check_section">
            {
                props?.item?.map((item) => {
                    return (
                        <div className="radio_box">
                            <input
                                id={item.id}
                                name={item.answer_text}
                                onChange={(e) => {handleChange(e)}}
                                className="styled-radio_box"
                                type="radio"
                                checked={parseInt(value) === parseInt(item.temperament_attribute_id)}
                                value={item.temperament_attribute_id}
                            />
                            <label htmlFor={item.id}>
                                {item.answer_text}
                            </label>
                        </div>
                    )
                })
            }
        </div>
    );
};

export default Radio;
