import React from 'react';
import styles from "../../styles/groupButton.module.css"
const GroupButton = (props) => {
    return (
        <div  className={styles.groupButton}>
            {props.children}
        </div>
    );
};

export default GroupButton;