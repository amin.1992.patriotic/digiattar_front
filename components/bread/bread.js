import React, {useEffect, useState} from 'react';
import Link from "next/link";
import {useRouter} from "next/router";

const Bread = (props) => {
    const router = useRouter()
    console.log(props)
    return (
        <div className="bread">
            <ul>
                <li>
                    <Link href={`/`}>
                        <a>
                            <span>{" دیجی عطار "}</span>
                        </a>
                    </Link>
                </li>
                {props && props.item.map((item) => {
                    return (
                        <li>
                            <Link href={`${item.link}`}>
                                <a>
                                    <span>{item.name}</span>
                                </a>
                            </Link>
                        </li>
                    )
                })}
            </ul>
        </div>
    );
};

export default Bread;
