import React, {useState} from 'react';
import styled from "./postRadio.module.css"

const PostRadio = (props) => {
    const [ value , setValue ] = useState(1)
    const { name } = props.item
    const { id } = props.item
    const { status } = props.item
    const { description } = props.item
    const { minimum_free_forwarding } = props.item
    const { default_cost } = props.item

    const handleValue = (e) => {
        setValue(e.target.id)
        props.onChange(e.target.id)
    }

    return (
        <div style={{display:+status !== 1 && "none"}} className={styled.post_radio}>
            <input onChange={(e) => handleValue(e)} id={id} value={ value } name={"post"} type={"radio"}/>
            <label htmlFor={id}>
                <div  className={styled.post_radio_detail}>
                    <img src={"/fast.png"}/>
                    <div className={styled.post_radio_label}>
                        <div>
                            <strong>نحوه ارسال : </strong>
                            {name}
                        </div>
                        <div>
                            <strong>قیمت : </strong>
                            {default_cost}
                        </div>
                        <div>
                            <strong> حداقل هزینه ارسال رایگان: </strong>
                            {minimum_free_forwarding}
                        </div>
                    </div>
                </div>
                {description &&
                <div>
                    <strong>توضیحات : </strong>
                    {description}
                </div>
                }
            </label>
        </div>
    );
};

export default PostRadio;