import React from 'react';
import styles from"../../styles/latestContent.module.css"
import Grid from "@material-ui/core/Grid";
import ContentBox from "../contentBox/contentBox";
import Line from "../line/line";
import {ENV} from "../../services/env";
const  LatestContent = (props) => {
    return (
            <div className={styles.latest_content}>
                <Line title={" تازه ترین مطالب "}/>
                <Grid spacing={3} container={true}>
                    {
                        props.data && props.data.data && props.data.data.map((item)=>{
                            return(
                                <Grid item md={3} sm={6} xs={12}>
                                    <ContentBox
                                        title={item.title}
                                        src={item.img && ENV["API"]["IMG"] + "/content/" + item.id + "/" + item.img}
                                        des={item.content}
                                        visitor={item.visitor}
                                        created_at={item.created_at}
                                        id={item.id}
                                        slug={item.slug}
                                    />
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </div>
    );
};

export default  LatestContent;
