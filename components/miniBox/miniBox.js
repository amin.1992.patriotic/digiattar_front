import React from 'react';
import {useRouter} from "next/router";
import {separate} from "../../services/separate";

const MiniBox = (props) => {
    const router = useRouter()
    return (
        <div onClick={() => {router.push(`/product?id=${props.id}&slug=${props.slug}`, `/product/${props.id}/${props.slug}`)}} className='mini_box'>
            <div>
                <div className='mini_box_title'>{props.title && props.title.substr(0,25)}</div>
                <div className='mini_box_total'>
                    {
                        <div  className='mini_box_off'> { typeof props.off === "undefined" ? " " : separate(props.off)}</div>
                    }

                    {
                        <div style={{color:props.off === 0 || props.off === props.price && "transparent"}} className='mini_box_price'>{props.price === 0 ? "ناموجود" :typeof props.price === "undefined" ? " " : separate(props.price)}</div>
                    }

                </div>
            </div>
            <div className='mini_box_image'>
                <img src={props.src}/>
            </div>
        </div>
    );
};

export default MiniBox;
