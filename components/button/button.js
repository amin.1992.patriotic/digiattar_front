import React from 'react';
import Loading from "../loading/loading";

const Button = (props) => {
    return (
        <div onClick={(e)=>{props.onClick && props.onClick(e)}} style={{backgroundColor:props.backgroundColor ,width:props.width}} className={`button ${props.disable && "disable"}`}>
            <span className="button-icon"><img src={props.icon}/></span>
            {
                props.loading ? <Loading position={"absolute"}/> :  <span>{props.text}</span>
            }

             <style jsx>
                 {`
                 .button{
                     margin: 20px ${props.margin}px;
                     font-size : ${props.fontSize}px;
                     opacity:${props.opacity};
                     cursor:${props.cursor};
                     pointer-events:${props.events};
                     }
                 `}
             </style>
        </div>
    );
};

export default Button;
