import React, {useState} from 'react';
import Api from "../../services/api";
import {toast} from "react-toastify";
import {loginModalToggle} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";
import {reminder} from "../../functions/functions";

const Share = (props) => {

    const [scrollHeight , setScrollHeight] = useState(0)
    const [like , setLike] = useState(false)
    const [bell , setBell] = useState(false)
    const {id} = props
    const {email} = props
    const {phone} = props
    const refs = React.createRef()
    const  handleSocial = () => {
        setScrollHeight(refs.current && refs.current.scrollWidth)
    }
    const isLogin = useSelector((state) => state.loginReducer.login);
    const dispatch = useDispatch();

    const getDeviceType = ()=> {
        const ua = navigator.userAgent;
        if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
            return "tablet";
        }
        if (/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(ua)) {
            return "mobile";
        }
        return "desktop";
    };
    const handleReminder = () => {
        setBell(true)
        reminder(phone , email , id ,"product")
    }
    const handleLike = () => {
        setLike(!like)
        new Api()[!like ? "post" : "delete" ](`/like/${id}`, {} ,true)
            .then((resp) => {
                if (typeof resp != "undefined") {
                  toast.success(resp.msg)
                } else {
                    setLike(!like)
                }
            })
            .catch((err) => {
                throw err
                setLike(!like)
            })
    }
    return (
        <div style={{position:props.position , width:props.width}} className={"main_icons"}>
            {
                !props.share &&
                <div
                    onClick={() =>  isLogin ? handleReminder() : dispatch(loginModalToggle(true))}
                    style={{width:props.width}}
                    className={"share_to_social"}>
                    <img src={!bell ? "/ring.svg" : "/bell-ring.png"}/>
                </div>
            }
            {
                <div  onMouseOver={handleSocial} onMouseLeave={()=>{setScrollHeight(0)}} className={"share_to_social"}>
                    <img src={"/share.svg"}/>
                    <div style={{right:-(scrollHeight) + "px"}}  ref={refs}>
                        <img onClick={()=>{ window.open(`https://${getDeviceType() === "desktop" ? "web" : "api"}.whatsapp.com/send?text=${window.location.href}`)}} src={"/whatsapp.svg"}/>
                        <img onClick={()=>{ window.open(`https://telegram.me/share/url?text=&url=${window.location.href}`)}} src={"/telegram.svg"}/>
                    </div>
                </div>
            }

            {
                !props.share &&
                <div
                     onClick={() =>  isLogin ? handleLike() : dispatch(loginModalToggle(true))}
                     className={"share_to_social"}>
                    <img src={!like ? "/heart.svg" : "/heart-red.svg"}/>
                </div>
            }

        </div>

    );
};

export default Share;
