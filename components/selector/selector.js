import React, {createRef, useEffect, useState} from 'react';
import {useSelector} from "react-redux";

const Selector = (props) => {
    const [options, setOptions] = useState([]);
    const selector = props.selector

    useEffect(() => {
        let n_options = [];
        n_options[0] = selector;
        setOptions([...n_options]);
    }, [selector]);




    const handleAddElement = (e, index) => {
        props.onChange(e.target && e.target.value)
        let n_options = options;
        n_options.splice(index + 1);
        setOptions([...n_options]);
        if (e.target.selectedIndex !== 0 && n_options[ index ][ e.target.selectedIndex - 1 ].children && n_options[ index ][ e.target.selectedIndex - 1 ].children.length > 0) {
            n_options[ index + 1 ] = n_options[ index ][ e.target.selectedIndex - 1 ].children;
            setOptions([...n_options]);
            props.onChange("")
        }
    }

    return (

        <div  className="selector">
            {
                options.map((op, index) => {
                    return (
                        <select  onChange={(e) => handleAddElement(e, index)}>
                            <option value={""}>{"لطفا انتخاب کنید"}</option>
                            {
                                op && op.map((item,index)=>{
                                    return(
                                        <option value={item.id}>{item.title}</option>
                                    )
                                })
                            }
                        </select>
                    );
                })

            }
        </div>
    );
};

export default Selector;
