import React from 'react';
import styles from "../../styles/subMenu.module.css"
import {useRouter} from "next/router";
import Link from "next/link";

const SubMenu = (props) => {
    const router = useRouter()
    const subMenuList = props.menuList
    return (
        <div className={styles.container_sub_menu}>
            <ul className={styles.sub_menu_list}>

                <li>
                    <Link href={`/shop/category/[...param]`} as={`/shop/category/923/گیاهان_دارویی`}>
                       <a>
                           <div style={{backgroundPositionX: 130, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                           <span>{"گیاهان دارویی"}</span></a>
                    </Link>
                </li>
                <li onClick={(e) => {router.push(`/shop/category/[...param]`, `/shop/category/924/داروهای_گیاهی`)}}>
                    <div style={{backgroundPositionX: 341, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                    <span>{"داروهای گیاهی"}</span>
                </li>
                <li onClick={(e) => {router.push(`/shop/category/[...param]`, `/shop/category/925/عرقیات`)}}>
                    <div style={{backgroundPositionX: 238, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                    <span>{"عرقیات"}</span>
                </li>
                <li onClick={(e) => {router.push(`/shop/category/[...param]`, `/shop/category/926/محصولات_ارگانیک`)}}>
                    <div  style={{backgroundPositionX: 660, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                    <span>{"محصولات ارگانیک"}</span>
                </li>
                <li onClick={(e) => {router.push(`/shop/category/[...param]`, `/shop/category/927/ملزومات_زندگی_سلامت`)}}>
                    <div style={{backgroundPositionX: 765, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                    <span>{"ملزومات زندگی سلامت"}</span>
                </li>
                <li onClick={(e) => {router.push(`/shop/category/[...param]`, `/shop/category/928/پوست_مو_زیبایی`)}}>
                    <div style={{backgroundPositionX: -153, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                    <span>{"پوست/مو/زیبایی"}</span>
                </li>
                <li onClick={(e) => {router.push(`/shop/category/[...param]`, `/shop/category/959/روغن `)}}>
                    <div style={{backgroundPositionX: 553, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                    <span>{"روغن ها"}</span>
                </li>
                <li onClick={(e) => {router.push(`/shop/category/[...param]`, `/shop/ccategory/960/دمنوش `)}}>
                    <div style={{backgroundPositionX: 450, backgroundPositionY: 118}} className={styles.sub_menu_list_image}/>
                    <span>{"دمنوش ها"}</span>
                </li>
            </ul>
        </div>
    );
};

export default SubMenu;
