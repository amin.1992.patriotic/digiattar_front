import React from 'react';

const Search = () => {
    return (
        <div>
            <label className="search">
                <input  placeholder={"جستجو کنید"}/>
                 <div className="search_btn">
                     <img src={"assets/dotted.png"}/>
                 </div>
            </label>

        </div>
    );
};

export default Search;
