import React, {useEffect, useState, memo} from 'react';
import Api from "../../services/api";
import {ENV} from "../../services/env";
import {useRouter} from "next/router";
import Slider from "../slider/slider";

const SearchMain = (props) => {
    const router = useRouter()
    const [searchWord, setSearchWord] = useState("")
    const [modalToggle, setModalToggle] = useState(false)
    const [data, setData] = useState("")
    const [blogData, setBlogData] = useState("")
    const [searchTime, setSearchTime] = useState("")
    const [tagData, setTagData] = useState("")
    const [loading, setLoading] = useState(false)


    const getData = (e) => {
        const word = e.target && e.target.value
        setSearchWord(word);
        if (searchTime) {
            clearTimeout(searchTime)
        }
        let loop = setTimeout(() => {
            if (word && word.length > 1) {
                setLoading(true)
                new Api().get(`/search`, {q: word, type: "site"}).then((response) => {
                    if (typeof response !== "undefined") {
                        if (response) {
                            setLoading(false)
                            setData(response)
                            localStorage.setItem("word", word)
                        } else {
                            setLoading(false)

                            console.log("err")
                        }
                    }
                })
                new Api().get(`/search`, {q: word, type: "blog"}).then((response) => {
                    if (typeof response !== "undefined") {
                        if (response) {
                            setLoading(false)
                            localStorage.setItem("word", word)
                            setBlogData(response)
                        } else {
                            setLoading(false)

                            console.log("err")
                        }
                    }
                })
                new Api().get(`/search`, {q: word, type: "tag"}).then((response) => {
                    if (typeof response !== "undefined") {
                        if (response) {
                            setLoading(false)
                            setTagData(response)
                            localStorage.setItem("word", word)
                        } else {
                            setLoading(false)
                            console.log("err")
                        }
                    }
                })
            } else {
                return false
            }
        }, 500)
        setSearchTime(loop)

    }
    return (
        <>
            <div className="search_main">
                <div className="search_icon" onClick={() => {
                    setModalToggle(true)
                }}/>
                <div onMouseDown={(e) => {
                    e.target === e.currentTarget && setModalToggle(false)
                }} className={`search_modal ${modalToggle ? "open_search" : ''}`}>
                    <div className={`search_modal_box ${modalToggle ? "open_search_box" : ''}`}>
                        <div className="search_modal_box_head">
                            {
                                !loading ?
                                    <span>کلمه مورد نظر را جستجو کنید</span>
                                    :
                                    <img src={"/line-loading.svg"}/>
                            }
                        </div>
                        <input ref={r => r && r.focus()} placeholder={"جستجو کنید"} onChange={(e) => {
                            getData(e)
                        }} value={searchWord} type={"search"}/>
                        {
                            searchWord && searchWord.length > 0 &&
                            <div className="search_main_results">
                                {
                                    data && data.categories && data.categories.length > 0 &&
                                    <>
                                        <b className="search_main_results_title">
                                            {`نتایج ${searchWord} در دسته بندی محصولات : `}
                                        </b>
                                        <div className={"search_category"}>
                                            <div className={"search_category_section"}>
                                                {
                                                    data && data.categories && data.categories.map((item) => {
                                                        return (
                                                            <div onClick={() => {
                                                                router.push(`/shop/category/[...param]`, `/shop/category/${item.id}/${item.slug}`);
                                                                setModalToggle(false)
                                                            }} className={"search_category_section_item"}>
                                                                {item.title}
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </>
                                }

                                {
                                    (data && data.contents && data.contents.length > 0 || data && data.other_name && data.other_name.length > 0) &&
                                    <div className="search_main_results_position">
                                        <span> نتایج جستجو در فروشگاه</span>
                                        <span  onClick={() => {
                                            router.push(`/search/[slug]` , `/search/${searchWord}`);
                                            setModalToggle(false)
                                        }}>
                                            <span>
                                            {(parseInt(data && data.other_name && data.other_name.length) + parseInt(data && data.contents && data.contents.length))}
                                                مورد یافت  شد
                                            </span>
                                            <img src={"/arrow-down.svg"}/>
                                        </span>
                                    </div>
                                }

                                {
                                    (data && data.contents && data.contents.length > 0) && <Slider closeModal={(e) => {
                                        setModalToggle(e)
                                    }} path={"/product"} searchWord={searchWord} data={data.contents} type={"search"}/>
                                }

                                {
                                    (data && data.other_name && data.other_name.length > 0) &&
                                    <Slider closeModal={(e) => {
                                        setModalToggle(e)
                                    }} path={"/product"} searchWord={searchWord} data={data.other_name}
                                            type={"search_other"}/>
                                }

                                {
                                    ( blogData && blogData.categories && blogData.categories.length > 0 || blogData && blogData.contents && blogData.contents.length > 0 ) &&
                                    <div className="search_main_results_position">
                                        <span>نتایج جستجو در مقالات وبلاگ</span>
                                        <span onClick={() => {
                                            router.push(`/search/[slug]` , `/search/${searchWord}`);
                                            setModalToggle(false)
                                        }}>
                                            <span>
                                                {parseInt(blogData && blogData.contents && blogData.contents.length)}
                                                مورد یافت  شد
                                            </span>
                                           <img src={"/arrow-down.svg"}/>
                                        </span>
                                    </div>
                                }

                                {/*{*/}
                                {/*    data && data.categories && data.categories.length > 0 &&*/}
                                {/*    <>*/}
                                {/*        <b className="search_main_results_title">*/}
                                {/*            {`نتایج ${searchWord} در دسته بندی مقالات : `}*/}
                                {/*        </b>*/}
                                {/*        <div className={"search_category"}>*/}
                                {/*            <div className={"search_category_section"}>*/}
                                {/*                {*/}
                                {/*                    blogData && blogData.categories && blogData.categories.map((item) => {*/}
                                {/*                        return(*/}
                                {/*                            <div onClick={() => {router.push(`/blog/[[...param]]`,`/blog/${item.id}/${item.slug}`) ;setModalToggle(false)}} className={"search_category_section_item"}>*/}
                                {/*                                {item.title}*/}
                                {/*                            </div>*/}
                                {/*                        )*/}
                                {/*                    })*/}
                                {/*                }*/}
                                {/*            </div>*/}
                                {/*        </div>*/}
                                {/*    </>*/}
                                {/*}*/}
                                {
                                    blogData && blogData.contents && blogData.contents.length > 0 &&
                                    <Slider closeModal={(e) => {
                                        setModalToggle(e)
                                    }} path={"/product"} searchWord={searchWord} data={blogData.contents}
                                            type={"search_blog"}/>
                                }
                                <div className="tag">
                                    {
                                        blogData && blogData.tags && blogData.tags.map((item) => {
                                            return (
                                                <span onClick={() => {
                                                    router.push(`/tags?id=${item.id}`, `/tags/${item.id}`);
                                                    setModalToggle(false)
                                                }} className="search_main_results_list">
                                                   {item.name}
                                               </span>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        }
                    </div>

                </div>
            </div>
        </>
    );
};


export default memo(SearchMain);
