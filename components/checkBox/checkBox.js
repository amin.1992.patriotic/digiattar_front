import React, {useEffect, useState} from 'react';
let arr1 = [];
const CheckBox = (props) => {
    const radioArr = []
    const handleChange =  () => {
        radioArr.push(props.item.id , props.item.slug)
        props.onChange(radioArr)
    }
    const handleChangeCategory =  (e) => {
        let arr2 = [e.target && e.target.value];
        if(!arr1.includes(props.item.id.toString())){
            arr1.push(...arr2);
            props.onChange([props.item.id , props.item.name ,...arr1])
        }else{
           let index = arr1.indexOf((props.item.id.toString()))
           arr1.splice(index,1)
           props.onChange([props.item.id , props.item.name ,...arr1])
        }
    }
    return (
        <>
            {
                props.type === "radio" ?
                    <div className="checkBox">
                        <input
                            name={props.name}
                            onChange={(e) => {
                                handleChange()
                            }} className="styled-checkbox"  id={props.item && props.item.id} type="radio" value={props.item && props.item.id}/>
                        <label htmlFor={props.item && props.item.id}/>
                        <style jsx>
                            {`
                                .styled-checkbox:checked + label:before {
                                   background:${props && props.color};
                                }
                             `}
                        </style>
                    </div>
                    :
                    <div className="checkBox">
                        <input
                            checked={props.checked && props.checked.includes(props.item && props.item.id)}
                            name={props.name}
                            onChange={(e) => {
                                handleChangeCategory(e)
                            }} className="styled-checkbox"  id={props.name === undefined ? props.item && props.item.id : props.item && props.name + props.item.id} type="checkbox" value={props.item && props.item.id}/>
                        <label htmlFor={props.name === undefined ? props.item && props.item.id : props.item && props.name + props.item.id}/>
                        <style jsx>
                            {`
                                .styled-checkbox:checked + label:before {
                                   background:${props && props.color};
                                }
                             `}
                        </style>
                    </div>
            }

        </>
    );
};

export default CheckBox;
