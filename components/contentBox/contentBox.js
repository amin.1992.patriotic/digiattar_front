import React from 'react';
import moment from 'moment-jalali';
import {useRouter} from "next/router";
import Link from "next/link";

const ContentBox  = (props) => {
    const  router = useRouter()
    return (
        <div className="content_box">
           <div className="content_box_image">
               {props.src && <img alt="blog" src={props.src}/>}
           </div>
            <div  className="content_box_detail">
                <div className="content_box_title">
                    <Link href={`blog/post/[...param]?id=${props.id}&slug=${props.slug}`}  as={`/blog/post/${props.id}/${props.slug}`}>
                        <a>{props.title}</a>
                    </Link>
                </div>
                <div  className="content_box_des" dangerouslySetInnerHTML={{__html:props.des}}/>
                <ul className="content_box_reaction">
                    <li>
                        <img src={"/time.svg"}/>
                        <span>{moment(props.created_at).format(' jYYYY/jM/jD')}</span>
                    </li>
                </ul>

                <div className="more_btn">
                    <Link href={`blog/post/[...param]?id=${props.id}&slug=${props.slug}`}  as={`/blog/post/${props.id}/${props.slug}`}>
                        <a>ادامه مطلب</a>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default ContentBox ;
