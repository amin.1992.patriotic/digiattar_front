import React, {useEffect, useState} from 'react';
import Header from "../header/header";
import Footer from "../footer/footer";
import {ToastContainer} from "react-toastify";

export default function Layout(props) {

    return (
        <>
            <Header/>
            {props.children}
            <ToastContainer
                className="impct-toast"
                position="top-right"
                autoClose={3000}
                hideProgressBar
                newestOnTop
                closeOnClick
                rtl={true}
                pauseOnVisibilityChange
                draggable={false}
                pauseOnHover
            />
            <Footer/>
        </>
    );
};











