import React, {useEffect} from 'react';
import styles from "../../styles/temperament.module.css"
import {Container} from "@material-ui/core";
import {useRouter} from "next/router";
import Link from "next/link";
import {ENV} from "../../services/env";
import {Helmet} from "react-helmet";

const Temperament = (props) => {
    const template = [
        {
            title: "دانستنی های مزاج دم",
            dataSrc: "/wind.png",
            id: 1,
            description: "هوا به سیال بودن، لطافت یا در ظرف و قالبی نگنجیدن و ویژگی هایی از این دست شناخته می شود، حال اگر بخواهیم این ویژگی ها را با حالات انسانی یک فرد دارای طبع گرم و تر (دم) تطبیق دهیم می توانیم این افراد خصوصیاتی چون..."
        },
        {
            title: "دانستنی های مزاج صفرا",
            dataSrc: "/fire.png",
            id: 2,
            description: "آتش به گرمی و تندی، در تکاپو بودن، به سرعت شعله ور شدن و به همان میزان زود خاموش شدن و ویژگی هایی از این دست شناخته می شود، حال اگر بخواهیم این ویژگی ها را با حالات انسانی یک فرد دارای طبع گرم و خشک (صفرا) تطبیق دهیم می توانیم بگوییم این افراد خصوصیاتی چون ..."
        },
        {
            title: "دانستنی های مزاج بلغم",
            dataSrc: "/water.png",
            id: 3,
            description: "آب به گذرا بودن، وزن داشتن و سنگینی، شکل پذیری و ویژگی هایی از این دست شناخته می شود، حال اگر بخواهیم این ویژگی ها را با حالات انسانی یکم فرد دارای طبع سرد و تر (بلغم) تطبیق دهیم می توانیم بگوییم این افراد خصوصیاتی چون ..."
        },
        {
            title: "دانستنی های مزاج سودا",
            dataSrc: "/earth.png",
            id: 4,
            description: "خاک به خشکی، سردی، عدم انعطاف و ویژگی هایی از این دست شناخته می شود، حال اگر بخواهیم این ویژگی ها را با حالات انسانی یکم فرد دارای طبع سرد و خشک (سودا) تطبیق دهیم می توانیم بگوییم این افراد خصوصیاتی چون ..."
        },
    ]
    const as = (e) => {

    }
    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.scrollY > 500) {
                [...document.querySelectorAll('#temperament img')].map(img => img.src = img.getAttribute("data-src"))
            }
        })
    }, [])
    const router = useRouter()
    return (
        <>
            <div id={"temperament"} className={styles.temperament}>
                <Container>
                    <div className={styles.main_section}>
                        <h4 className={styles.temperament_list_title}>تعیین مزاج آنلاین به سبک دیجی عطار</h4>
                        <div className={styles.temperament_main}>
                            <div className={styles.temperament_list}>
                                <ul>
                                    {template && template.map((item, index) => {
                                        return (
                                            <li>
                                                <div onClick={as} className={styles.temperament_list_image}>
                                                    <img src={""} data-src={item.dataSrc}/>
                                                </div>
                                                <div onClick={() => {
                                                    localStorage.setItem("temperament_id", item.id);
                                                    router.push(`blog/post/[...param]?id=${159}&slug=${"مزاج-شناسی-یا-خود-شناسی"}`, `/blog/post/159/مزاج-شناسی-یا-خود-شناسی/`)
                                                }} className={styles.title}>
                                                    <h5>{item.title}</h5>
                                                    <span className={styles.description}>{item.description}</span>
                                                </div>
                                            </li>
                                        )
                                    })}
                                </ul>
                            </div>
                            <div className={styles.temperament_image}>
                                <img src={""} data-src={"/temperament.png"}/>
                                <Link
                                    href={"/determination_of_temperament"}
                                >
                                    <span  className={styles.temperament_btn}>تعیین مزاج</span>
                                </Link>
                            </div>
                        </div>

                    </div>
                </Container>
            </div>

        </>
    );
};

export default Temperament;
