import React from 'react';

const InlineLoader = () => {
    return (
        <div className="inline_loader">
           <div className="inline_loader_column"/>
        </div>
    );
};

export default InlineLoader;
