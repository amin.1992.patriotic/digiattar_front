import React, {useEffect, useState} from 'react';

const Select = (props) => {
    return (
            <div  className="select">
                <select onChange={(e) => {props.onChange(e.target.value)}}  name="slct" id="slct">
                    {
                        props.option && props.option.map((item,index)=>{
                            return(
                                <>
                                    <option value={item.id}>{item.title}</option>
                                </>
                            )
                        })
                    }
                </select>
                <label>
                    {props.title}
                </label>
            </div>
    );
};

export default Select;
