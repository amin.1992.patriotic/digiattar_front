import React, {useState} from 'react';

const RadioSelect = (props) => {
    const city = JSON.parse(props.city)
    return (
        <div className='radio'>
                <input onClick={()=>{props.onChange(props.id)}} type="radio" id={props.id} name={props.name} value={props.id} />
            {
                <label className="label_detail" htmlFor={props.id}>
                    <span><span> نام و نام خانوادگی : </span>{props.reciverName && props.reciverName}</span>
                    <span><span>  شماره تلفن : </span>{props.reciverMobile && props.reciverMobile}</span>
                    <span><span>آدرس : </span>
                        {
                            city && city.map((item) => {
                                return (
                                    item.title + " - "
                                )
                            })
                        }
                        {props.address && props.address}
                    </span>
                    <span><span>کد پستی : </span>{props.postalCode && props.postalCode}</span>
                </label>
            }
            {
                (props.title || props.icon) &&
                <label className="label_icon" htmlFor={props.id}>
                    <div className="radio_icon">
                        <img src={props.icon}/>
                    </div>
                    <span>{props.title}</span>
                </label>
            }
            <div className={"icon_image_edit"} onClick={() => {props.onEdit(props.id)}}>
                <img src={"/edit.svg"}/>
            </div>
            <div  className={"icon_image_delete"}  onClick={() => {props.onDelete(props.id)}}>
                <img src={"/delete.svg"}/>
            </div>
        </div>
    );
};

export default RadioSelect;
