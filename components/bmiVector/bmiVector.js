import React from 'react';
import styles from "../../styles/bmiVector.module.css"
import {useRouter} from "next/router";

const BmiVector = ({data}) => {
    console.log(data)
    return (
        <div className={styles.bmi_vector}>
            <span className={`${styles.circle}`}>
                <span style={{left: data.location.hot + "%" , top:data.location.dry + "%"}} className={styles.position}/>
            </span>
            <span className={`${styles.vertical_line}`}>
                   <svg className={styles.top_arrow} xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                        viewBox="0 0 24 24"><path d="M22 12l-20 12 5-12-5-12z"/></svg>
                <svg className={styles.down_arrow} xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                     viewBox="0 0 24 24"><path d="M22 12l-20 12 5-12-5-12z"/></svg>
            </span>
            <span className={`${styles.horizontal_line}`}>
                <svg className={styles.right_arrow} xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                     viewBox="0 0 24 24"><path d="M22 12l-20 12 5-12-5-12z"/></svg>
                <svg className={styles.left_arrow} xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                     viewBox="0 0 24 24"><path d="M22 12l-20 12 5-12-5-12z"/></svg>
            </span>

            <div className={`${styles.bmi_vector_items} ${styles.Bile}`}/>
            <div className={`${styles.bmi_vector_items} ${styles.soda}`}/>
            <div className={`${styles.bmi_vector_items} ${styles.bellows}`}/>
            <div className={`${styles.bmi_vector_items} ${styles.mucus}`}/>
        </div>
    );
};

export default BmiVector;
