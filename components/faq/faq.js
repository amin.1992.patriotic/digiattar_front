import React, {useState} from 'react';
import styles from "./faq.module.css"
import {Grid} from "@material-ui/core";

const Faq = (props) => {
    const [toggle , setToggle] = useState(false)
    const [id , setId] = useState(null)
    const {list} = props
    return (
        <Grid item xs={12}>
           <div className={styles.faq_list}>
               <h2>پرسش‌های متداول</h2>
               <ul>
                   {
                      list?.map((item) => {
                          return(
                              <li>
                                  <div itemScope itemProp={"mainEntity"} itemType={"https://schema.org/Question"}>
                                      <div onClick={(e) => { setToggle(( toggle && id === item.id ) ? false : true) ; setId(item.id)}} className={`${styles.faq_question} ${(toggle && id === item.id) ? styles.faq_question_open : styles.faq_question_close}`} >
                                          <img src={"/arrow-down.svg"}/>
                                          <h4 itemProp={"name"}>
                                              {item.question}
                                          </h4>
                                      </div>
                                      <div className={`${styles.faq_answer} ${(toggle && id === item.id) ? styles.faq_answer_open : styles.faq_answer_close}`} itemScope itemProp={"acceptedAnswer"} itemType={"https://schema.org/Answer"}>
                                          {item.answer}
                                      </div>
                                  </div>
                              </li>
                          )
                      })
                   }
               </ul>
           </div>
       </Grid>
    );
};

export default Faq;