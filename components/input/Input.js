import React, {useEffect, useState} from 'react';

const Input = (props , ref) => {
    const handleChange = event => {
        props.onchange(event.target.value);
    }

    return (
        <>
            <label  className="input">
                {props.hasText != "" && props.hasText}
                {props.img !="" && <img className="input_image"  src={props.img}/>}
                <input
                    ref ={ref}
                    value={props.data}
                    placeholder={props.placeholder}
                    type={props.type}
                    onChange= {handleChange}
                />
            </label>
            <style jsx>
                {`
                .input{
                    width : ${props.width};
                    margin : ${props.margin}
                  }
                .input input{
                    margin : ${props.margin}
                  }
                `}
            </style>
        </>
    );
};

export default React.forwardRef(Input);
