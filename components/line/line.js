import React from 'react';
import Loading from "../loading/loading";

const Line = (props) => {
    return (
        <div className="row">
            {
                props.loading ?  <span>{"در حال بارگزاری..."}</span> :  <span>{props.title}</span>
            }

        </div>
    );
};

export default Line;
