import React , { useEffect, useState } from 'react';
const InputSearch = (props) => {
    const [text , setText] =useState("")
    const handleSearch = (e)=>{
        setText(e.target && e.target.value)
        props.search(e.target && e.target.value)
    }
    return (
        <div>
            <input placeholder={"جستجو"} value={text} onChange={(e)=>{handleSearch(e)}} className="input_search" type="text"/>
        </div>
    );
};

export default InputSearch;
