import React, {useState} from 'react';
import {useRouter} from "next/router";
import {connect, useDispatch, useSelector} from "react-redux";
import {PREV_DATA_COUNT, LOGIN_MODAL, PREV_DATA} from "../../redux/types";
import Api from "../../services/api";
import {toast} from "react-toastify";
import Loading from "../loading/loading";
import {separate} from "../../services/separate"
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

const CategoryBox = (props) => {
    const [loading, setLoading] = useState(false)
    const isLogin = useSelector(state => state.loginReducer.login)
    const router = useRouter()
    const addToCard = (typeId) => {
        setLoading(true)
            new Api().post("/card", {
                count: 1,
                id: typeId,
            })
                .then((res) => {
                    setLoading(false)
                    if (typeof res !== "undefined") {
                        if (res.status) {
                            router.push("/cart")
                            // toast.success("با موفقیت به سبد خرید اضافه شد");
                            props.prevDataCount(res.card)
                            props.prevData(res.card)

                        } else {
                            if (res.msg === "Column 'product_pins_id' cannot be null") {
                                toast.info("محصول ناموجود است ");
                            } else {
                                toast.error(res.msg);
                            }

                        }
                    }
                })
                .catch((err) => {
                    setLoading(false)
                    toast.error(err);
                })

    };
    console.log(props.src)

    return (
        <div className={props.row && "go-row"}>
            <div className="categoryMainBox">
                <div onClick={() => {
                    router.push(`/product/[...param]?id=${props.id}&slug=${props.slug}`, `/product/${props.id}/${props.slug}`)
                }} className="categoryBoxImage">
                    {loading && <Loading width = {"30"} height = {"30"} position ={"absolute"}/>}
                    {
                        props.src.includes("null") ?
                            <img alt={props.slug === "" ? "عطاری آنلاین دیجی عطار، فروشگاه جامع گیاهان دارویی وطب سنتی" : props.slug } className="image"  src={"/emty_image.png"}/>
                            :
                            <LazyLoadImage
                                alt={props.slug}
                                effect="blur"
                                src={props.src} />
                    }
                </div>
                <div onClick={(e) => {
                    e.target === e.currentTarget && router.push(`/product/[...param]?id=${props.id}&slug=${props.slug}`, `/product/${props.id}/${props.slug}`)
                }} className='categoryBox'>
                <span onClick={(e) => {
                    e.target === e.currentTarget && router.push(`/product/[...param]?id=${props.id}&slug=${props.slug}`, `/product/${props.id}/${props.slug}`)
                }} className='categoryBoxTitle'>{props.title && props.title.substr(0, 28) + " " }</span>
                    <div onClick={(e) => {
                        e.target === e.currentTarget && router.push(`/product/[...param]?id=${props.id}&slug=${props.slug}`, `/product/${props.id}/${props.slug}`)
                    }} className="categoryPriceBox">
                        <div className="categoryPrice">
                            <span style={{color:( props.off === 0 || props.off === props.price ) && "transparent"}} className="categoryOriginalPrice">{typeof props.price === "undefined" ? " " : separate(props.price)}</span>
                            <span  className="categoryOff">{typeof props.off === "undefined" ? " " :separate(props.off)} تومان </span>
                            <div style={{color:( props.weight === undefined ) && "transparent"}} className={"weight"}>
                                {`${props.weight}`}
                            </div>
                        </div>
                        {
                            loading && <Loading position={"absolute"}/>
                        }
                               <div onClick={(e) => {
                                    isLogin ? addToCard(props.typeId) : props.loginModalToggle()
                                }} className="categoryPriceBtn">
                                   {
                                       <div className="surface"/>
                                   }
                                </div>
                    </div>

                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        data: state.others,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        prevDataCount: function (payload) {
            dispatch({
                'type': PREV_DATA_COUNT,
                payload: payload
            });
        },
        prevData: function (payload) {
            dispatch({
                'type': PREV_DATA,
                payload: payload
            });
        },
        loginModalToggle: function (payload) {
            dispatch({
                'type': LOGIN_MODAL,
                payload: true
            });
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryBox);



