import React from 'react';
import moment from 'moment-jalali';
import {useRouter} from "next/router";
import Link from "next/link";
import InlineLoader from "../inlineLoader/inlineLoader";

const BlogBox  = (props) => {
    console.log(props)
    const  router = useRouter()
    return (
        <div  className="blog_box">
            <div className="blog_box_image">
                 <img alt={props.title} src={props.src !== null ? props.src : "/null-image.png"}/>
            </div>
            <div  className="blog_box_detail">
                <div  className="blog_box_title">
                    <Link href={`/blog/post/[...param]`} as={`/blog/post/${props.id}/${props.slug}`}>
                        <a >
                            {props.title}
                        </a>
                    </Link>
                </div>
                <div dangerouslySetInnerHTML={{__html:props.des}} className="blog_box_des"/>
                <ul className="blog_box_reaction">
                    <li>
                        <img src={"/time.svg"}/>
                        <div>{moment(props.created_at).format(' jYYYY/jM/jD')}</div>
                    </li>
                </ul>
                <div className="blog_more_btn">
                    <Link href={`/blog/post/[...param]`} as={`/blog/post/${props.id}/${props.slug}`}>
                        <a >
                            ادامه مطلب
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default BlogBox ;
