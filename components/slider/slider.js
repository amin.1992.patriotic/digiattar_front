import React, {useEffect, useState} from 'react';
import CategoryBox from "../categoryBox/categoryBox";
import MiniBox from "../miniBox/miniBox";
import {ENV} from "../../services/env"
import {useRouter} from 'next/router'
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {Navigation, Pagination, Scrollbar, A11y, Autoplay, EffectFade} from 'swiper';
import Loading from "../loading/loading";
import Link from "next/link";
import {separate} from "../../services/separate";
import Image from "next/image"
import {LazyLoadImage} from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Autoplay, EffectFade]);


const Slider = (props) => {
    const [modal, setModal] = useState(false)
    const [itemId, setItemId] = useState(null)
    const router = useRouter()
    let lg = (props.type === "showOne" || props.type === "mainSlider") ? 1 : props.type === "CategoryBox" ? 5 : (props.type === "search" || props.type === "search_other" || props.type === "search_blog") ? 2 : 4
    let md = (props.type === "showOne" || props.type === "mainSlider") ? 1 : props.type === "CategoryBox" ? 4 : (props.type === "search" || props.type === "search_other" || props.type === "search_blog") ? 2 : 3.5
    let sm = (props.type === "showOne" || props.type === "mainSlider") ? 1 : props.type === "CategoryBox" ? 3 : (props.type === "search" || props.type === "search_other" || props.type === "search_blog") ? 2 : 2
    let xs = (props.type === "showOne" || props.type === "mainSlider") ? 1 : props.type === "CategoryBox" ? 2.2 : (props.type === "search" || props.type === "search_other" || props.type === "search_blog") ? 2 : 1.2
    const params = {
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: false,
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
            disabledClass: '.swiper-button-disabled',
            hideOnClick: true,
            lockClass: 'swiper-button-lock',
        },
        autoplay: props.type === false,
        breakpoints: {
            1024: {
                slidesPerView: lg,
                spaceBetween: 30
            },
            768: {
                slidesPerView: md,
                spaceBetween: 30
            },
            640: {
                slidesPerView: sm,
                spaceBetween: 20
            },
            320: {
                slidesPerView: xs,
                spaceBetween: 10
            }
        }
    }
    return (
        <>

            {props.type === "mainSlider" &&
            <Swiper effect={'creative'}   {...params} navigation>
                {
                    !props.loading ?
                        props.data && props.data.map((item) => {
                                return (
                                    <SwiperSlide>
                                        <Link href={`${item.link}`}>
                                            <LazyLoadImage
                                                effect="blur"
                                                style={{cursor: "pointer"}}
                                                alt={"عطاری آنلاین دیجی عطار، فروشگاه جامع گیاهان دارویی وطب سنتی"}
                                                className="main_slider_image" src={item.prefix + "/" + item.file}/>
                                        </Link>
                                    </SwiperSlide>
                                )
                            }
                        ) :
                        <SwiperSlide>
                            <div className="main_slider">
                                <Loading position={"absolute"}/>
                            </div>
                        </SwiperSlide>
                }

            </Swiper>
            }
            {props.type === "showOne" &&
            <Swiper navigation>
                {
                    props.data && props.data.map((item) => {
                        return (
                            <SwiperSlide>
                                <Image layout="fill" alt={"عطاری آنلاین دیجی عطار، فروشگاه جامع گیاهان دارویی وطب سنتی"}
                                       src={item.prefix + "/400/" + item.file}/>
                            </SwiperSlide>
                        )
                    })
                }
            </Swiper>
            }
            {props.type === "search" &&
            <Swiper {...params} navigation>
                <>
                    {
                        props.data && props.data.map((item) => {
                            return (
                                <SwiperSlide>
                                    <div style={{borderBottom: "1px dotted var(--border)"}} onClick={() => {
                                        router.push(`/product/[...param]`, `/product/${item.id}/${item.slug}`);
                                        props.closeModal(false)
                                    }} className="search_main_results_list">
                                        <div className={"search_img"}>
                                            <img
                                                src={(item.img !== null && item.img !== undefined) ? ENV["API"]["IMG"] + "/product/" + item.id + "/100/" + item.img : "/emty_image.png"}/>
                                        </div>
                                        {
                                            props.path && props.path.includes("category") &&
                                            <>
                                                <div
                                                    dangerouslySetInnerHTML={{__html: props.searchWord.replaceAll(props.searchWord, '<b style="color:red">' + props.searchWord + '</b>')}}/>
                                                {"  در  "}
                                            </>
                                        }
                                        <div
                                            dangerouslySetInnerHTML={{__html: item.title.replaceAll(props.searchWord, '<b style="color:red " >' + props.searchWord + '</b>')}}/>
                                        <b className={"search_main_results_box_detail_price"}>
                                            {(item.types[0] && item.types[0].package_count < 1 || typeof item.types[0] === "undefined") ? "ناموجود" : item.types[0] && separate(item.types[0].off_price) + " تومان "}
                                        </b>
                                    </div>
                                </SwiperSlide>
                            )
                        })
                    }
                </>
            </Swiper>
            }
            {props.type === "search_blog" &&
            <Swiper {...params} navigation>
                <>
                    {
                        props.data && props.data.map((item) => {
                            return (
                                <SwiperSlide>
                                    <div onClick={() => {
                                        router.push(`/blog/post/[...param]?id=${props.id}&slug=${props.slug}`, `/blog/post/${item.id}/${item.slug}`);
                                        props.closeModal(false)
                                    }} className="search_main_results_list">
                                        <div className={"search_img"}>
                                            <img
                                                src={(item.img !== null && item.img !== undefined) ? ENV["API"]["IMG"] + "/content/" + item.id + "/100/" + item.img : "/emty_image.png"}/>
                                        </div>
                                        {
                                            props.path && props.path.includes("category") &&
                                            <>
                                                <div
                                                    dangerouslySetInnerHTML={{__html: props.searchWord.replaceAll(props.searchWord, '<b style="color:red">' + props.searchWord + '</b>')}}/>
                                                {"  در  "}
                                            </>
                                        }
                                        <div style={{width: "100%", marginTop: "10px"}}
                                             dangerouslySetInnerHTML={{__html: item.title.replaceAll(props.searchWord, '<b style="color:red">' + props.searchWord + '</b>')}}/>
                                    </div>
                                </SwiperSlide>
                            )
                        })
                    }
                </>
            </Swiper>
            }
            {props.type === "search_other" &&
            <Swiper {...params} navigation>
                <>
                    {
                        props.data && props.data.map((item) => {
                            return (
                                <SwiperSlide>
                                    <div className="search_main_results_list_box">
                                        <div onClick={() => {
                                            router.push(`/product/[...param]`, `/product/${item.id}/${item.slug}`);
                                            props.closeModal(false)
                                        }} className="search_main_results_list">
                                            <div className={"search_img"}>
                                                <img
                                                    src={(item.img !== null && item.img !== undefined) ? ENV["API"]["IMG"] + "/product/" + item.id + "/100/" + item.img : "/emty_image.png"}/>
                                            </div>
                                            <div className={"search_title"}
                                                 dangerouslySetInnerHTML={{__html: item.title.replaceAll(props.searchWord, '<b style="color:red !important;">' + props.searchWord + '</b>')}}/>
                                            <div style={{width: "100%", marginTop: "10px", color: "rgba(0,0,0,.7)"}}>
                                                <div style={{height: "75px"}}
                                                     className={`${item.heading.length > 100 && "search_main_results_list_des"}`}
                                                     dangerouslySetInnerHTML={{__html: item.heading.replaceAll(props.searchWord, '<b style="color:red !important;">' + props.searchWord + '</b>').substr(0, 100)}}/>
                                            </div>
                                        </div>
                                        {
                                            item.heading.length > 100 &&
                                            <b onMouseOver={() => {
                                                setModal(true);
                                                setItemId(item.id)
                                            }} onMouseOut={() => {
                                                setModal(false)
                                            }} className={"more_btn_search_item"} style={{color: "green"}}>نام های بیشتر
                                                <div
                                                    className={`search_modal_more_name ${(modal && item.id === itemId) ? "more_name_show" : "more_name_hide"}`}
                                                    style={{width: "100%", marginTop: "10px", color: "rgba(0,0,0,.7)"}}>
                                                    <div
                                                        dangerouslySetInnerHTML={{__html: item.heading.replaceAll(props.searchWord, '<b style="color:red !important;">' + props.searchWord + '</b>')}}/>
                                                </div>
                                            </b>
                                        }
                                    </div>
                                </SwiperSlide>
                            )
                        })
                    }
                </>
            </Swiper>
            }
            {props.type === "CategoryBox" &&
            <Swiper loop={true} {...params} navigation>
                {
                    !props.loading ?
                        props.data && props.data.map((item, key) => {

                            return (
                                <SwiperSlide>
                                    <CategoryBox
                                        loading={props.loading}
                                        product_id={item.pivot && item.pivot.product_id}
                                        src={ENV["API"]["IMG"] + "/product/" + item.id + "/200/" + item.img}
                                        title={item.title}
                                        price={item.types && item.types[0] && item.types[0].price}
                                        off={item.types && item.types[0] && item.types[0].off_price}
                                        id={item.id}
                                        typeId={item.types && item.types[0] && item.types[0].id}
                                        slug={item.slug}
                                        weight={item.types?.[0].price_parameters?.[0]?.title}
                                    />
                                </SwiperSlide>
                            )
                        })
                        :
                        <>
                            <SwiperSlide>
                                <CategoryBox
                                    loading={props.loading}
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                                <CategoryBox
                                    loading={props.loading}
                                />
                            </SwiperSlide>
                        </>
                }
            </Swiper>
            }
            {props.type === "MiniBox" &&
            <Swiper {...params} navigation>
                {
                    !props.loading ?
                        props.data && props.data.map((item, key) => {
                            return (
                                <SwiperSlide>
                                    <MiniBox
                                        id={item.id}
                                        slug={item.slug}
                                        src={ENV["API"]["IMG"] + '/product/' + item.id + '/200/' + item.img}
                                        title={item.title}
                                        price={item.types && item.types[0].price}
                                        off={item.types && item.types[0].off_price}
                                    />
                                </SwiperSlide>

                            )
                        }) :
                        <>
                            <SwiperSlide>
                                <MiniBox
                                    loading={props.loading}
                                />
                            </SwiperSlide>
                            <SwiperSlide>
                                <MiniBox
                                    loading={props.loading}
                                />
                            </SwiperSlide>
                        </>
                }
            </Swiper>
            }
        </>
    )
};
export default Slider;
