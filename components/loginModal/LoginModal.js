import React, {useState, useEffect, useRef} from 'react';
import styles from "../../styles/LoginModal.module.css"
import Input from "../input/Input";
import Button from "../button/button"
import Api from "../../services/api";
import {toast} from 'react-toastify';
import {useDispatch, useSelector} from "react-redux";
import { prevData, prevDataCount,userData} from "../../redux/actions"
import Cookies from 'universal-cookie';
import ReCaptcha from '../reCaptcha/ReCaptcha'


const LoginModal = (props) => {
    const [loading, setLoading] = useState(false)
    const dispatch = useDispatch()
    const [loginShow, setloginShow] = useState(false)
    const [enterTheCode, setEnterTheCode] = useState(false)
    const [password, setPassword] = useState("")
    const [phone, setPhone] = useState("")
    const [signUpPassword, setSignUpPassword] = useState("")
    const [loginMode, setLoginMode] = useState(0)
    const [captcha, setCaptcha] = useState("")
    const [captchaRegenerate, setCaptchaRegenerate] = useState(true)
    const loginData = useSelector((state) => state.loginReducer)
    const cookies = new Cookies();


    useEffect(() => {
        setloginShow(props.show)
        setEnterTheCode(false)
    }, [props.show])


    useEffect(() => {
        if (loginData.login) {
            new Api().get("/card", {})
                .then((res) => {
                    if (typeof res !== "undefined") {
                        dispatch(prevDataCount(res))
                        dispatch(prevData(res))
                    }
                })
                .catch((err) => {
                    toast.error(err);
                })
        }
    }, [loginData.login])



    const verification = () => {
        if (phone === "") {
            toast.error('شماره موبایل خود را وارد کنید');
            return false
        }
        if (phone.length !== 11) {
            toast.error(" شماره موبایل اشتباه است");
            return false
        }
        if (isNaN(phone)) {
            toast.error(" شماره موبایل اشتباه است");
            return false
        }
        if (parseInt(phone.charAt(0)) !== 0) {
            toast.error(" شماره موبایل با صفر شروع می شود");
            return false
        }
        if (parseInt(phone.charAt(1)) !== 9) {
            toast.error(" رقم دوم شماره موبایل 9 است");
            return false
        }
        if (password === "") {
            toast.error(" رمز عبور خود را وارد کنید .");
            return false
        }
        return true
    }
    const verificationSignUp = () => {
        if (phone === "") {
            toast.error('شماره موبایل خود را وارد کنید');
            return false
        }
        if (phone.length !== 11) {
            toast.error(" شماره موبایل اشتباه است");
            return false
        }
        if (isNaN(phone)) {
            toast.error(" شماره موبایل اشتباه است");
            return false
        }
        if (parseInt(phone.charAt(0)) !== 0) {
            toast.error(" شماره موبایل با صفر شروع می شود");
            return false
        }
        if (parseInt(phone.charAt(1)) !== 9) {
            toast.error(" رقم دوم شماره موبایل 9 است");
            return false
        }
        if (signUpPassword === "") {
            toast.error(" رمز عبور خود را وارد کنید .");
            return false
        }

        return true
    }
   // useEffect(()=>{
   //     setCaptchaRegenerate(false)
   // } ,[captcha])

   const login = () => {

       if(!verification()) return false
       setCaptchaRegenerate(false)
       setLoading(true)
        new Api().post('/auth/login', {
            "username": phone,
            "password": password,
            "captcha": captcha
        })
            .then(resp => {
                if (resp.status) {
                    cookies.set('auth' , resp.token)
                    toast.success(resp.msg);
                    setLoading(false)
                    dispatch(userData(resp))
                } else {
                    toast.error(resp.msg);
                    setLoading(false)
                }
                setCaptchaRegenerate(true)
            })
            .catch((err) => {
                setLoading(false)
            });
    }
    const signUp = () => {
        if(!verificationSignUp()) return false
        setCaptchaRegenerate(false)
        setLoading(true)
        new Api().post('/auth/register', {
            "username": phone,
            "password": signUpPassword,
            "captcha": captcha
        })
            .then(resp => {
                if (resp.status) {
                    toast.success(resp.msg);
                    setLoading(false)
                    setLoginMode(0)
                } else {
                    toast.error(resp.msg);
                    setLoading(false)
                }
                setCaptchaRegenerate(true)
            })
            .catch((err) => {
                setLoading(false)
            });
    }
    const handleLoginMode = () => {
        switch (loginMode) {
            case 0 :
                return (
                    <>
                        <div className={styles.register_header}>
                            <span>ورود به سایت</span>
                        </div>
                        <div onKeyUp={(e) => {e.key === "Enter" && ( login())}} className={styles.main}>
                            {
                                <Input
                                    img={"/mobile.png"}
                                    hasText={" شماره تلفن همراه"}
                                    data={phone}
                                    onchange={(e) => {
                                        setPhone(e)
                                    }}
                                    placeholder={" شماره تلفن همراه"}
                                    type={"text"}
                                />
                            }
                            {
                                <Input
                                    img={"/password.png"}
                                    hasText={"رمز عبور"}
                                    data={password}
                                    onchange={(e) => {setPassword(e)}}
                                    placeholder={"رمز عبور "}
                                    type={"password"}
                                />
                            }
                            <Button
                                onClick={()=>{login()}}
                                icon={"/arrow.png"}
                                text={"ورود"}
                                backgroundColor={"var(--primary)"}
                                width={"98%"}
                                padding={10}
                                margin={0}
                                loading={loading}
                            />
                        </div>
                        <div className={styles.login_footer}>
                            <span onClick={()=>{setLoginMode(1)}}>ثبت نام نکرده اید ؟ کلیک کنید </span>
                        </div>
                    </>
                )
            case 1 :
                return (
                    <>
                        <div className={styles.register_header}>
                            <span>ثبت نام در سایت</span>
                        </div>
                        <div onKeyUp={(e) => {
                            e.key === "Enter" && (login())
                        }} className={styles.main}>
                                <Input
                                    img={"/mobile.png"}
                                    hasText={"شماره تلفن"}
                                    data={phone}
                                    onchange={(e) => {
                                        setPhone(e)
                                    }}
                                    placeholder={"شماره تلفن"}
                                    type={"text"}
                                />
                                <Input
                                    img={"/password.png"}
                                    hasText={"رمز عبور"}
                                    data={signUpPassword}
                                    onchange={(e) => {
                                        setSignUpPassword(e)
                                    }}
                                    placeholder={"رمز عبور را وارد کنید "}
                                    type={"password"}
                                />

                            <Button
                                onClick={()=>{signUp()}}
                                icon={"/arrow.png"}
                                text={"ثبت نام"}
                                backgroundColor={"var(--primary)"}
                                width={"98%"}
                                padding={10}
                                margin={0}
                                loading={loading}
                            />
                        </div>
                        <div className={styles.login_footer}>
                            <span onClick={()=>{setLoginMode(0)}}>قبلا حساب کاربری دارید ؟ کلیک کنید  </span>
                        </div>
                    </>
                )
        }

    }

    return (
        <>
            {
                !loginData.login &&
                <div
                    className={loginShow || (loginData.login === "true") ? styles.open_login_modal : styles.login_modal_section}
                    onClick={(e) => {
                        e.target === e.currentTarget && setloginShow(false);
                        e.target === e.currentTarget && props.onchange(false)
                    }}>
                    <div className={loginShow ? styles.login_modal : styles.close_login_modal}>
                        {handleLoginMode()}
                    </div>
                    {
                        loginShow && <ReCaptcha regenerate={captchaRegenerate} onComplete={(token) => setCaptcha(token)}/>
                    }
                </div>
            }
        </>
    );
};


export default LoginModal;

