import React, {useState} from 'react';
import styles from "../../styles/Footer.module.css";
import {Grid} from "@material-ui/core";
import Container from '@material-ui/core/Container';
import {useSelector} from 'react-redux'

import Link from "next/link";
import {reminder} from "../../functions/functions";

const Footer = (props) => {
    const data = useSelector(state => state.setting.data)
    const categoryData = useSelector((state) => state.menu.data)
    const [email , setEmail] = useState()
    const handleReminder = () => {
        reminder(null , email , null ,"blog")
    }
    return (
        <>
            <img className={styles.footer_image_layer} src={"/layer.png"}/>
            <div className={styles.footer}>
                <Container>
                    <Grid container className={styles.footer_dir} spacing={3}>
                        <Grid item md={4} xs={12}>
                            <div className={styles.footer_about}>
                                <div className={styles.footer_about_section}>
                                    {/*<ul>*/}
                                    {/*    <h5 className={styles.footer_about_title}>{categoryData && categoryData.footer_menu[0].title}</h5>*/}
                                    {/*    {*/}
                                    {/*        categoryData && categoryData.footer_menu[0].children.map((item) => {*/}
                                    {/*            return (*/}
                                    {/*                <li>*/}
                                    {/*                    <Link*/}
                                    {/*                    href={{pathname: "/page", query: {type: `${item.title}`}}}*/}
                                    {/*                    as={`/page/${item.title}`}*/}
                                    {/*                >*/}
                                    {/*                    {item.title}*/}
                                    {/*                </Link>*/}
                                    {/*                </li>*/}
                                    {/*            )*/}
                                    {/*        })*/}
                                    {/*    }*/}
                                    {/*</ul>*/}
                                    <ul>
                                        <h5 className={styles.footer_about_title}>{categoryData && categoryData.footer_menu[0].title}</h5>
                                        {/*{*/}
                                        {/*    categoryData && categoryData.footer_menu[0].children.map((item) => {*/}
                                        {/*        return (*/}
                                        {/*            <li>{item.title}</li>*/}
                                        {/*        )*/}
                                        {/*    })*/}
                                        {/*}*/}
                                        <li>
                                            <Link
                                                href={{pathname: "/page", query: {type: "قوانین-و-مقررات"}}}
                                                as={"/page/قوانین-و-مقررات"}
                                            >
                                                {" قوانین و مقررات "}
                                            </Link>
                                        </li>
                                        <li>
                                            <Link
                                                href={{pathname: "/page", query: {type: "ارتباط-با-ما"}}}
                                                as={"/page/ارتباط-با-ما"}
                                            >
                                                {"ارتباط با ما"}
                                            </Link>
                                        </li>
                                        <li>
                                            <Link
                                                href={{pathname: "/page", query: {type: "درباره-ما"}}}
                                                as={"/page/درباره-ما"}
                                            >
                                                {"درباره ما"}
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                                {data && data.links && data.links.map((license, index) => {
                                    if (license.type === 'contact') {
                                        return (
                                            <div style={{display:license.id === 3 && "none"}} className={license.id === 6 ? styles.tel_number : styles.time_speech}>{license.value}</div>
                                        );
                                    }
                                })}
                                    <div className={styles.policy}>
                                    <span>{data && data.copy_right}</span>
                                </div>
                            </div>
                        </Grid>
                        <Grid item md={4} xs={12}>
                            <div className={styles.footer_description}>
                                <div className={styles.footer_email}>
                                    <span className={styles.footer_email_text}>برای اطلاع از آخرین اخبار ما ایمیل خودرا وارد کنید</span>
                                    <div className={styles.footer_email_section}>
                                        <input
                                        onChange={(e) => {setEmail(e.target.value)}}
                                        placeholder={"آدرس ایمیل خود را وارد کنید"}/>
                                        <div onClick={handleReminder} className={styles.footer_email_btn}>
                                            ثبت ایمیل
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.footer_social}>
                                    {data && data.links && data.links.map((license, index) => {
                                        if (license.type === 'social') {
                                            return (
                                                <>
                                                    <div className={styles.footer_social_section}>
                                                        <Link href={`${license.value}`}>
                                                            <a>
                                                                <img src={"/instagram.png"}/>
                                                            </a>
                                                        </Link>
                                                    </div>
                                                    <div className={styles.footer_social_section}>
                                                        <Link href={`${license.value}`}>
                                                            <a>
                                                                <img src={"/telegram.png"}/>
                                                            </a>
                                                        </Link>
                                                    </div>
                                                </>
                                            );
                                        }
                                    })}

                                </div>
                                <div className={styles.footer_symbols}>
                                    <div className={styles.footer_symbols_section}>
                                        {data && data.links && data.links.map((license, index) => {
                                            if (license.type === 'license') {
                                                return (
                                                    <>
                                                        <img
                                                            referrerPolicy="origin"
                                                            src="https://Trustseal.eNamad.ir/logo.aspx?id=211178&Code=mIS40TPqkfedlT2BZC5D"
                                                            alt=""
                                                            style={{cursor: "pointer"}}
                                                            id="mIS40TPqkfedlT2BZC5D"
                                                            onClick={() => {
                                                                window.open("https://trustseal.enamad.ir/?id=211178&Code=mIS40TPqkfedlT2BZC5D", "Popup", "toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30")
                                                            }}
                                                        />
                                                    </>
                                                );
                                            }
                                        })}

                                    </div>
                                </div>
                            </div>
                        </Grid>
                        <Grid item md={4} xs={12} className={styles.footer_description}>
                            <img className={styles.footer_logo} src={'/logo.png'}/>
                            <h1>عطاری آنلاین دیجی عطار ، مرجع کامل اطلاعات و فروشگاه گیاهان دارویی و طب سنتی </h1>
                            <p>{data && data.introduce && data.introduce.substr(0,500)}</p>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        </>
    );
}


export default Footer;

