// setting
export const SETTING = "SETTING";

// product
export const PRODUCT_REQUESTING = "PRODUCT_REQUESTING";
export const PRODUCT_SUCCESS = "PRODUCT_SUCCESS";
export const PRODUCT_FAILURE = "PRODUCT_FAILURE";



//swiper
export const SWIPER_REQUESTING = "SWIPER_REQUESTING";
export const SWIPER_SUCCESS = "SWIPER_SUCCESS";
export const SWIPER_FAILURE = "SWIPER_FAILURE";


//slider
export const SLIDER_REQUESTING = "SLIDER_REQUESTING";
export const SLIDER_SUCCESS = "SLIDER_SUCCESS";
export const SLIDER_FAILURE = "SLIDER_FAILURE";



//blog
export const BLOG_REQUESTING = "BLOG_REQUESTING";
export const BLOG_SUCCESS = "BLOG_SUCCESS";
export const BLOG_FAILURE = "BLOG_FAILURE";




//category
export const CATEGORY_REQUESTING = "CATEGORY_REQUESTING";
export const CATEGORY_SUCCESS = "CATEGORY_SUCCESS";
export const CATEGORY_FAILURE = "CATEGORY_FAILURE";


//cart
export const PREV_DATA_COUNT = 'PREV_DATA_COUNT';
export const PREV_DATA = 'PREV_DATA';
export const ORDER = 'ORDER';




//login
export const LOGIN = 'LOGIN';
export const LOG_OUT = 'LOG_OUT';
export const USER_DATA = 'USER_DATA';


//login takmil nist
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const TOGGLE_LOGIN_DIALOG = 'TOGGLE_LOGIN_DIALOG';
export const TOGGLE_LOGOUT_DIALOG = 'TOGGLE_LOGOUT_DIALOG';
export const TOGGLE_AUTH_LOADING = 'TOGGLE_AUTH_LOADING';



export const SHOPPING_LINE = "SHOPPING_LINE";
export const LOGIN_MODAL = "LOGIN_MODAL";
export const SWIPER = "SWIPER";
export const MENU = "MENU";


export const TREE = "TREE"






