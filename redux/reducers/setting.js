// Export for unit testing
import {SETTING} from "../types";

 const initialState  =  {
    data: ""
};

export default (state = initialState, action ) => {
    switch (action.type) {
        case SETTING:
            return {
                ...state,
                 data:action.payload
            };
        default:
            return state;
    }

};
