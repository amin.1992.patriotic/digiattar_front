import {
    PREV_DATA_COUNT,
    PREV_DATA,
    ORDER
} from "../types"

const INITIAL_STATE = {
    data: [] ,
    prevData:0,
    prevDataCount:0,
    orderData:""
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PREV_DATA:
            return { ...state ,prevData:action.payload};
        case PREV_DATA_COUNT:
            return { ...state ,prevDataCount:action.payload && action.payload.length};
        case ORDER:
            return { ...state ,orderData:action.payload};
        default:
            return state;
    }
};
