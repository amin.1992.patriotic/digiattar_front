import {
    SHOPPING_LINE,
    LOGIN_MODAL,
    TREE
} from '../types';

const INITIAL_STATE = {
    step: "" ,
    toggleLoginModal : false,
    tree:[]
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SHOPPING_LINE:
            return { ...state ,step : action.payload };
        case LOGIN_MODAL:
            return { ...state ,toggleLoginModal : action.payload };
        case TREE:
            return { ...state ,tree : action.payload };
        default:
            return state;
    }
};
