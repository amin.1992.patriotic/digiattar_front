// Export for unit testing
import {SLIDER_FAILURE, SLIDER_REQUESTING, SLIDER_SUCCESS} from "../types";

export const initialState  =  {
    status: 'invalid',
    err: null,
    data: []
};

export default (state = initialState, action ) => {
    switch (action.type) {
        case SLIDER_REQUESTING:
            return {
                ...state,
                status: 'request',
                data: action.payload
            };
        case SLIDER_SUCCESS:
            return {
                ...state,
                status: 'success',
                data: action.payload
            };
        case SLIDER_FAILURE:
            return {
                ...state,
                status: 'failure',
                err: action.err
            };
        default:
            return state;
    }

};
