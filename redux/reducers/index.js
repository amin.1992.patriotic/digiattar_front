import { combineReducers} from 'redux';
import settingReducer from './setting';
import cartReducer from './cartReducer';
import loginReducer from "./auth";
import othersReducer from './othersReducer';
import menuReducer from './menuReducer';

export default combineReducers({
    loginReducer,
    setting:settingReducer,
    cart:cartReducer,
    others: othersReducer,
    menu: menuReducer,
});
