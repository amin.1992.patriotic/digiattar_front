import {
    MENU,
} from '../types';

const INITIAL_STATE = {
    data: "" ,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case MENU:
            return { ...state ,data : action.payload };
        default:
            return state;
    }

};
