import {LOGIN, LOG_OUT,USER_DATA} from '../types';
// @ts-ignore
import moment from 'moment-jalali';
// @ts-ignore
import Cookies from "universal-cookie";

import { getLocalStore } from 'next-persist';

// Export for unit testing
export const initialState = {
    login: false,
    user:{},
};

const cookies = new Cookies();
const persistedState = getLocalStore('state', initialState);

export default (state = persistedState, action) => {
    switch (action.type) {
        case LOG_OUT:
            cookies.remove('auth');
            return {
                ...state,
                login: false,
                user:{},
            };
        case USER_DATA:
            cookies.set('auth', action.payload.token, { path: '/', expires: moment().add(1, "year").toDate() });
            return {
                ...state,
                login: true,
                user: action.payload.user[0],
            };
        default:
            return state;
    }
};
