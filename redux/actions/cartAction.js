import {
    PREV_DATA_COUNT,
    PREV_DATA,
    ORDER
} from "../types"



export const prevDataCount = (payload) => {
    return {
        type: PREV_DATA_COUNT,
        payload:payload
    };
};

export const prevData = (payload) => {
    return {
        type: PREV_DATA,
        payload:payload
    };
};

export const order = (payload) => {
    return {
        type: ORDER,
        payload:payload
    };
};
