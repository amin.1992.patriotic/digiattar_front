
import { SETTING} from "../types";

export const fetchSettings = (payload) => {
    return {
        type:SETTING,
        payload:payload
    };
};
