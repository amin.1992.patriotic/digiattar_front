import {
    MENU,
} from '../types';


export const menu = (payload) => {
    return {
        type: MENU,
        payload:payload
    };
};
