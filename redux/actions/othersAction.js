import {
    SHOPPING_LINE,
    LOGIN_MODAL,
    TREE,
} from '../types';

export const shoppingLine = (payload) => {
    return {
        type: SHOPPING_LINE,
        payload:payload
    };
};

export const loginModalToggle = (payload) => {
    return {
        type: LOGIN_MODAL,
        payload:payload
    };
};

export const tree = (payload) => {
    return {
        type: TREE,
        payload:payload
    };
};
