import {LOGIN, LOG_OUT, LOGIN_MODAL,USER_DATA} from "../types";



export const logOut = (payload) => {
    return {
        type: LOG_OUT,
        payload:payload
    };
};

export const userData = (payload) => {
    return {
        type: USER_DATA,
        payload:payload
    };
};
